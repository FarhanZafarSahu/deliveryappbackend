<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dateTime('coupon_expiry')->nullable();
			$table->string('is_percentage')->default(0)->comment('0 means it is a simple amount coupon and 1 means it is a percentage of total amount');
			$table->string('city_id')->nullable()->comment('location wise Coupon');	
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('coupon_expiry');
            $table->dropColumn('is_percentage');
            $table->dropColumn('city_id');

        });
    }
}
