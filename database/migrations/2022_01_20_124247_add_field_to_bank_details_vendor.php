<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToBankDetailsVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_bank_details', function (Blueprint $table) {
            $table->string("IFSC_code")->nullable();
			$table->string("account_type")->nullable();
			$table->string("branch_name")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_bank_details', function (Blueprint $table) {
			$table->dropColumn('IFSC_code');
			$table->dropColumn('account_type');
			$table->dropColumn('branch_name');

        });
    }
}
