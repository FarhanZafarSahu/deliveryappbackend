<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
			$table->integer('customer_id');
			$table->integer('no_of_items');
			$table->decimal('total_price',10,2);
			$table->decimal('discounted_price',10,2);			
			$table->decimal('discount_amount',10,2);						
			$table->decimal('delivery_charges',10,2);
			$table->dateTime('order_time');
			$table->string('order_no');
			$table->string('delivery_name');
			$table->string('delivery_address');
			$table->string('delivery_mobile_number');
			$table->string('pickup_name');
			$table->string('pickup_address');
			$table->string('pickup_mobile_number');
			$table->integer('status')->default(0);
			$table->integer('cancel_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
