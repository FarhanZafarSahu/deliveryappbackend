<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTotalJarQtyFieldToSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_subscription_plans', function (Blueprint $table) {
            $table->integer('total_jar_qty')->default(0);
			$table->timestamp('subscription_end_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_subscription_plans', function (Blueprint $table) {
            $table->dropColumn('total_jar_qty');
			$table->dropColumn('subscription_end_date');
        });
    }
}
