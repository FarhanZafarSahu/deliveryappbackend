<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionPaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_payment_histories', function (Blueprint $table) {
            $table->id();
			$table->integer('subscription_id');
			$table->integer('qty')->nullable();
			$table->integer('per_unit_price')->nullable();
			$table->integer('total_amount')->nullable();
			$table->integer('balance_amount')->nullable();
			$table->integer('payed_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_payment_histories');
    }
}
