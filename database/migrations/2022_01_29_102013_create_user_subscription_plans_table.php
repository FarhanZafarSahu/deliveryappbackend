<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subscription_plans', function (Blueprint $table) {
            $table->id();
			$table->integer('user_id');
			$table->integer('plan_id');
			$table->integer('qty');
			$table->integer('empty_jar_qty');
			$table->decimal('per_unit_empty_jar_price',10,2);
			$table->integer('product_id');
			$table->boolean('is_today_delivery')->comment('manage delivery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscription_plans');
    }
}
