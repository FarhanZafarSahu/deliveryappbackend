<?php

use App\SiteSetting;
use Illuminate\Database\Seeder;

class SiteSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteSetting::create([
           'about_description'  =>   "T&H deliver added mineral water to customers by T&H Water Delivery Application . we deliver product with help of T&H certified vendors . vendor also manage their business in the most hassle-free way.
           They can track daily jar delivery, collect payments, manage customers and inventory to ensure the smooth running of your business.",
           'contact_mobile'       => '80 85 85 85 80',
           'contact_address'     => 'New Tariq Abad, Multan, Pakistan',
           't_description'          => 'This is Demo Description'
        ]);
    }
}
