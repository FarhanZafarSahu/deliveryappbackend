<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Role::truncate();
        $roles =  [
            [
			  'name' => 'Super Admin',
              'slug' =>  Str::slug('super-admin'),
    		],
            [
			  'name' => 'Admin',
              'slug' =>  Str::slug('admin'),
    		],
  			[
			  'name' => 'Vendor',
              'slug' =>  Str::slug('vendor'),
    		],
			[
			  'name' => 'Customer',
              'slug' =>  Str::slug('customer'),
    		],
			[
			  'name' => 'delivery Boy ',
              'slug' =>  Str::slug('delivery-boy'),
    		],
          ];

          Role::insert($roles);
	    }
}
