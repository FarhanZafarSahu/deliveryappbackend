<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		User::truncate();
        $users =  [
            [
				'name'     =>  'Super Admin',
				'email'    =>  'superadmin@admin.com',
				'password' =>  Hash::make('12345678'),
				'role_id'  =>  1,
            ],
            [
              	'name'     =>  'Admin',
				'email'    =>  'admin@admin.com',
				'password' =>  Hash::make('12345678'),
				'role_id'  =>  2,
            ],
 			[
              	'name'     =>  'Vendor',
				'email'    =>  'vendor@vendor.com',
				'password' =>  Hash::make('12345678'),
				'role_id'  =>  3,
            ],
			[
               	'name'     =>  'Delivery Boy',
				'email'    =>  'delivery@delivery.com',
				'password' =>  Hash::make('12345678'),
				'role_id'  =>  5,
            ],
          ];

          User::insert($users);
    }
}
