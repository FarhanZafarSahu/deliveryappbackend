<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            Plan::create([
                   'id'  => 1 ,
                   'plan_name' => 'Daily'
            ]);
            Plan::create([
                'id'  => 2 ,
                'plan_name' => 'Alternate Day'
         ]);
         Plan::create([
            'id'  => 3 ,
            'plan_name' => 'Every 3 Day'
     ]);
     Plan::create([
        'id'  => 4 ,
        'plan_name' => 'Flexible'
 ]);


    }
}
