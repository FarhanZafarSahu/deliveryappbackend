{{-- modal Edit Account --}}
  
<div class="modal fade" id="editCouponModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editCouponHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="editCouponForm" name="editCouponForm">
                <input type = "hidden" name = "id" id = "id">
                @csrf
                <div>
                    <label>Coupon Code</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="couponcode" name="couponCode" placeholder="" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-heading"></span>
                            </div>
                        </div>
                    </div>
<label>Select Coupon Type </label>
                    <div class="mb-3">
                        <input type="radio" id="amountradiobtn" name="is_percentage"  value = "0" checked onclick="toggle(0);">
						<label for="amount">Amount</label>
                        <input type="radio" id="percentageradiobtn" name="is_percentage"  value = "1"  onclick="toggle(1);">
						<label for="percentage">Percentage</label>
                    </div>  
<div id = "amountedit" style = "display:none">
            
        <label>Coupon Amount </label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" id="couponamount" name="couponAmount" placeholder="">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
</div>
<div id = "percentageedit" style = "display:none">
					<label>Add Coupon Percentage</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" id="amountpercentageedit" name="amountpercentageedit">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fas fa-percent"></i> </span>
                            </div>
                        </div>
                    </div>                 
      </div>
 <label>Group</label>
                    <div class="input-group mb-3">
                      <select id = "EditcityId" name = "cityId" class="form-control">
                      </select>
                    </div>
<label>Expiry Date</label>
                    <div class="input-group mb-3">
                     <input type="datetime-local" class="form-control" id="expiryDate" name="expiryDate" required >
                    </div>
                    <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="couponSubmitButton" value="Edit">Edit Coupon                    </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>