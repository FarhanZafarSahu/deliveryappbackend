
<div class="modal fade" id="addCouponModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="bannersHeading"> Add Coupon</h4>
            </div>
            <div class="modal-body">
              <form method="POST" enctype="multipart/form-data" id="addCouponForm" action="javascript:void(0)" >
                @csrf
                <div>
                    <label>Add Coupon Code </label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="couponCode" name="couponCode" required>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                  <span><i class="fas fa-heading"></i> </span>
                            </div>
                        </div>
                    </div>   
 <label>Select Coupon Type </label>
                    <div class="mb-3">
                        <input type="radio" id="amountradio" name="is_percentage"  value = "0" checked onclick="alert(0);">
						<label for="amount">Amount</label>
                        <input type="radio" id="percentageradio" name="is_percentage"  value = "1" required onclick="alert(1);">
						<label for="percentage">Percentage</label>
                    </div>   
	<div id = "amount" >
    <label>Add Coupon Amount </label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" id="couponAmount" name="couponAmount">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fas fa-heading"></i> </span>
                            </div>
                        </div>
                    </div>
</div>                 
	<div id = "percentage" style = "display:none">
					<label>Add Coupon Percentage</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" id="amountpercentage" name="amountPercentage">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fas fa-percent"></i> </span>
                            </div>
                        </div>
                    </div>                 
      </div>
  <label>Group</label>
                    <div class="input-group mb-3">
                      <select id = "AddcityId" name = "cityId" class="form-control">
                      </select>
                    </div>
<label>Expiry Date</label>
                    <div class="input-group mb-3">
                     <input type="datetime-local" class="form-control" id="expiryDate" name="expiryDate" required >
                    </div>
                 <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="couponsubmit" value="Add Coupon ">Add Coupon
                     </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>