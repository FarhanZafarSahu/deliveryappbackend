@extends('portal.layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
            <li class="breadcrumb-item active">Coupons List</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- Main content -->

  <!-- /.content -->
  <div class="row">
      <div class=" m-2 col-lg-3">
          <div class="card">
              <div class="card-body">
                  <div class="row">
                      <div class="col-4 align-self-center">
                          <div class="icon-info">
                              <i class="fa fa-user fa-2x" aria-hidden="true" ></i>
                          </div>
                      </div>
                      <div class="col-8 align-self-center text-right">
                          <div class="ml-2">
                              <p class="mb-1 text-muted">Recent Coupon </p>
                              <input class=" h3 mt-0 mb-1 font-weight-semibold text-center" name="" id=""
                                  value="{{ recentCoupon() }}" readonly
                                  style="background: transparent; border: none;margin-left: -30px">
                          </div>
                      </div>
                  </div>
              </div>
              <!--end card-body-->
          </div>
          <!--end card-->
      </div>
  </div>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
              <h4 class="float-left">Coupon Details</h4>
              <div class="float-right"style="display: inline-flex;">
                   <a title="Add Coupon" id = "addCoupon" onclick="cities('AddcityId')" ><i class="fa fa-plus mr-1 red" style="cursor:pointer;"></i></a>
                <!-- <button type="button" class="btn btn-secondary" id = "addBanner">Add Banner</button> -->
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>id</th>
                <th>Coupon Code</th>
                <th>Coupon Amount</th>
			    <th>Coupon Expiry</th>
                <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['coupon'] as $index => $data)
                <tr>
                  <td>{{$data->id}}</td>
 				  <td>{{$data->coupon_code}}</td>

                  <td>@if($data->is_percentage == 1) {{$data->amount}} % @else {{$data->amount}}@endif</td>
					<td>{{$data->coupon_expiry}}</td>
				<td>
                    <a href="javascript:void(0)" title="Edit Coupon" onclick="cities('EditcityId','{{$data->city_id}}')" data-toggle="tooltip"  data-id= "{{$data->id}}" class="edit editCoupon" id = "editCoupon"><i class="fa fa-edit mr-1 edit-btn"></i></a>
                   <a title="Delete"><i onclick="delete_confirmation('{{url('/coupon/delete',$data->id)}}')" class="fa fa-trash mr-1 red" style="cursor:pointer;"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@include('coupon.modal.addCouponModal')
@include('coupon.modal.editCouponModal')

@endsection
@section('script')
    @include('coupon.js.addCouponJavascript')
    @include('coupon.js.editCouponJavascript')

@endsection
