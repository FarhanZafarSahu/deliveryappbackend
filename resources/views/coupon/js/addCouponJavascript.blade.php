<script>

 $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
 $('#addCoupon').click(function () {
        $('#addCouponForm').trigger("reset");
        $('#addCouponModal').modal('show');
    });
$('#addCouponForm').submit(function(e) {
e.preventDefault();
var formData = new FormData(this);
$.ajax({
url: "{{ url('coupon/store') }}",
type: "POST",
data: formData,
cache:false,
contentType: false,
processData: false,
 success: function (response)
          {
              if(response.data.code == 200)
              {
                $('#addCouponForm').trigger("reset");
                $('#addCouponModal').modal('hide');
                  location.reload();
                 toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                $('#couponsubmit').html('Add Coupon');
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
               toastr.error(response.responseJSON.message);
                $('#couponsubmit').html('Add Coupon');
          }
});
});
 });
function alert(message)
{
 if(message == 0 )
	{
$("#amount").css("display", "block");
$("#percentage").css("display", "none");
	}
else if (message == 1 )
	{
	$("#amount").css("display", "none");
$("#percentage").css("display", "block");
	}
}

</script>
<script type="text/javascript">
function cities(name,id){
            $.ajax({
                type : 'POST',
               url: "{{ url('api/list-city')}}",
            success : function(response)
            {
                let city = response.data.result
                if(response.data.code == 200)
                {
                  $('#'+name).empty();
                  $('#'+name).append(`<option value="">Select Group</option>`);
                  for(let i=0; i<city.length; i++)
                  {
                    $('#'+name).append(`<option value="${city[i].id}">${city[i].name}</option>`);
                  }
                  $('#'+name).val(id);
                }
                else if(response.data.code== 400)
                {
                  toastr.error(response.data.message);
                }
            }
        });
 }
 </script>
