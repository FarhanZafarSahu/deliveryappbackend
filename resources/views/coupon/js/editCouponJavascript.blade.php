<script>
 
 $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    }); 
 $('body').on('click', '.editCoupon', function (){
      var coupon_id = $(this).data('id');
      $.get("{{ url('coupon') }}" +'/' + coupon_id +'/edit', function (data) {
          $('#couponcode').val(data.coupon_code);
console.log(data.amount);
console.log(data.is_percentage);

		if(data.is_percentage == 0 )
		{
   $(this).find('form').trigger('reset');
  		document.getElementById("amountradiobtn").checked = true;
		 $("#amountedit").css("display", "block");
		$("#percentageedit").css("display", "none");
          $('#couponamount').val(data.amount);
		}
		else if(data.is_percentage == 1 )
		{
   		$(this).find('form').trigger('reset');
  		document.getElementById("percentageradiobtn").checked = true;
		 $("#percentageedit").css("display", "block");
		 $("#amountedit").css("display", "none");
		  $('#amountpercentageedit').val(data.amount);

		}
          $('#id').val(coupon_id);
  $('#editCouponModal').modal('show');
        
      })
   });

$('#editCouponForm').submit(function(e) {
e.preventDefault();
var formData = new FormData(this);
$.ajax({
url: "{{ url('coupon/update') }}",
type: "POST",
data: formData,
cache:false,
contentType: false,
processData: false,
 success: function (response)
          {
              if(response.data.code == 200)
              {
                $('#editCouponForm').trigger("reset");
                $('#editCouponModal').modal('hide');
                  location.reload();
                 toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                $('#couponSubmitButton').html('Edit Coupon');
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
               toastr.error(response.responseJSON.message);
                $('#couponSubmitButton').html('Edit Coupon');
          }
});
});
 });
function toggle(message)
{
console.log(message)
 if(message == 0 )
	{
$("#amountedit").css("display", "block");
$("#percentageedit").css("display", "none");
	}
else if (message == 1 )
	{
	$("#amountedit").css("display", "none");
$("#percentageedit").css("display", "block");
	}
}
</script>
<script type="text/javascript">
function cities(name,id){
            $.ajax({
                type : 'POST',
               url: "{{ url('api/list-city')}}",
            success : function(response)
            {
                let city = response.data.result
                if(response.data.code == 200)
                {
                  $('#'+name).empty();
                  $('#'+name).append(`<option value="">Select Group</option>`);
                  for(let i=0; i<city.length; i++)
                  {
                    $('#'+name).append(`<option value="${city[i].id}">${city[i].name}</option>`);
                  }
                  $('#'+name).val(id);
                }
                else if(response.data.code== 400)
                {
                  toastr.error(response.data.message);
                }
            }
        });
 }
 </script>