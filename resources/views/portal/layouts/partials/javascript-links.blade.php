<!-- jQuery -->
<script src="{{asset('public/portal/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> --}}
<script src="{{asset('public/portal/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/mervick/emojionearea@master/dist/emojionearea.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/portal/dist/js/adminlte.min.js')}}"></script>

<!-- DataTables  & Plugins -->
<script src="{{asset('public/portal/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/jszip/jszip.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/filterizr/jquery.filterizr.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('public/portal/plugins/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
<script src="{{asset('public/portal/dist/js/demo.js')}}"></script>
{{-- <script src ="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
<script src ="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>
{{-- <script src ="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script> --}}
<script src ="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

<!-- Select2 -->
<script src="{{asset('public/portal/plugins/select2/js/select2.full.min.js')}}"></script>

<!-- Toastr -->
<script src="{{asset('public/portal/plugins/toastr/toastr.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('public/portal/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Date Time Picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
{{-- tinymce editor --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.10.2/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({selector:'.editor'});
	var Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 3000
	});

	function toast_error(message){
		toastr.error(message)
	}

	function toast_success(message){
		toastr.success(message)
	}

	function toast_info(message){
		toastr.info(message)
	}

	function sweet_alert_info(message)
	{
		Toast.fire({
			icon: 'info',
			title: message
		});
	}

	function delete_confirmation(link="http://www.google.com",  btnText = "Yes, delete it!", text="You won't be able to revert this!")
	{
		Swal.fire({
			title: 'Are you sure?',
			text: text,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: btnText,
		}).then((result) => {
			if (result.isConfirmed) {
				window.location = link;
			}
		})
	}

    function status_confirmation(link="http://www.google.com",  btnText = "Yes, Change it!", text="You can revert it in Future!")
	{
		Swal.fire({
			title: 'Are you sure?',
			text: text,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: btnText,
		}).then((result) => {
			if (result.isConfirmed) {
				window.location = link;
			}
		})
	}

	function add_confirmation(link="http://www.google.com",  btnText = "Yes, Add Amount!", text="You won't be able to revert this!")
	{
		Swal.fire({
			title: 'Are you sure?',
			text: text,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: btnText,
		}).then((result) => {
			if (result.isConfirmed) {
				window.location = link;
			}
		})
	}
	function Approve_wallet_request(link="http://www.google.com", text="You won't be able to revert this!")
	{
		Swal.fire({
			title: 'Are you sure?',
			text: text,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Approved it!'
		}).then((result) => {
			if (result.isConfirmed) {
				window.location = link;
			}
		})
	}


</script>

@if(session('success'))
<script type="text/javascript">
	toast_success("{{session('success')}}")
</script>
@endif

@if(session('error'))
<script type="text/javascript">
	toast_error("{{session('error')}}")
</script>
@endif

@if ($errors->any())
@foreach ($errors->all() as $error)
<script type="text/javascript">
	toast_error("{{$error}}")
</script>
@endforeach
@endif

<script>
	$(function () {
		$(".table").DataTable({
			"responsive": true, "lengthChange": true, "autoWidth": false,
			"buttons": ["print"],
			"order": [],
  "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      if (aData[5] == "OWN Customer") {
        $('td', nRow).css('background-color', 'grey');
      } else if (aData[5] == "Assign Customer") {
        $('td', nRow).css('background-color', 'lightblue');
      }
    }
		}).buttons().container().appendTo('#table-plugins');

		//Initialize Select2 Elements
		$('.select2').select2({});
		$('.select2bs4').select2({
			theme: 'bootstrap4'
		});

		//Date and time picker
		$('.date-time-picker').datetimepicker({
			format:'m-d-Y',
			timepicker:false
		});


	});
</script>
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>
