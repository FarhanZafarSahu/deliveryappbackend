<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/fontawesome-free/css/all.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('public/portal/dist/css/adminlte.min.css')}}">

<!-- Custom style -->
<link rel="stylesheet" href="{{asset('public/portal/custom.css')}}">

<!-- Select2 -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('public/portal/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/portal/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

<!-- Toastr -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/toastr/toastr.min.css')}}">

<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

<!-- Date Time picker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css">

<link rel="stylesheet" href="{{asset('public/portal/plugins/ekko-lightbox/ekko-lightbox.css')}}">
{{-- <link rel= "stylesheet" href = "https://cdn.datatables.net/1.11.2/css/jquery.dataTables.min.css"> --}}
<link rel= "stylesheet" href = "https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">
 <link rel="stylesheet" href="https://cdn.rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css">