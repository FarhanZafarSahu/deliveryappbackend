<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="height: 100%">
  <!-- Brand Logo -->
  <a href="{{ route('dashboard') }}" class="brand-link">
    <img src="{{asset('public/portal/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">
      Delivery App
      {{-- TODO: For future reference --}}
    </span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('public/portal/dist/img/AdminLTELogo.png')}}" class="img-circle elevation-2" alt="User Image" >
      </div>
      <div class="info">
        <a href="{{ route('profile') }}" class="d-block">{{ auth()->user()->name }}</a>
      </div>
    </div>
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->
        <li class="nav-item">

        <a href="{{ route('dashboard') }}" class="nav-link {{linkActivator('home') !== false? 'active':''}}">
          <i class="fa fa-home nav-icon"></i>
          <p>Home</p>
        </a>
      </li>
@if(permission('customer',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{route("customer.list")}}" class="nav-link {{linkActivator('customer') !== false? 'active':''}}">
          <i class="fa fa-users nav-icon"></i>
          <p>Customer List</p>
        </a>
      </li>
@endif
{{-- @if(permission('slider',auth()->user()->role_id)) --}}
      <li class="nav-item">
        <a href="{{route("slider.list")}}" class="nav-link {{linkActivator('slider') !== false? 'active':''}}">
          <i class="fa fa-users nav-icon"></i>
          <p>Slider List </p>
        </a>
      </li>
{{-- @endif --}}
@if(permission('/delivery/lock',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{route('delivery.lock')}}" class="nav-link {{linkActivator('delivery/lock') !== false? 'active':''}}">
          <i class="fa fa-home nav-icon"></i>
          <p>Lock/Unlock Delivery</p>
        </a>
      </li>
@endif
@if(permission('newcustomer',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{route("newcustomer.list")}}" class="nav-link {{linkActivator('new/customers') !== false? 'active':''}}">
          <i class="fa fa-users nav-icon"></i>
          <p>New Customers</p>
        </a>
      </li>
@endif
@if(permission('vendors',auth()->user()->role_id))
       <li class="nav-item">
        <a href="{{ route('vendor.list') }}" class="nav-link {{linkActivator('vendors') !== false? 'active':''}}">
          <i class="fas fa-leaf nav-icon"></i>
          <p>Vendor List</p>
        </a>
      </li>
@endif
@if(permission('deliveries',auth()->user()->role_id))

      <li class="nav-item">
        <a href="{{route('deliveries.list')}}" class="nav-link {{linkActivator('deliveries') !== false? 'active':''}}">
          <i class="fa fa-home nav-icon"></i>
          <p>Today Delivery</p>
        </a>
      </li>
@endif
@if(permission('invoice',auth()->user()->role_id))

      <li class="nav-item">
        <a href="{{ route('invoice.list') }}" class="nav-link {{linkActivator('invoice') !== false? 'active':''}}">
          <i class="fa fa-file nav-icon"></i>
          <p>Invoices</p>
        </a>
      </li>
@endif
@if(permission('complain',auth()->user()->role_id))

      <li class="nav-item">
        <a href="{{ route('complain.list') }}" class="nav-link {{linkActivator('complains') !== false? 'active':''}}">
          <i class="fa fa-box nav-icon"></i>
          <p>Complain</p>
        </a>
      </li>
@endif
@if(permission('feedback',auth()->user()->role_id))

      <li class="nav-item">
        <a href="{{ route('feedback.list') }}" class="nav-link {{linkActivator('feedback') !== false? 'active':''}}">
          <i class="fa fa-comment nav-icon"></i>
          <p>FeedBack</p>
        </a>
      </li>
@endif
@if(permission('about-us',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{ route('aboutus') }}" class="nav-link {{linkActivator('about-us') !== false? 'active':''}}">
          <i class="fa fa-address-card nav-icon"></i>
          <p>About Us</p>
        </a>
      </li>
@endif
@if(permission('contact-us',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{ route('contactus') }}" class="nav-link {{linkActivator('contact-us') !== false? 'active':''}}">
          <i class="fa fa-phone nav-icon"></i>
          <p>Contact Us</p>
        </a>
      </li>
@endif
@if(permission('term-condition',auth()->user()->role_id))
      <li class="nav-item">
        <a href="{{ route('terms') }}" class="nav-link {{linkActivator('term-condition') !== false? 'active':''}}">
          <i class="fa fa-user-secret nav-icon"></i>
          <p>Term Condtion</p>
        </a>
      </li>
@endif
@if(permission('coupon',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{route('coupon')}}" class="nav-link {{(linkActivator('coupon') !== false || linkActivator('coupon') !== false )? 'active':''}}">
              <i class="fa fa-store nav-icon"></i>
              <p>Coupons</p>
            </a>
          </li>
@endif
@if(permission('QR',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('QR/list')}}" class="nav-link {{linkActivator('QR/list') !== false? 'active':''}}">
              <i class="fas fa-qrcode nav-icon"></i>
              <p>QR code</p>
            </a>
          </li>
@endif
@if(permission('group',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('group/list')}}" class="nav-link {{linkActivator('group/list') !== false? 'active':''}}">
              <i class="fas fa-users nav-icon"></i>
              <p>Groups</p>
            </a>
          </li>
@endif

@if(permission('category',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('category/list')}}" class="nav-link {{linkActivator('category/list') !== false? 'active':''}}">
              <i class="fas fa-layer-group nav-icon"></i>
              <p>Category</p>
            </a>
          </li>
@endif

@if(permission('product',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('product/list')}}" class="nav-link {{linkActivator('product/list') !== false? 'active':''}}">
              <i class="fas fa-tags nav-icon"></i>
              <p>Product</p>
            </a>
          </li>
@endif

@if(permission('route',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('route/list')}}" class="nav-link {{linkActivator('route/list') !== false? 'active':''}}">
              <i class="fas fa-qrcode nav-icon"></i>
              <p>Route</p>
            </a>
          </li>
@endif


@if(permission('order',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('order/list')}}" class="nav-link {{linkActivator('order/list') !== false? 'active':''}}">
              <i class="fas fa-qrcode nav-icon"></i>
              <p>New ORDER</p>
            </a>
          </li>
@endif
@if(permission('deliveryboy',auth()->user()->role_id))
          <li class="nav-item">
            <a href="{{url('deliveryboy/list')}}" class="nav-link {{linkActivator('deliveryboy/list') !== false? 'active':''}}">
              <i class="fas fa-qrcode nav-icon"></i>
              <p>DeliveryBoy</p>
            </a>
          </li>
@endif
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
