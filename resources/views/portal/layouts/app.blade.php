<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{env('APP_NAME')}}</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
  @include('portal.layouts.partials.css-links')

</head>
<body class="hold-transition">
  <div class="wrapper">

    @include('portal.layouts.partials.nav')

    @include('portal.layouts.partials.sidebar')
    <div class="content-wrapper">
      @yield('content')
    </div>

    @include('portal.layouts.partials.right-panel')
    {{-- @include('portal.layouts.partials.footer') --}}

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->
  @include('portal.layouts.partials.javascript-links')
  @yield('script')
  
</body>
</html>
