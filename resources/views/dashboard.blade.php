@extends('portal.layouts.app')

@section('content')

 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="row">
@if(Auth::user()->role_id == 1)
 <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{totalTodayDelivery()}}</h3>

                  <p>Today's Total Delivery</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="{{route('deliveries.list')}}" class="small-box-footer">See Details <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
@endif
@if(Auth::user()->role_id == 1)
 <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{totalTodayDoneDelivery()}}</h3>

                  <p>Today's Total Done Delivery</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="{{route('deliveries.list')}}" class="small-box-footer">See Details <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
@endif
            <!-- ./col -->
          </div>


      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->

@endsection
