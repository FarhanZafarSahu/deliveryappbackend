@extends('portal.layouts.app')
@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
					<th>#Invoice No.</th>
					<th>Vendor Name</th>
					<th>Product</th>
					<th>Qty</th>
					<th>Sub Total</th>
					<th>GST</th>
					<th>Total</th>
					<th class="text-right">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['orders'] as $index => $data)
                <tr>
                  <td>{{$data->order_no}}</td>
                  <td>{{$data->vendor->name ?? ''}}</td>
 				  <td>{{$data->orderDetails[0]->product->product_title}}</td>
 				  <td>{{$data->quantity}}</td>
 				  <td>{{$data->discounted_price}}</td>
 				  <td>{{$data->delivery_charges}}</td>
 				  <td>{{$data->discounted_price + $data->delivery_charges}}</td>
 				  <td>{{$data->name}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection