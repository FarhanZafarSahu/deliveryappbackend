@extends('portal.layouts.app')

@section('content')
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Route</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Add Route </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Add Route</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('route.store.assign-deliveryboy-route')}}" method = "POST" enctype="multipart/form-data" >
				@csrf
				<input type = "hidden" name = "user_id" value="{{$result['selectedDeliveryBoy']->id}}">
                <div class="card-body">
					 <div class="form-group">
                        <label for="exampleInputAddress">Selected Delivery Boy</label>
                        <input type="text" name = "user_name" class="form-control" id="exampleInputName" value = "{{$result['selectedDeliveryBoy']->name}}" readonly>
                      </div>
					@if($result['deliveryBoys']->count() != 0)
					 <div class="form-group">
                        <label for="exampleInputAddress">Select Delivery Boy To Clone Their Route</label>
                        <select class="form-control" name = "assign_d_id" id="exampleInputAddress">
						<option value = "0" selected>Select Delivery Boy </option>
						@foreach($result['deliveryBoys'] as $d)
						<option value = "{{$d->id}}">{{$d->name}}</option>			
						@endforeach
						<select>
                      </div>
					@endif
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>


@endsection
@section('script')
    
@endsection