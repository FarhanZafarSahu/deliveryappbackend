@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
{{-- <a href="{{route('route.create')}}" style="background-color:blue; color:azure; padding:10px; "><i class="fa fa-plus" aria-hidden="true"></i> Add Product</a> --}}
            <div>
              {{-- <h4 class="float-left">Customer Details</h4> --}}
              {{-- <div class="float-right"style="display: inline-flex;">
                   <a title="Add customer" id = "addCustomer" ><i class="fa fa-plus mr-1 red" style="cursor:pointer;"></i></a>
                <!-- <button type="button" class="btn btn-secondary" id = "addBanner">Add Banner</button> -->
              </div> --}}
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th># </th>
                <th>Delivery Boy </th>
                <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['deliveryBoys'] as $index => $data)
                <tr>
                  <td>{{$data->id}}</td>
 				  <td>{{$data->name}}</td>
		 		 <td>
                    <a href="{{route('route.create',$data->id)}}"  class="add" id = "editCoupon"><i class="fas fa-plus mr-1 edit-btn"></i></a>
                    <a href="{{route('route.show',$data->id)}}"  class="view" id = "editCoupon"><i class="far fa-eye"></i></a> 				
                    <a href="{{route('route.assign-deliveryboy-route',$data->id)}}"  class="view" id = "editCoupon"><i class="far fa-eye"></i></a> 				
					 <a title="Delete"><i onclick="delete_confirmation('{{url('/route/delete',$data->id)}}')" class="fa fa-trash mr-1 red" style="cursor:pointer;"></i></a>           
 				</td>
         
        		</tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
    
@endsection