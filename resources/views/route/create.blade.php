@extends('portal.layouts.app')

@section('content')
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Route</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Add Route </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Add Route</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('route.store')}}" method = "POST" enctype="multipart/form-data" >
				@csrf
				<input type = "hidden" name = "user_id" value="{{$result['deliveryBoys']->id}}">
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputName">Route Name</label>
                    <input type="name" name = "route_name" class="form-control" id="exampleInputName" placeholder="Enter Location">
                  </div>
 					 <div class="form-group">
                        <label for="exampleInputAddress">Selected Delivery Boy</label>
                        <input type="text" name = "user_name" class="form-control" id="exampleInputName" value = "{{$result['deliveryBoys']->name}}" readonly>
                      </div>
					@if($result['deliveryBoys']->location->count() != 0)
					 <div class="form-group">
                        <label for="exampleInputAddress">Location Before</label>
                        <select class="form-control" name = "route_sequence" id="exampleInputAddress">
						<option value = "0" selected>Select Location </option>			
						
						@foreach($result['deliveryBoys']->location as $route)
						<option value = "{{$route->route_sequence}}">{{$route->route_name}}</option>			
						@endforeach
						<select>
                      </div>
					@endif
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>


@endsection
@section('script')
    
@endsection