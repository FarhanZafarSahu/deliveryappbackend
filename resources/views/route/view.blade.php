@extends('portal.layouts.app')

@section('content')
 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">

          <div class="col-sm-3">
            <h1>Timeline</h1>
          </div>
          <div class="col-sm-">
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Timeline</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <!-- Timelime example  -->
        <div class="row">
          <div class="col-md-12">
            <!-- The time line -->
            <div class="timeline">
              <!-- timeline time label -->
              <div class="time-label">
                <span class="bg-red">{{\Carbon\Carbon::now()->format('Y-M-d')}}</span>
              </div>
              <!-- /.timeline-label -->
              <!-- timeline item -->
			@foreach($result['deliveryBoys']->locationAsc as $key => $value)
              <div>
                <i class="fas fa-envelope bg-blue">{{$key+1}}</i>
                <div class="timeline-item">
                  {{-- <span class="time"><i class="fas fa-clock"></i> 12:05</span> --}}
                  <h3 class="timeline-header"><a href="#">{{$value->route_name}}</a> <strong> is your {{$key+1}} Location</strong></h3>

                  <div class="timeline-body">

                  </div>
                </div>
              </div>
			@endforeach
              <!-- /.timeline-label -->
              </div>
          </div>
          <!-- /.col -->
        </div>
      </div>
      <!-- /.timeline -->

    </section>
    <!-- /.content -->
@endsection
@section('script')
    
@endsection