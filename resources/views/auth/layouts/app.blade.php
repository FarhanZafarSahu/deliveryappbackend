<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{env('APP_NAME')}}</title>
    
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/public/portal/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('/public/portal/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/public/portal/dist/css/adminlte.min.css') }}">

    <!-- Custom style -->
    <link rel="stylesheet" href="{{ asset('/public/css/custom/auth.css') }}">

    <!-- Toastr -->
    <link rel="stylesheet" href="{{asset('/public/portal/plugins/toastr/toastr.min.css')}}">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('/public/portal/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

</head>
<body class="hold-transition login-page main-background">

    @yield('content')
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <!-- jQuery -->
    <script src="{{ asset('/public/portal/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/public/portal/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/public/portal/dist/js/adminlte.min.js') }}"></script>

	<script src ="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script>
	{{-- <script src ="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script> --}}
	<script src ="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <!-- Toastr -->
    <script src="{{asset('/public/portal/plugins/toastr/toastr.min.js')}}"></script>

    <!-- SweetAlert2 -->
    <script src="{{asset('/public/portal/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

    <script type="text/javascript">

        var Toast = Swal.mixin({
            toast: true,
            position: 'top',
            showConfirmButton: false,
            timer: 3000
        });

        function toast_error(message){
            toastr.error(message)
        }

        function toast_success(message){
            toastr.success(message)
        }

        function toast_info(message){
            toastr.info(message)
        }

        function sweet_alert_info(message)
        {
            Toast.fire({
                icon: 'info',
                title: message
            });
        }
    </script>

    @if(session('success'))
    <script type="text/javascript"> 
        toast_success("{{session('success')}}")
    </script>
    @endif

    @if(session('error'))
    <script type="text/javascript"> 
        toast_error("{{session('error')}}")
    </script>
    @endif
    
    @yield('script')
</body>
</html>
