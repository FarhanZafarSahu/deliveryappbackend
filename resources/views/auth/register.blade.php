@extends('auth.layouts.app')

@section('content')
<div class="register-box register-box-width">
    <div class="card">
        <div class="card-body">
            <form id="register-form" method="POST" action="{{ route('register_farm') }}">
                @csrf
                <div id="step-1">
                    <p class="multi-form-heading">Login Info</p>
                    <hr>
                    <label>Email</label>
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" id="email" name="email" placeholder="abc@example.com">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <label>Password</label>
                    <i class="fa fa-info-circle glowingHelp" onclick="showPasswordRules()" aria-hidden="true" title="(Hint: Password must be greater than 6 character. It must contain at least one digit and upper case letter and should contain one special character like  !,@,#,$,%,^,&,*)" ></i>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="password" name="password" placeholder="******">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <label>Confirm Password</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="******">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <p class="mt-1 mb-0">
                                <a href="{{ route('login') }}" class="text-center">Already have an account? Login</a>
                            </p>
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary btn-block" onclick="next(1)">Next</button>
                        </div>
                    </div>
                </div>
                <div id="step-2" class="hidden">
                    <p class="multi-form-heading">Primary Info</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <label>First Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="primary_first_name" name="primary_first_name" placeholder="John">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Last Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="primary_last_name" name="primary_last_name" placeholder="Doe">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="primary_email" name="primary_email" placeholder="abc@example.com" readonly>
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Phone Number</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="primary_phone" name="primary_phone" placeholder="+111111111">
                            </div>  
                        </div>
                    </div>

                    <p class="multi-form-heading">Secondary Info</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <label>First Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="secondary_first_name" name="secondary_first_name" placeholder="John">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Last Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="secondary_last_name" name="secondary_last_name" placeholder="Doe">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="secondary_email" name="secondary_email" placeholder="abc@example.com">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Phone Number</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="secondary_phone" name="secondary_phone" placeholder="+111111111">
                            </div>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <button type="button" class="btn btn-primary btn-block" onclick="back(2)">Back</button>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary btn-block" onclick="next(2)">Next</button>
                        </div>
                    </div>
                </div>
                <div id="step-3" class="hidden">
                    <p class="multi-form-heading">Farm Info</p>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Name</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_name" name="farm_name" placeholder="ABC">
                            </div>  
                        </div>
                        <div class="col-md-12">
                            <label>Address</label>
                            <div class="input-group mb-3">
                                <textarea type="text" class="form-control" id="farm_address" name="farm_address" placeholder="ABC"></textarea>
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>City</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_city" name="farm_city" placeholder="ABC">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>State</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_state" name="farm_state" placeholder="ABC">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Zip</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_zip" name="farm_zip" placeholder="00000">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Phone</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_phone" name="farm_phone" placeholder="+111111111">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Email</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_email" name="farm_email" placeholder="abc@example.com">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <label>Primary Contact</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="farm_primary_contact" name="farm_primary_contact" placeholder="+111111111">
                            </div>  
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="terms_condition" name="terms_condition">
                                <label class="form-check-label" for="terms_condition">
                                    I accept the <a href="https://www.d1solutionsdev.com/files/Documents/D1Solutions-Service-Agreement.pdf">Terms & Conditions</a>
                                </label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="privacy_policy" name="privacy_policy">
                                <label class="form-check-label" for="privacy_policy">
                                    I read the <a href="https://www.d1solutionsdev.com/files/Documents/D1-Solutions-Privacy-Policy.pdf">Privacy Policy</a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-4">
                            <button type="button" class="btn btn-primary btn-block" onclick="back(3)">Back</button>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary btn-block" onclick="next(3)">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.form-box -->
    </div><!-- /.card -->
</div>
<!-- /.register-box -->
@endsection
@section('script')
<script type="text/javascript">

    function next(n)
    {
        if(!steps_validations(n))
        {
            if(n==3)
            {
                $( "#register-form" ).submit();
            }
            else
            {
                $('#step-'+n).slideUp(500);
                $('#step-'+(n+1)).slideDown(500);
            }
        }
    }

    function back(n)
    {
        var errorStatus = false;
        if( errorStatus==false)
        {
            $('#step-'+n).slideUp(500);
            $('#step-'+(n-1)).slideDown(500);
        }
    }

    function steps_validations(n)
    {
        if(n==1)
            return first_step_validation();
        else if(n==2)
            return second_step_validation();
        else if(n==3)
            return third_step_validation();
        else
            return false;
    }

    function first_step_validation()
    {
        var errorStatus = false;
        if($('#email').val()=="")
        {
            toast_error("Email is required!");
            errorStatus = true;
        }
        else
        {
            if(isCorrectEmail($('#email').val())==0)
            {
                toast_error("Please enter a valid Email address!");
                errorStatus = true;
            }
        }

        if($('#password').val()=="")
        {
            toast_error("Password is required!");
            errorStatus = true;
        }
        else
        {
            if(isCorrectPassword($('#password').val())==0)
            {
                toast_error("Password must be greater than 6 character. It must contain at least one digit and upper case letter and should contain one special character like  !,@,#,$,%,^,&,*");
                errorStatus = true;
            }
        }

        if($('#confirm_password').val()=="")
        {
            toast_error("Confirm Password is required!");
            errorStatus = true;
        }
        else
        {
            if($('#password').val()!="")
            {
                if($('#password').val()!=$('#confirm_password').val())
                {
                    toast_error("Password does not match!");
                    errorStatus = true;
                }
            }
        }

        if(!errorStatus)
            $('#primary_email').val($('#email').val());

        return errorStatus;
    }

    function second_step_validation()
    {
        var errorStatus = false;
        if($('#primary_first_name').val()=="")
        {
            toast_error("Primary first name is required!");
            errorStatus = true;
        }

        if($('#primary_last_name').val()=="")
        {
            toast_error("Primary last name is required!");
            errorStatus = true;
        }

        if($('#primary_phone').val()=="")
        {
            toast_error("Primary phone number is required!");
            errorStatus = true;
        }

            // Secondary Info Validations

            if($('#secondary_first_name').val()=="")
            {
                toast_error("Secondary first name is required!");
                errorStatus = true;
            }

            if($('#secondary_last_name').val()=="")
            {
                toast_error("Secondary last name is required!");
                errorStatus = true;
            }

            if($('#secondary_email').val()=="")
            {
                toast_error("Secondary email is required!");
                errorStatus = true;
            }
            else
            {
                if(isCorrectEmail($('#secondary_email').val())==0)
                {
                    toast_error("Please enter a valid secondary email address!");
                    errorStatus = true;
                }
            }

            if($('#secondary_phone').val()=="")
            {
                toast_error("Secondary phone number is required!");
                errorStatus = true;
            }

            return errorStatus;
        }

        function third_step_validation()
        {
            var errorStatus = false;
            
            if($('#farm_name').val()=="")
            {
                toast_error("Name is required!");
                errorStatus = true;
            }

            if($('#farm_address').val()=="")
            {
                toast_error("Address is required!");
                errorStatus = true;
            }

            if($('#farm_city').val()=="")
            {
                toast_error("City is required!");
                errorStatus = true;
            }

            if($('#farm_state').val()=="")
            {
                toast_error("State is required!");
                errorStatus = true;
            }

            if($('#farm_zip').val()=="")
            {
                toast_error("Zip is required!");
                errorStatus = true;
            }

            if($('#farm_phone').val()=="")
            {
                toast_error("Phone is required!");
                errorStatus = true;
            }

            if($('#farm_email').val()=="")
            {
                toast_error("Email is required!");
                errorStatus = true;
            }
            else
            {
                if(isCorrectEmail($('#farm_email').val())==0)
                {
                    toast_error("Please enter a valid Email address!");
                    errorStatus = true;
                }
            }

            if($('#farm_primary_contact').val()=="")
            {
                toast_error("Primary Contact is required!");
                errorStatus = true;
            }

            if(!$('#terms_condition').is(":checked"))
            {
                toast_error("Please read and agree to Terms & Conditions!");
                errorStatus = true;
            }

            if(!$('#privacy_policy').is(":checked"))
            {
                toast_error("Please read and check Privacy Policy!");
                errorStatus = true;
            }

            return errorStatus;
        }

        function  isCorrectEmail(email)
        {
            var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            
            if (reg.test(email))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        function  isCorrectPassword(password)
        {
            var reg = /^(?=.*[!@#$%^&*0-9])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
            // (?=.*[0-9])
            if (reg.test(password))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        function showPasswordRules()
        {
            sweet_alert_info("Password must be greater than 6 character. It must contain at least one digit and upper case letter and should contain one special character like  !,@,#,$,%,^,&,*");
        }

    </script>
    @endsection