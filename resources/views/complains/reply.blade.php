@extends('portal.layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Add Complain</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('complain.response', $complain->id) }}" method="POST" class="card-box" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputBName">Your Response</label>
                            <textarea id="editor" class="editor" name="message"></textarea>
                        </div>
                </div>
                <div class="form-group pr-3">
                    <a href="{{ route('complain.list') }}"
                        class="btn btn-danger btn-sm text-light px-4 mt-3 float-right mb-0 ml-2">Cancel</a>
                    <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">
                        Respond Now</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- /.content -->

    <!-- /.content-wrapper -->

@endsection
