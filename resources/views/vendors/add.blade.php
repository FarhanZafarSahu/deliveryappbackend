@extends('portal.layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Add Vendor</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('vendor.save') }}" method="POST" class="card-box" autocomplete="off" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputName">Name</label>
                            <input type="name" name="name" class="form-control" id="exampleInputName"
                                placeholder="Enter Name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Mobile</label>
                            <input type="name" name="phone_number" class="form-control" id="exampleInputNum"
                                placeholder="Enter Mobile number" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBName">Brand Name</label>
                            <input type="name" name="brand_name" class="form-control" id="exampleInputBName"
                                placeholder="Enter Brand Name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBusName">Business Name</label>
                            <input type="name" name="business_name" class="form-control" id="exampleInputBusName"
                                placeholder="Enter Business Name" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAddress">Business Address</label>
                            <input type="name" name="business_address" class="form-control" id="exampleInputAddress"
                                placeholder="Enter Address" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPincode">Pincode</label>
                            <input type="name" name="pin_code" class="form-control" id="exampleInputPincode"
                                placeholder="Enter Business Pincode" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">Alternative Mobile Number</label>
                            <input type="name" name="alternative_phone_number" class="form-control" id="exampleInputAnum"
                                placeholder="Alternative Mobile Number">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">Sale Price</label>
                            <input type="number" name="sale_price" class="form-control" id="exampleInputAnum"
                                placeholder="Sale Price">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">License No.</label>
                            <input type="name" name="license_no" class="form-control" id="exampleInputAnum"
                                placeholder="License Number">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">Upload Document.</label>
                            <input type="file" name="document" class="form-control" >
                        </div>
                </div>
                <input type="button" class="btn btn-primary" id="addbank"  value="Add Bank Details">
                <div class="row" id="bank">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputAnum">Bank Name</label>
                                    <input type="name" name="bank_name" class="form-control" id="exampleInputAnum"
                                        placeholder="Bank Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputAnum">Branch Name</label>
                                    <input type="name" name="branch_name" class="form-control" id="exampleInputAnum"
                                        placeholder="Branch Name">
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">Account Holder Name</label>
                                        <input type="name" name="account_holder" class="form-control" id="exampleInputAnum"
                                            placeholder="Account Holder Name">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">Account  Number</label>
                                        <input type="name" name="account_number" class="form-control" id="exampleInputAnum"
                                            placeholder="Account Number">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">ISFC Code</label>
                                        <input type="name" name="IFSC_code" class="form-control" id="exampleInputAnum"
                                            placeholder="ISFC Code">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">Account  Type</label>
                                        <input type="name" name="account_type" class="form-control" id="exampleInputAnum"
                                            placeholder="Account Type">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group pr-3">
                    <a href="{{ route('vendor.list') }}"
                        class="btn btn-danger btn-sm text-light px-4 mt-3 float-right mb-0 ml-2">Cancel</a>
                    <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- /.content -->

    <!-- /.content-wrapper -->

@endsection
@section('script')
<script>
    $(document).ready(function(){
  $("#addbank").click(function(){
    $("#bank").toggle();
  });
});
</script>
@endsection
