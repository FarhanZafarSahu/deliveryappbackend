@extends('portal.layouts.app')

@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Edit Vendor</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('vendor.save', $vendor->id) }}" method="POST" class="card-box"
                        autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputName">Name</label>
                            <input type="name" name="name" class="form-control" id="exampleInputName"
                                value="{{ $vendor->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                value="{{ $vendor->email }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputName">Mobile</label>
                            <input type="name" name="phone_number" class="form-control" id="exampleInputNum"
                                value="{{ $vendor->phone_number }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBName">Brand Name</label>
                            <input type="name" name="brand_name" class="form-control" id="exampleInputBName"
                                value="{{ $vendor->brand_name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputBusName">Business Name</label>
                            <input type="name" name="business_name" class="form-control" id="exampleInputBusName"
                                value="{{ $vendor->business_name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAddress">Business Address</label>
                            <input type="name" name="business_address" class="form-control" id="exampleInputAddress"
                                value="{{ $vendor->business_address }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPincode">Pincode</label>
                            <input type="name" name="pin_code" class="form-control" id="exampleInputPincode"
                                value="{{ $vendor->pin_code }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">Alternative Mobile Number</label>
                            <input type="name" name="alternative_phone_number" class="form-control" id="exampleInputAnum"
                                value="{{ $vendor->alternative_phone_number }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">Sale Price</label>
                            <input type="number" name="sale_price" class="form-control" value="{{ $vendor->sale_price }}"
                                placeholder="Sale Price">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputAnum">License No.</label>
                            <input type="name" name="license_no" class="form-control" value="{{ $vendor->license_no }}"
                                placeholder="License Number">
                        </div>
                        {{-- <div class="form-group">
                            <label for="exampleInputAnum">Upload Document.</label>
                            <input type="name" name="document" class="form-control" value="">
                        </div> --}}
                        <input type="button" class="btn btn-primary" id="addbank"  value="Add Bank Details">
                <div class="row" id="bank">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputAnum">Bank Name</label>
                                    <input type="name" name="bank_name" class="form-control" id="exampleInputAnum"
                                        placeholder="Bank Name" value="{{ $vendor->bankDetails->bank_name ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputAnum">Branch Name</label>
                                    <input type="name" name="branch_name" class="form-control" value="{{ $vendor->bankDetails->branch_name ?? '' }}"
                                        placeholder="Branch Name">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputAnum">Account Holder Name</label>
                                    <input type="name" name="account_holder" class="form-control" id="exampleInputAnum"
                                        placeholder="Account Holder Name" value="{{ $vendor->bankDetails->account_holder ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputAnum">Account  Number</label>
                                    <input type="name" name="account_number" class="form-control" id="exampleInputAnum"
                                        placeholder="Account Number" value="{{ $vendor->bankDetails->account_number ?? '' }}">
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">ISFC Code</label>
                                        <input type="name" name="IFSC_code" class="form-control" value="{{ $vendor->bankDetails->IFSC_code ?? '' }}"
                                            placeholder="ISFC Code">
                                    </div>
                                    <div class="form-group col-md-6 col-lg-6">
                                        <label for="exampleInputAnum">Account  Type</label>
                                        <input type="name" name="account_type" class="form-control" value="{{ $vendor->bankDetails->account_type ?? '' }}"
                                            placeholder="Account Type">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group pr-3">
                    <a href="{{ route('vendor.list') }}"
                        class="btn btn-danger btn-sm text-light px-4 mt-3 float-right mb-0 ml-2">Cancel</a>
                    <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <!-- /.content -->

    <!-- /.content-wrapper -->

@endsection
