@extends('portal.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Vendor Doscument</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
<a href="{{ route('vendor.list') }}" class="btn btn-primary mr-4 mb-2" style="float: right">Back To List</a>
<iframe src="{{ asset('/storage/'.$userDoc) }}" width="96%" height="500" class="ml-3 mr-3"></iframe>

@endsection
@section('script')

@endsection


