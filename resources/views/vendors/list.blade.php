@extends('portal.layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">Vendors List</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="row">
    <div class=" m-2 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-4 align-self-center">
                        <div class="icon-info">
                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                        </div>
                    </div>
                   <div class="col-8 align-self-center text-right">
                            <div class="ml-2">
                                <p class="mb-1 text-muted">Total Vendors </p>
                                <input class=" h3 mt-0 mb-1 font-weight-semibold text-center" name="" id=""
                                    value="{{ totalVendor() }}" readonly
                                    style="background: transparent; border: none;margin-left: -30px">
                            </div>
                        </div>
                </div>
            </div>
            <!--end card-body-->
        </div>
        <!--end card-->
    </div>
</div>
<!-- Main content -->
 <div class="row">
		@if(\Auth::user()->role_id == 2 || \Auth::user()->role_id == 1)
        <div class="col-md-12 text-right pr-3">
            <a href="{{ route('vendor.add') }}" class="btn btn-primary px-4 mt-0 mb-3"><i
                    class="mdi mdi-plus-circle-outline mr-2"></i>Add New</a>
        </div>
		@endif
    <div class="  col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="table-data" class="table table-bordered table-striped">
                     <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Approved</th>
                                    <th>Active</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                <!--end tr-->
                            </thead>
                            <tbody>
                                @foreach ($vendors as $index => $data)
                                    <tr>
                                        <td>
                                            <img src=" {{ asset('public') . '/' . $data->photo }} " alt="vendor photo"
                                                style="height: 50px;width:50px; border-radius:15px">
                                        </td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->is_approved }}</td>
                                        <td>{{ $data->is_active }}</td>
                                        <td>
                                            <a href="{{ route('vendor.document.view', $data->id) }}"
                                                class="btn btn-sm btn-primary">document</a>
                                                <a href="{{ route('vendor.detail', $data->id) }}"
                                                    class="btn btn-sm btn-info">details</a>
                                            <a class="btn btn-sm btn-success"
                                                onclick="status_confirmation('{{ url('/vendors/active', $data->id) }}')">{{ $data->is_active == 1 ? 'deactive' : 'active' }}</a>
                                            @if (\Auth::user()->role_id == 1)
                                                <a class="btn btn-sm btn-danger"
                                                    onclick="status_confirmation('{{ url('/vendors/approve', $data->id) }}')">{{ $data->is_approved == 1 ? 'disable' : 'enable' }}</a>
                                            @endif
                                            <a href="{{ route('vendor.edit', $data->id) }}"
                                                class="btn btn-sm btn-primary">edit</a>
                                            <a class="btn btn-sm btn-danger"
                                                onclick="delete_confirmation('{{ url('/vendors/delete', $data->id) }}')">delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
          
                </table>
              </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection
