@extends('portal.layouts.app')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Vendor Details</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('vendor.list') }}">Vendors</a></li>
                    <li class="breadcrumb-item active">Vendor Details</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- Main content -->
    <section class="content-header">
    </section>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            {{-- <br><br> --}}
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        {{-- <div class="card-header">
                            <div>
                                <h4>Vendor Details </h4>
                            </div>
                        </div> --}}

                        <div class="row m-0 mt-2">
                            <div class="col-lg-12 col-md-12 col-sm-12">

                                <div class="callout callout-danger"
                                    style="background:lightsteelblue;border-top: 3px solid #007bff;border-bottom: 3px solid #007bff;border-right: 3px solid #007bff;border-left: 3px solid #007bff !important;">
                                    <h5><i class="fas fa-info"></i> Vendor Details:</h5>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Vendor Name :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->name ?? '' }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Vendor Email :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->email }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Vendor Phone :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->phone_number }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Alternative Phone # :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->alternative_phone_number }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Brand Name :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->brand_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Business  Name :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->business_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Business Address :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->business_address }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Pin Code # :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->pin_code }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Sale Price :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->sale_price }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            License No # :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->license_no }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col-md-12 -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div>
                                <h4>Vendor Bank Details </h4>
                            </div>
                        </div>

                        <div class="row m-0 mt-2">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="callout callout-danger"
                                    style="background:lightsteelblue;border-top: 3px solid #007bff;border-bottom: 3px solid #007bff;border-right: 3px solid #007bff;border-left: 3px solid #007bff !important;">
                                    <h5><i class="fas fa-info"></i> Bank Information:</h5>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Bank Name :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->bank_name ?? '' }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Account Holder :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->account_holder ?? '' }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Account Number :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->account_number ?? ''}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Branch Name  :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->branch_name ?? ''}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            IFSC Code #:
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->IFSC_code ?? ''}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Account Type  :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            {{ $vendor->bankDetails->account_type ?? ''}}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </div>
@endsection
