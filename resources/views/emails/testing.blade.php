<!DOCTYPE html>
<html>
<head>
    <title>Comapny Name</title>
</head>
<body style="background-color: #EEEEEE !important;">
    <div style="padding-top: 5% !important;">
        <div style="background-color: white !important; margin: 1% 8% 1% 8% !important; box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19) !important;color: black !important">
            <br>
            <div style="min-width: 100% !important; text-align: -webkit-center !important;">
            <h1 style="color:#ff5b5b;font-weight: 900;font-size: 60px;line-height: 1;text-align:center">Company Name</h1>
            </div>  
            <hr style="margin: 3% 10% 3% 10% !important;">
            <div style="padding-right: 10% !important; padding-left: 10% !important;">
       <h3>Dear Farhan Zafar</h3>
            <p>Stay Awesome,<br>Company Name<br><u>{{env('MAIL_FROM_ADDRESS')}}</u></p>
            </div>
            <br>
        </div>
    </div>  
    <br>
    <div style="min-width: 100% !important; text-align: -webkit-center !important;">
        <br>
        <p style="text-align: center;color:black !important">Copyright© 2022 Company Name Inc, All rights reserved.</p>
    </div>
    <br>
</body>
</html>