
@extends('portal.layouts.app')

@section('content')

 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Feedback List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <!-- /.content -->
    <div class="row">
        <div class=" m-2 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 align-self-center">
                            <div class="icon-info">
                                <i class="fa fa-user fa-2x" aria-hidden="true" ></i>
                            </div>
                        </div>
                        <div class="col-8 align-self-center text-right">
                            <div class="ml-2">
                                <p class="mb-1 text-muted">Total Feedback </p>
                                <input class=" h3 mt-0 mb-1 font-weight-semibold text-center" name="" id=""
                                    value="{{ totalFeedback() }}" readonly
                                    style="background: transparent; border: none;margin-left: -30px">
                            </div>
                        </div>
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
    </div>

    <div class="row">
        @if(auth()->user()->id != 1)
        <div class="col-md-12 text-right pr-3">
            <a href="{{ route('feedback.add') }}" class="btn btn-primary px-4 mt-0 mb-3"><i
                    class="mdi mdi-plus-circle-outline mr-2"></i>Add New</a>
          </div>
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Phone Number</th>
                                    <th>Feedback</th>
                                    <th>Reply</th>
                                    @if(auth()->user()->id==1)
                                    <th class="text-right">Action</th>
                                    @endif
                                </tr>
                                <!--end tr-->
                            </thead>
                            <tbody>
                                @foreach ($feedback as $index => $data)
                                @if(auth()->user()->id==1 || $data->user_id==auth()->user()->id)
                                   <tr>
                                       <td>{{ $data->name }}</td>
                                       <td>{{ $data->phone_number }}</td>
                                       <td>{!! $data->feedback !!}</td>
                                       <td>{!! $data->feedback_reply !!}</td>
                                       <td>
                                           @if(Auth::user()->role_id==1)
                                            <a href="{{ route('feedback.reply', $data->id) }}" class="btn btn-sm btn-primary">Pay Thanks</a>
                                           @endif

                                        </td>
                                   </tr>
                                   @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- /.content-wrapper -->

@endsection
