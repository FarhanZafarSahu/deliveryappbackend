@extends('delivery-boy.layouts.app')
@section('title', 'Todays Delivery')
@section('page_title', 'Todays Delivery')
@section('content')
    <!-- Main -->
    <main class="site_main bg-white">
        <div class="d-flex justify-content-between">
            <div class="d-flex">
                <img src="images/alls-groups.png" alt="" width="30">
                <span class="ps-3">Daily Paying Customer Collection</span>
            </div>
            <div class="d-flex pe-3">
                <i class="fa-solid fa-indian-rupee-sign d-flex align-items-center pe-2 pt-1"></i>
                <span class="pt-1">0</span>
            </div>
        </div>
        <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <img src="images/all-groups-calender%20(1).png" alt="" width="30">
                <span class="ps-3">Monthly Paying Customer Collection</span>
            </div>
            <div class="d-flex pe-3">
                <i class="fa-solid fa-indian-rupee-sign d-flex align-items-center pe-2 pt-1"></i>
                <span class="pt-1">0</span>
            </div>
        </div>
        <div class="total-jar d-flex justify-content-between pt-3 ps-2 pe-3">
            <div><span>Total Jar Delivered</span></div>
            <div class="d-flex">
                <div class="container d-flex" style="border-right: 1px solid;">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg"
                        alt="" width="30">
                    <span class="pt-1">0</span>
                </div>
                <div class="d-flex">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg"
                        alt="" width="30">
                    <span class="pt-1">0</span>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between px-2 pt-3">
            <div>
                <a href="#" class="text-decoration-none">{{ $group->name ?? '' }}</a><br>
                <span class="text-primary">({{ isset($group->customerUser) ? $customers->count() : 0 }} Customers)</span>
            </div>
            <div class="d-flex">
                <a href="#" class="pe-3"><img src="images/User_Circle.png" alt="" width="30"></a>
                <a href="#"><img src="images/mobile-data.png" alt="" width="30"></a>
            </div>
        </div>
        <form action="{{ route('delivery-boy.today.delivery') }}" method="GET" class="mt-3">
            <div class="input-group border-theme rounded p-1">
                <span class="input-group-text bg-white border-0 pe-0">
                    <span class="material-icons">search</span>
                </span>
                <input type="search" name="search" value = "{{$search}}"class="form-control border-0" placeholder="Search">
            </div>
        </form>
        @if (isset($group))
            @forelse($customers as $key => $data)
                <div class="d-flex justify-content-between pt-3">
                    <div class="d-flex">
                        <div class="fs-3 pe-3"><i class="fa-solid fa-user"></i></div>
                        <div class="text-secondary">
                            <span><b>{{ $data['customer']->name }}</b></span><br>
                            <span><b>{{ $data['customer']->phone }}</b></span>
                        </div>
                    </div>
                    <div class="pe-2">
                        <div class="d-flex">
                            <img src="images/User_Circle.png" alt="" width="30">
                            <span class="pt-1 ps-2">0</span>
                        </div>
                        <span>Jar Bal.</span>
                        <div class="d-flex">
                            <img src="images/User_Circle.png" alt="" width="30">
                            <span class="pt-1 ps-2">0</span>
                        </div>

                    </div>
                </div>
            @empty
            @endforelse
        @endif
    </main>
    <!-- Main Close -->
@endsection
