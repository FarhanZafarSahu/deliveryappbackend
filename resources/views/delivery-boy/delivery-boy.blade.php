@extends('delivery-boy.layouts.app')
@section('title','Delivery Boy')
@section('page_title',auth('deliveryboy')->user()->name)
@section('content')
    <!-- Main -->
    <main class="site_main bg-white px-3">
        <div class="row g-3 text-center">
            <div class="col-6">
                <a href="{{ route('delivery-boy.today.delivery') }}" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <i class="fa-solid fa-truck"></i>
                            </div>
                            <div class="mt-2 fw-bold">Today Delivery</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6">
                <a href="{{ route('delivery-boy.payments') }}" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">paid</span>
                            </div>
                            <div class="mt-2 fw-bold">Payment</div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </main>
  @endsection