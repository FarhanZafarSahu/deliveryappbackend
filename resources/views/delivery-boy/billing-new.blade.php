@extends('delivery-boy.layouts.app')
@section('title','Todays Delivery')
@section('page_title','Billing New')

    <!-- Main -->
    <main class="site_main bg-white">
        <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <div class="icon2 fs-3 me-3 pt-1"><a href="#" class="text-decoration-none"><i class="fa-solid fa-user-pen" style="color: #000;"></i></a></div>
                <div class="text-secondary">
                    <span><b>Abhishek Gupta Nikhil C16 Nirmaldhara</b></span><br>
                    <span><b>Mobile Number</b></span>
                </div>
            </div>
            <div class="pe-2">
                <a href="#"><img src="images/barcode.webp" alt="" width="30"></a>
                <span><a href="#" class="text-p text-decoration-none">Scan paytm/ PhonePe/ GPay</a></span>
            </div>
        </div>

        <div class="d-flex justify-content-between pt-3">
            <span><b>22 Dec 2021</b></span>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <i class="fa-solid fa-file-circle-check pe-2"></i><span>Past Payment</span>
            </button>

            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title w-75 d-flex justify-content-center ps-5 ms-5" id="staticBackdropLabel">Select Date</h5>
                            <hr>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex justify-content-center">
                                <input type="date">
                            </div>
                            <hr>
                            <div class="d-flex justify-content-center pt-4">
                                <input type="text" placeholder="Enter Payment" class="int py-3 w-100">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr style="#0F0A44">
        <div class="past">
            <span class="text-dark"><b>Payment Amount</b></span>
            <div class="pt-3 d-flex justify-content-between">
                <span>Send WhatsApp Message For Entry?</span>
                <div class="d-flex">
                    <span class="d-flex align-items-center pe-2 pt-1">NO</span>
                    <div class="check pe-2">
                        <input type="checkbox" id="checkbox1" />
                        <label for="checkbox1"></label>
                    </div>
                    <span class="d-flex align-items-center pe-2 pt-1">Yes</span>
                </div>

            </div>
        </div>
        <div class="bill">
            <div class="d-flex justify-content-between pt-3 px-3">
                <div class="d-flex">
                    <span><b>November 2021 Bill</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end">
                        <span>100</span>
                    </div>
                </div>
            </div>
            <div class="b-amount d-flex justify-content-between pt-2 px-3">
                <div class="d-flex">
                    <span><b>Balance Amount</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end">
                        <span>-10</span>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between pt-2 px-3">
                <div class="d-flex">
                    <span><b>Total Amount</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end">
                        <span>90</span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="text-center pt-4">
            <button class="btn btn-theme w-50" type="submit">Submit</button>
        </div>

    </main>
    @endsection
