@extends('delivery-boy.layouts.app')
@section('title','Payments')
@section('page_title','Payments')
@section('content')
    <!-- Main -->
    <main class="site_main bg-white">
        <h4>All Customers <span class="fs-5">( {{isset($group->customerUser) ? $group->customerUser->count() : 0 }} Customers)</span></h4>
        <form action="{{ route('delivery-boy.payments') }}" method="GET" class="mt-3 shadow-sm">
            <div class="input-group border-theme rounded p-1">
                <span class="input-group-text bg-white border-0 pe-0">
                    <span class="material-icons">search</span>
                </span>
                <input type="search"  name ="search" value = "{{$search}}" class="form-control border-0" placeholder="Search by name or phone number">
            </div>
        </form>
        {{-- <div class="mt-5 d-flex justify-content-between px-5 shadow-sm">
            <div class="filter1 w-50">
                <a href="#" class="text-decoration-none" style="color: #0F0A44;">Filter<i class="fa-solid fa-sliders ps-5"></i></a>
            </div>
            <div>
                <select class="form-select  border-0" aria-label=".form-select example">
                    <option selected>Select Option</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>
        </div> --}}
        <div class="d-flex justify-content-between mt-5">
            <span class="fs-5"><b>Customer Name</b></span>
            <span class="fs-5"><b>Previous Month Due/Credit</b></span>
        </div>
		@if(isset($group))
		@forelse($group->customerUser as $key => $data)
		<div class="d-flex justify-content-between mt-5 shadow-sm px-3 py-2">
            <div class="d-flex">
                <div class="icon1 fs-3 me-3"><i class="fa-solid fa-user"></i></div>
                <div class="text-secondary">
                    <span><b>{{$data->name}}</b></span><br>
                    <span><b>{{$data->phone}}</b></span>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <div>
					@php 
						
						$subscription = App\UserSubscriptionPlan::where('user_id',$data->id)->latest()->with('paymentHistory')->first();

						@endphp
                    <span class="pe-3">{{isset($subscription->paymentHistory)  ? $subscription->paymentHistory->balance_amount : 0  }}</span>
                    <i class="fa-solid fa-indian-rupee-sign pt-1"></i>
                </div>
            </div>
        </div>
       	
		@empty
			
		@endforelse
		@endif
       
    </main>
   @endsection