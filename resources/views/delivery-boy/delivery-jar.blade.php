@extends('delivery-boy.layouts.app')
@section('title','Delivery Jar')
@section('page_title',' Jar Entry')
@section('content')
    <!-- Main -->
    <main class="site_main bg-white">
        <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <div class="fs-3 pe-3 pt-1"><a href="#" class="text-decoration-none"><i class="fa-solid fa-user-pen" style="color: #000;"></i></a></div>
                <div class="text-secondary">
                    <span><b>Apurva Ji Lupin Nirmaldhara</b></span><br>
                    <span><b>Mobile Number</b></span>
                </div>
            </div>
            <div class="pe-2">
                <button type="button" class="btn btn-primary">Payment</button>
            </div>
        </div>
        <div class="balance">
            <div class="main-balance">

                <div class="icon d-flex">
                    <i class="fa-solid fa-house pt-1 pe-3"></i>
                    <span>Balance Jar</span>
                </div>
                <div class="numb d-flex align-items-center">
                    <span class="pe-2">0</span>
                    <span class="pt-1">*</span>
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <span class="pe-2">+</span>
                    <span class="pe-2">0</span>
                    <span class="pt-1">*</span>
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <span class="pe-2">=</span>
                    <span class="pe-2">0</span>
                </div>
            </div>

        </div>
        <div class="past">
            <div class="d-flex justify-content-between pt-2">
                <span><b>Jar Entry</b></span>
                <button type="button" class="btn btn-outline-primary"><i class="fa-solid fa-file-circle-check pe-2"></i><span>Past Entries</span></button>
            </div>
            <hr style="#0F0A44">
            <div>
                <span class="text-dark"><b>22 Dec 2021</b></span>
                <div class="pt-3 d-flex justify-content-between">
                    <span>Send WhatsApp Message For Entry?</span>
                    <div class="d-flex">
                        <span class="d-flex align-items-center pe-2 pt-1">NO</span>
                        <div class="check pe-2">
                            <input type="checkbox" id="checkbox1" />
                            <label for="checkbox1"></label>
                        </div>
                        <span class="d-flex align-items-center pe-2 pt-1">Yes</span>
                        <a href="#" class="pt-2"><img src="images/youtube.png" alt="" width="30"></a>
                    </div>

                </div>
            </div>
            <hr style="#0F0A44">
        </div>
        <div class="past1">
            <span><b>Blue Chilled</b></span>
            <div class="d-flex justify-content-between py-3">
                <div class="d-flex">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <i class="fa-solid fa-arrow-right pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Full Jar (Jar In)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="1">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <i class="fa-solid fa-arrow-left pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Empty Jar (Jar Out)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="1">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="past1 pt-3">
            <span><b>Blue Regular</b></span>
            <div class="d-flex justify-content-between py-3">
                <div class="d-flex">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <i class="fa-solid fa-arrow-right pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Full Jar (Jar In)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="1">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <img src="images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg" alt="" width="30">
                    <i class="fa-solid fa-arrow-left pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Empty Jar (Jar Out)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="1">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Main Close -->
@endsection