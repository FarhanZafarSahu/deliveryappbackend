<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/css/customer-style.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    @yield('css')
</head>

<body class="pb-0">
    <!-- Header -->
    <header class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
  
            <span class="material-icons d-block px-2" onclick="window.history.back()" style="cursor: pointer">arrow_back</span>
            <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar" class="btn btn-sm shadow-sm-none text-white ps-0">
                <div class="d-flex">
                    <span class="text-white ps-2">@yield('page_title')</span>
                </div>
            </a>
        </div>
        
        <div>
           {{-- logout deliveryboy with post method --}}
            <form action="{{ route('delivery-boy.logout') }}" method="post">
                @csrf
                <button type="submit" class="btn btn-sm shadow-none text-white ps-0">
                    <i class="fa-solid fa-power-off fs-3"></i>
                </button>
            </form>

        </div>
    </header>
    <!-- Header  close-->

    <!-- Sidebar -->
    <div class="offcanvas offcanvas-start sidebar border-0" tabindex="-1" id="sidebar" aria-labelledby="sidebarLabel">
        <div class="sidebar_header">
            <h1 class="sub_heading">Hi, {{ auth('deliveryboy')->user()->name }}</h1>
            <div class="text">{{ auth('deliveryboy')->user()->name }}</div>
            <div class="sidebar_divider"></div>
            <div class="d-flex align-items-center">
                <a href="profile_setting.html" class="d-flex align-items-center text-decoration-none text-white">
                    <span class="material-icons d-block text-white m-0">settings</span>
                    <strong class="ms-2">Profile Settings</strong>
                </a>
            </div>
        </div>
        <div class="offcanvas-body sidebar_main px-1">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="{{ route('delivery-boy.home') }}" class="nav-link active"><span class="material-icons">home</span> Home</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('delivery-boy.new.jar.entry') }}" class="nav-link"><span class="material-icons">payments</span> New Jar History</a>
                </li>
            </ul>
            <div class="dropdown-divider"></div>
            <strong class="text-muted ms-3">Taste & Healthy Special</strong>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="#" class="nav-link"><span class="material-icons">send</span> Refer & Earn</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Sidebar Close -->

    @yield('content')


    <!-- bootstrap.min.js -->
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}" defer></script>
    @yield('js')
</body></html>