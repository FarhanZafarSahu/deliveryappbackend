@extends('portal.layouts.app')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <a href="{{ route('slider.create') }}"
                                style="background-color:blue; color:azure; padding:10px; "><i class="fa fa-plus"
                                    aria-hidden="true"></i> Add Slider</a>
                            <div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="table-data" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sliders as $index => $data)
                                        <tr>
                                            <td><img src="{{ asset('/storage/' . $data->image) }}"
                                                    style="height: 100px; width:100px">
                                            </td>
                                            <td>{{ $data->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
