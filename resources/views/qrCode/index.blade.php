@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    {{-- <br><br> --}}
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
              <h4 class="float-left">QR Details</h4>
              <div class="float-right"style="display: inline-flex;">
                <a href="javascript:void(0)" id="addqr" class="float-right mr-2 btn btn-primary">Add QR</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>id</th>
                <th>QR Number </th>
                </tr>
              </thead>
              <tbody>
                @foreach($Qr as $index => $data)
                <tr>
                  <td>{{$data->id}}</td>
                   <td>{{$data->qr_number}}</td>
                 </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>

<!-- /.content -->
{{-- Modal ADD Account  --}}
  
<div class="modal fade" id="addqrModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addqrHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="add_qr_form" name="add_qr_form">
                @csrf
                <div>
                    <label>Enter Qty</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" id="qty" name="qty">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="addqrbtn" value="Add QR">Submit
                     </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    
<script type="text/javascript">
  $(function () {
     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
              'Content-Type': 'application/pdf'
          }
    });
     
    $('#addqr').click(function () {
        $('#addqrbtn').val("Add Account");
        $('#id').val('');
        $('#add_qr_form').trigger("reset");
        $('#addqrHeading').html("Generate QR");
        $('#addqrModal').modal('show');
    });
    
    $('#addqrbtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        var $qty = $('#qty').val();
        var $url =  "{{ url('QR/create')}}"+'?qty='+$qty;
        $('#addqrModal').modal('hide');
        location.href = $url;
    });

  });
</script>
@endsection