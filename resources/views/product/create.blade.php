@extends('portal.layouts.app')

@section('content')
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Product</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Add Product </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Add Product</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('product.store')}}" method = "POST" enctype="multipart/form-data" >
				@csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputName">Product Title</label>
                    <input type="name" name = "product_title" class="form-control" id="exampleInputName" placeholder="Enter Product Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Description</label>
                    <input type="text" class="form-control" name = "description" id="exampleInputEmail1" placeholder="Enter Description">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputName">price</label>
                      <input type="number" class="form-control" name="price" min="1.00" value="0.00" step="0.01" id="exampleInputNum" placeholder="Enter Price">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress">Product Unit</label>
                        <input type="text" class="form-control" name = "unit" id="exampleInputAddress" placeholder="Enter Product Unit">
                      </div>
  					<div class="form-group">
                        <label for="exampleInputAddress">Product Category</label>
                        <select class="form-control" name = "category_id" id="exampleInputAddress">
						@foreach($result['categories'] as $category)
						<option value = "{{$category->id}}">{{$category->name}}</option>			
						@endforeach
						<select>
                      </div>
					<div class="form-group">
                        <label for="exampleInputAddress">Product Type</label>
                        <select class="form-control" name = "product_type" id="exampleInputAddress">
						<option value = "1">Chilled Water</option>
						<option value = "2">Regular Water</option>			
						<select>
                      </div>
                    <div class="form-group">
                      <label for="exampleInputlandmark">Image</label>
                      <input type="file" class="form-control" name = "image" id="exampleInputlandmark">
                    </div>

                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>


@endsection
@section('script')
    
@endsection