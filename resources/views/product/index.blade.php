@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 tex-right">
        <div class="card card-primary card-outline ">
          <div class="card-header ">
              <a href="{{route('product.create')}}" style="background-color:blue; color:azure; padding:10px; "><i class="fa fa-plus" aria-hidden="true"></i> Add Product</a>
            <div>
              {{-- <h4 class="float-left">Customer Details</h4> --}}
              {{-- <div class="float-right"style="display: inline-flex;">
                   <a title="Add customer" id = "addCustomer" ><i class="fa fa-plus mr-1 red" style="cursor:pointer;"></i></a>
                <!-- <button type="button" class="btn btn-secondary" id = "addBanner">Add Banner</button> -->
              </div> --}}
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>Product Title</th>
                <th>Product Description</th>
                <th>Product Unit</th>
                <th>Product Price</th>
                <th>Product Category</th>

                <th>Produt Image</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['products'] as $index => $data)
                <tr>
                  <td>{{$data->product_title}}</td>
 				  <td>{{$data->description}}</td>
                  <td>{{$data->unit}}</td>
                  <td>{{$data->price}}</td>
                  <td>{{$data->category->name ?? NULL}}</td>
				  <td><img src="{{ asset('/storage/'.$data->image ) }}" alt="" style = "height:100px;width:100px">
				  </td>
        		</tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')

@endsection
