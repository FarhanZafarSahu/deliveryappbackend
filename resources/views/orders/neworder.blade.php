@extends('portal.layouts.app')
@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
	
              <h4 class="float-left">Order Details</h4>
              <div class="float-right"style="display: inline-flex;">
              <select class="form-control" id="filter" name = "filter">
                    <option   value =""selected disabled>Select Filter</option>
                    <option   value ="{{ url("order/list", 1)}}" @php if($result['filter'] == 1) echo "selected" @endphp>New Order</option>
                    <option  value ="{{ url("order/list", 2)}}" @php if($result['filter'] == 2) echo "selected" @endphp>All Other Order</option>
				</select>
              </div>
    	</div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
					<th>#Invoice No.</th>
					<th>Order Place Time.</th>
					<th>Order Delivery Date</th>
					<th>Vendor Name</th>
					<th>Product</th>
					<th>Qty</th>
					<th>Sub Total</th>
					<th>GST</th>
					<th>Total</th>
@if($result['filter'] == 1)
					<th class="text-right">Action</th>
@endif
                </tr>
              </thead>
              <tbody>
                @foreach($result['orders'] as $index => $data)
                <tr>
                  <td>{{$data->order_no}}</td>
 				  <td>{{$data->order_time}}</td>
 				  <td>{{$data->delivery_date}}</td>
                  <td>{{$data->vendor->name ?? ''}}</td>
 				  <td>{{$data->orderDetails[0]->product->product_title}}</td>
 				  <td>{{$data->quantity}}</td>
 				  <td>{{$data->discounted_price}}</td>
 				  <td>{{$data->delivery_charges}}</td>
 				  <td>{{$data->discounted_price + $data->delivery_charges}}</td>
					@if($result['filter'] == 1)
					<td>  <a class="btn btn-sm btn-success"
                                                onclick="delete_confirmation('{{ url('/order/accept', $data->id) }}' , 'Yes Accept it.')">Accept</a>
  <a class="btn btn-sm btn-danger"
                                                onclick="delete_confirmation('{{ url('/order/reject', $data->id) }}' , 'Yes Reject it.')">Reject</a>

@endif                                      
</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
<script type="text/javascript">
    $('#filter').on('change', function (e) {
        var link = $("option:selected", this).val();
        if (link) {
            location.href = link;
        }
    });
</script>
@endsection