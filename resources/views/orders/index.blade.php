@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row mb-2">
  <div class="col-sm-6">
              <h1 class="m-0">Today's Delivery</h1>
            </div><!-- /.col -->
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
		{{-- <a href="{{route('deliveryboy.create')}}" style="background-color:blue; color:azure; padding:10px; "><i class="fa fa-plus" aria-hidden="true"></i> Add DeliverBoy</a> --}}
            <div>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>#</th>
				<th>Delivery name</th>
				<th>Delivery Address</th>
				<th>Delivery Phone #</th>
				<th>Total Order Amount #</th>
				<th style = "display:none">Status</th> 
                </tr>
              </thead>
              <tbody>
                @foreach($result['orders'] as $index => $data)
                <tr>
                  <td>{{$data->id}}</td>
					<td>{{$data->delivery_name}}</td>
					<td>{{$data->delivery_address}}</td>
					<td>{{$data->delivery_mobile_number}}</td>
					<td>{{$data->discounted_price + $data->delivery_charges }}</td>
					<td style = "display:none">{{(isset($data->customer) ? $data->customer->assigned_by  : 0 == NULL && Auth::user()->role_id == 3) ? "OWN Customer" :((Auth::user()->role_id == 3 ) ? "Assign Customer" : "N/A")}}</td> 
					</tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
    <script>
</script>
@endsection