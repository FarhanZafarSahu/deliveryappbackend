@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        {{-- <br><br> --}}
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <div>
			<h1>Customer Details Form </h1>
                        </div>
                    </div>

                    <div class="row m-0 mt-2">
<div class="col-lg-3 col-md-3 col-sm-3">
</div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="callout callout-danger" style = "background:lightsteelblue;border-top: 3px solid #007bff;border-bottom: 3px solid #007bff;border-right: 3px solid #007bff;border-left: 3px solid #007bff !important;">
                                <h5><i class="fas fa-info"></i> Customer Details:</h5>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        Customer Name :
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
									{{$customer->name}}
								</div>
                                </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Customer Email :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
										{{$customer->email}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            Customer Address :
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
										{{$customer->address}}
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        Customer Contact # :
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                	{{$customer->phone}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        Customer Assigned Vednor Name :
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
											{{$customer->vendor->name ?? "Not Yet Assigned"}}
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        Customer Plan Name :
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
											{{$customer->subscriptions->plan->plan_name ?? "Not Yet Availed"}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
</div>
@endsection

