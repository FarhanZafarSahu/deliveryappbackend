@extends('portal.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Customer List</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <!-- /.content -->
    <div class="row">
        <div class=" m-2 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 align-self-center">
                            <div class="icon-info">
                                <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-8 align-self-center text-right">
                            <div class="ml-2">
                                <p class="mb-1 text-muted">Total Customers </p>
                                <input class=" h3 mt-0 mb-1 font-weight-semibold text-center" name="" id=""
                                    value="{{ $result['customer']->count() }}" readonly
                                    style="background: transparent; border: none;margin-left: -30px">
                            </div>
                        </div>
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
    </div>

    <!-- Main content -->

    <div class="row">
    
            <div class="col-md-12 text-right pr-3">
             <a href="{{ route('customer.create') }}" class="btn btn-primary px-4 mt-0 mb-3"><i
                        class="mdi mdi-plus-circle-outline mr-2"></i>Add New</a>
            </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="table-data" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Mobile #</th>
                                <th>customer Name</th>
                                <th>Customer email</th>
                                <th>Customer Address</th>
                                <th>Customer Land Mark</th>
                                <th>Customer QR</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($result['customer'] as $index => $data)
                                <tr>
                                    <td>{{ $data->phone }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->address }}</td>
                                    <td>{{ $data->land_mark }}</td>
                                    <td>
                                        <img
                                            src="https://api.qrserver.com/v1/create-qr-code/?data='{{ $data->qr }}'&amp;size=100x100">
                                    </td>
                                    <td>
                                        <a href="{{ route('customer.edit', $data->id) }}" class="edit editCoupon" id="editCoupon"><i
                                                class="fa fa-edit mr-1 edit-btn"></i></a>
									@if($data->group_id == NULL)
                        				<a href="#"  class="btn btn-sm btn-info" onclick = "assignGroup({{$result['user_id']}},{{$data->id}})">assign group</a>
									@endif
                                        <a title="Delete"><i
                                                onclick="delete_confirmation('{{ url('/customer/delete', $data->id) }}')"
                                                class="fa fa-trash mr-1 red" style="cursor:pointer;"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




{{-- Modal ADD Account  --}}
  
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="account_addition_form" name="account_addition_form">
                @csrf
                <input type = "hidden" name = "customer_id"  id = "customer_id" value = ""
                <div>
                  
                    <label>Group</label>
                    <div class="input-group mb-3">
                      <select id = "group_id" name = "group_id" class="form-control">
                      </select>
                    </div>
                   <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                     </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script>
function assignGroup(user_id,customer_id)
{
            $.ajax({
                type : 'POST',
               url: "{{ url('api/list-group')}}",
				data: {user_id:user_id},
            success : function(response)
            {
                let group = response.data.result
                if(response.data.code == 200)
                {
					let name = "group_id";
                  $('#'+name).empty();
                  $('#'+name).append(`<option value="">Select Group</option>`);
                  for(let i=0; i<group.length; i++)
                  {
                    $('#'+name).append(`<option value="${group[i].id}">${group[i].name}</option>`);
                  }
                  $('#'+name).val(id);
                }
                else if(response.data.code== 400)
                {
                  toastr.error(response.data.message);
                }
            }
        });
        $('#customer_id').val(customer_id);
  		$('#saveBtn').val("Assign Group");
        $('#account_addition_form').trigger("reset");
        $('#modelHeading').html("Assign Group");
        $('#ajaxModel').modal('show');
}




// save changes 


$('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
		$(this).attr("disabled", true);
        $.ajax({
          data: $('#account_addition_form').serialize(),
          url: "{{ url('customer/post-assign-group') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
                $('#account_addition_form').trigger("reset");
                $('#ajaxModel').modal('hide');
                location.reload();
                  toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                $('#saveBtn').html('Save Changes');
				$('#saveBtn').attr("disabled", false);
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
			  $('#saveBtn').attr("disabled", false);
			  toastr.error(response.responseJSON.message);
              $('#saveBtn').html('Save Changes');
          }
      });
    });


</script>
@endsection
