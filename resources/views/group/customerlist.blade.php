@extends('portal.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Group Member List</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Customer List</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->

    <div class="row">
    
          
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="table-data" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Position</th>
                                <th>Mobile #</th>
                                <th>customer Name</th>
                                <th>Customer email</th>
                                <th>Customer Address</th>
                                <th>Customer Land Mark</th>
                                <th>Customer QR</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
									

                            @foreach ($result['customer'] as $index => $data)
                                <tr>
									<td>{{$data->serial_number}}</td>
                                    <td>{{ $data->phone }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->address }}</td>
                                    <td>{{ $data->land_mark }}</td>
                                    <td>
                                        <img
                                            src="https://api.qrserver.com/v1/create-qr-code/?data='{{ $data->qr }}'&amp;size=100x100">
                                    </td>
                                    <td>
                        				<a href="#"  class="btn btn-sm btn-info" onclick = "changePosition({{$data->id}},{{$data->group_id}})">Change Position</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




{{-- Modal ADD Account  --}}
  
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="account_addition_form" name="account_addition_form">
                @csrf
                <input type = "hidden" name = "customer_id"  id = "customer_id" value = "" />
                <input type = "hidden" name = "group_id"  id = "group_id" value = "" />

                <div>
                  
                    <label>Group</label>
                    <div class="input-group mb-3">
                      <input type = "integer" id = "serial_number" name = "serial_number" class="form-control">
                      </select>
                    </div>
                   <div class="col-sm-offset-2 col-sm-10 mt-2">
                     <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                     </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script>
function changePosition(customer_id,group_id)
{
           
       
        $('#customer_id').val(customer_id);
        $('#group_id').val(group_id);

  		$('#saveBtn').val("Change Position");
        $('#account_addition_form').trigger("reset");
        $('#modelHeading').html("Change Position");
        $('#ajaxModel').modal('show');
}




// save changes 


$('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
		$(this).attr("disabled", true);
        $.ajax({
          data: $('#account_addition_form').serialize(),
          url: "{{ url('group/change-member-position') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
                $('#account_addition_form').trigger("reset");
                $('#ajaxModel').modal('hide');
                location.reload();
                  toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                $('#saveBtn').html('Save Changes');
				$('#saveBtn').attr("disabled", false);
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
			  $('#saveBtn').attr("disabled", false);
			  toastr.error(response.responseJSON.message);
              $('#saveBtn').html('Save Changes');
          }
      });
    });


</script>
@endsection
