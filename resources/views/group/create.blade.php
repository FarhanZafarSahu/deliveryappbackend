@extends('portal.layouts.app')

@section('content')
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Group</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Add Customer </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Add Group</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('group.store')}}" method = "POST">
				@csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="text" name = "name" class="form-control" id="exampleInputName" placeholder="Enter Name">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>


@endsection
@section('script')
    
@endsection