@extends('portal.layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
						<h1>Delivery Setting</h1>

                    <div class="card-body">
                        <table id="table-data" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
									@if($setting->lock_time == null)
										Delivery is Available
                                    @else
										 will be Locked at <strong>{{Carbon\Carbon::parse($setting->lock_time)->format('H:i A')}} </strong>
                                    @endif
									</td>
                                    @if($setting->lock_time==null)
                                    <td><a href="{{ route('delivery.lock.edit') }}" class="btn btn-sm btn-primary">Lock</a></td>
                                @else
								<form action = "{{ route('delivery.locked',2) }}" method = "Post" >
								@csrf
                                <td><button type = "submit" href="" class="btn btn-sm btn-primary">UnLock</a></td>
                               </form> @endif
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </div>
</div>
</div>
    <!-- /.content -->
@endsection
@section('script')

@endsection
