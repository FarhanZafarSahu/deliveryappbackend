@extends('portal.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Terms & Condition<h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Edit  Terms & Condition </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->

                <!-- general form elements -->
                <div class="card card-primary" style="width:100%">
                    <div class="card-header">
                        <h3 class="card-title">Edit Terms & Conditions </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('save.setting') }}" method="POST">
                        @csrf
						<input type = "hidden" name = "id" value = "{{$setting->id}}"
                        <div class="card-body">

                            <div class="form-group">
                                <label for="exampleInputName">Terms & Condition</label>
                                <textarea class="form-control" name="t_description">{{ $setting->t_description ?? '' }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>

    </section>


@endsection
@section('script')

@endsection
