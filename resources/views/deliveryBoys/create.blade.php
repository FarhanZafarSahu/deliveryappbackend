@extends('portal.layouts.app')

@section('content')
  <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">DeliveryBoy</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Add DeliveryBoy </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Add DeliveryBoy</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('deliveryboy.store')}}" method = "POST">
				@csrf
                <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="name" name = "name" class="form-control" id="exampleInputName" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name = "email" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputName">Mobile</label>
                      <input type="name" class="form-control" name = "phone" id="exampleInputNum" placeholder="Enter Mobile number">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress">Address</label>
                        <input type="name" class="form-control" name = "address" id="exampleInputAddress" placeholder="Enter Address">
                      </div>
                    <div class="form-group">
                      <label for="exampleInputlandmark">Enter LandMark</label>
                      <input type="text" class="form-control" name = "land_mark" id="exampleInputlandmark" placeholder="Enter LandMark">
                    </div>
					<div class="form-group">
                      <label for="exampleInputlandmark">Enter Password</label>
                      <input type="text" class="form-control" name = "password" id="exampleInputlandmark" placeholder="Enter Password">
                    </div>

                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>


@endsection
@section('script')
    
@endsection