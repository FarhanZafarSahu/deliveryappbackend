@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
		<a href="{{route('deliveryboy.create')}}" style="background-color:blue; color:azure; padding:10px; "><i class="fa fa-plus" aria-hidden="true"></i> Add DeliverBoy</a>
            <div>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                <th>#</th>
                <th>Name</th>
                <th>email</th>
                <th>Mobile Number</th>
                <th>Group</th>
                <th>Edit/Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($result['deliveryBoys'] as $index => $data)
                <tr>
                  <td>{{$data->id}}</td>
 				  <td>{{$data->name}}</td>
 				  <td>{{$data->email}}</td>
 				  <td>{{$data->phone_number}}</td>
				
 				  <td>{{$data->group->name ?? "N/A"}}</td>
				 <td>
					<a href="{{route('deliveryboy.edit',$data->id)}}"><i class="fas fa-edit"></i></a>
                   <a title="Delete"><i onclick="delete_confirmation('{{url('/deliveryboy/delete',$data->id)}}')" class="fa fa-trash mr-1 red" style="cursor:pointer;"></i></a>           
					<a href="{{route('deliveryboy.assign-group',$data->id)}}" data-toggle="tooltip" data-placement="top" title="Assign Group" ><i class="fas fa-users"></i></a>
				     </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
    
@endsection