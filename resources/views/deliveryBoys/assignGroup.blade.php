@extends('portal.layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">DeliveryBoy Assign Group</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Update DeliveryBoy Assign Group</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Assign Group to  DeliveryBoy</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('deliveryboy.post-assign-group')}}" method = "post">
				@csrf
				<input type = "hidden" name  = "user_id" value = "{{$deliveryBoy->id}}" >
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="name" class="form-control" name = "name" value = {{$deliveryBoy->name}} id="exampleInputName" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputName">Select Group</label>
                    <select  class="form-control" name = "group_id">
					@foreach($groups as $group)
						<option value = "{{$group->id}}"> {{$group->name}}
						</option>
					@endforeach

					</select>
                  </div>
                  
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>



@endsection
@section('script')
    
@endsection