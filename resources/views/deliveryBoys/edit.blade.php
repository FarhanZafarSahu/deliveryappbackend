@extends('portal.layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">DeliveryBoy</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Update DeliveryBoy </li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->

            <!-- general form elements -->
            <div class="card card-primary" style="width:100%">
              <div class="card-header">
                <h3 class="card-title">Update DeliveryBoy</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action = "{{route('deliveryboy.update')}}" method = "post">
				@csrf
				<input type = "hidden" name  = "id" value = "{{$deliveryBoy->id}}" >
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="name" class="form-control" name = "name" value = {{$deliveryBoy->name}} id="exampleInputName" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name = "email" value = "{{$deliveryBoy->email}}" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputName">Mobile</label>
                      <input type="name" class="form-control" name = "phone" value = "{{$deliveryBoy->phone_number}}"  id="exampleInputNum" placeholder="Enter Mobile number">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputAddress">Address</label>
                        <input type="name" class="form-control" name = "address" value = "{{$deliveryBoy->business_address}}" id="exampleInputAddress" placeholder="Enter Address">
                      </div>
                    <!-- <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="3" placeholder="Enter address"></textarea>
                      </div> -->
                    <div class="form-group">
                      <label for="exampleInputlandmark">Enter LandMark</label>
                      <input type="text" class="form-control" name = "land_mark" value = "{{$deliveryBoy->land_mark}}" id="exampleInputlandmark" placeholder="Enter LandMark">
                    </div>

                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

          </div>

      </section>



@endsection
@section('script')
    
@endsection