@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
          <h1>Contact Us</h1>
			</div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                      <th>Contact Mobile</th>
                    <th>Contact Address</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                       <td> {{ $settings->contact_mobile }}    </td>
                      <td> {{ $settings->contact_address }} </td>
                     <td>
                        <a href="{{ route('contactus.edit',$settings->id) }}" class="btn btn-sm btn-primary">Edit</a>
                     </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
    
@endsection