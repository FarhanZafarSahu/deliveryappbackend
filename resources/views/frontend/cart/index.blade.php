@extends('frontend.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('category.detail',$product->category->name)}}" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>My Cart</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
@endsection
@section('content')
 
        <div class="card border-0">
            <div class="card-body">
				<form method="POST"action="{{route('placed.order')}}">
				@csrf
				<input type="hidden" value="{{$qty}}" id="hqty" name = "quantity">
				<input type="hidden" value="{{$product->id}}" id="productId" name = "productId">
                <div class="row mt-2">
                    <div class="col-4">
                        <img src="{{asset('storage/'.$product->image)}}" alt="" class="card_list_img">
                    </div>
                    <div class="col-8">
                        <div><strong>Amul</strong></div>
                        <h4 class="card_heading">{{$product->product_title}}</h4>
                        <p class="small text-muted">{{$product->unit}} pouch</p>
                        <div class="d-flex justify-content-between align-items-center my-2">
                            <strong>${{$product->price}}</strong>
							<input type="hidden" value="{{$qty}}" id="hqty">
							<div class="d-inline-flex shadow-sm rounded-pill p-1">
                                    <button type="button" class="qty_btn" id="removebtn" onclick="cartqty(1)">
                                        <span class="material-icons d-block">remove</span>
                                    </button>
                                    
								
									<strong class="px-3" id="qty">
									1
									</strong>
                                    <button type="button" class="qty_btn" id="addbtn" onclick="cartqty(2)">
                                        <span class="material-icons d-block">add</span>
                                    </button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <h3 class="sub_heading p-2">Order Summary</h3>
        <div class="card border-0">
            <div class="card-body">
                <div class="row mb-2">
                    <div class="col-8 text-muted">Sub Total</div>
                    <div class="col-4 text-end"><strong id = "subtotal"></strong></div>
                </div>
                <div class="row mb-2">
                    <div class="col-8 text-muted">Service Charge</div>
                    <div class="col-4 text-end"><strong id = "serviceCharges">${{config('constant.DELIVERY_CHARGES')}}*</strong></div>
                </div>
            </div>
            <div class="card-footer border-0">
                <div class="row">
                    <div class="col-8 fw-bold">Sub Total</div>
                    <div class="col-4 text-end" id = "totalprice"><strong></strong></div>
                </div>
            </div>
        </div>
 			<h3 class="sub_heading p-2">Date Of delivery : </h3>
           <div class="form-group mb-2">
                <input type="datetime-local" class="form-control rounded-pill" id = "delivery_date" name = "delivery_date" required>
            </div>
        <h3 class="sub_heading p-2">Personal Details</h3>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Your name" name = "buyer_name" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Mobile Number" name = "buyer_contact_no" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 1" name = "buyer_address" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 2" name = "buyer_address_2" required>
            </div>
       

@endsection
@section('footer')
  <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-stretch">
            <div class="col-6">
                <a href="#" class="btn d-flex flex-column active w-100 border-0 shadow-none">
                    <span class="material-icons"> shopping_cart </span>
                    <strong id = "footertotal">1 item - $20</strong>
                    
                </a>
            </div>
            <div class="col-6 bg-theme d-flex align-items-center text-center">
                <button type = "submit" class="btn w-100 text-white d-flex align-items-center justify-content-center border-0 shadow-none">
                    Place Order
				</button>
            </div>
 </form>
        </div>
    </footer>
    <!-- Footer Close -->



@endsection
@section('script')

<script>
$(document).ready(function(){
		var subtotal = {{$qty}} * {{$product->price}};
		var serviceCharges = {{$qty}}*{{config('constant.DELIVERY_CHARGES')}};
		var totalprice = subtotal + serviceCharges;
		$("#subtotal").html("$"+subtotal);
		$("#totalprice").html("$"+totalprice);
		$('#qty').html({{$qty}});
		$('#serviceCharges').html("$"+serviceCharges);
		$('#footertotal').html("1 item - $"+totalprice);
  });
function cartqty($id)
{
	$qty = $('#hqty').val();

	if($id == 1)
	{
		if($qty == 1)
		{
			  toastr.error("Quantity must be greater then 1");
			return ;
		}
		$qty --
	var serviceCharges = $qty*{{config('constant.DELIVERY_CHARGES')}};
	$('#serviceCharges').html("$"+serviceCharges);
		$('#hqty').val($qty);
		$('#qty').html($qty);
		var subtotal = $qty * {{$product->price}};
		var totalprice = subtotal + serviceCharges;
		$("#subtotal").html("$"+subtotal);
		$("#totalprice").html("$"+totalprice);
		$('#qty').html($qty);
		$('#footertotal').html("1 item - $"+totalprice);

	}
	else if ($id == 2)
	{
		$qty ++
		var serviceCharges = $qty*{{config('constant.DELIVERY_CHARGES')}};
		$('#hqty').val($qty);
		$('#qty').html($qty);
		var subtotal = $qty * {{$product->price}};
		var totalprice = subtotal + serviceCharges ;
		$("#subtotal").html("$"+subtotal);
		$("#totalprice").html("$"+totalprice);
		$('#qty').html($qty);
	$('#serviceCharges').html("$"+serviceCharges);
		$('#footertotal').html("1 item - $"+totalprice);

	}
}
</script>
@endsection
