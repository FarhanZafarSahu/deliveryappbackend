@extends('frontend.layouts.app')
@section('header')
  <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('homefeed')}}" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>TNH Water</strong>
        </div>
        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a>
        </div>
    </header>
@endsection
@section('content')
{{-- Product Model --}}

<div id="productimagedetail">

</div>

{{-- End Product Model --}}
<div id="allproducts">
    <div class="row g-3 mb-2">
        @foreach($product as $object=>$data)
        <div class="col-6">
            <div class="card border-0 mb-0">
                <div class="card-body">
                    <img src="{{asset('/storage/'.$data->image)}}" alt="" class="card_list_top_img" data onclick="sendRequestForDetails({{ $data->id }})" data-toggle="modal" data-target="#exampleModal" style="cursor: pointer">
                    <h4 class="sub_heading mt-2 mb-0">{{ $data->product_title }}</h4>
                    {{-- <p class = "mt-2 mb-0">{{$data->description}}</p> --}}
                    <div><strong>Can capicity</strong></div>
                    <div><strong>{{ $data->price }}</strong></div>
                    <p class="small text-muted"></p>
                    <a href="{{route('subscribe.index',$data->id)}}" class="btn-theme-sm w-100 mb-1 text-center">Regular Customer Plan</a>
                    <a href="#" class="btn-theme-sm w-100 mb-1 text-center"  onclick = "occasional()"  id = "Occasional">Occasional/Event Plan</a>
                   <div class="row" id = "cartCheckout" style = "display:none">
                       <div class="col-md-8">
                             <input type="hidden" name = "product_id"id="productid" value="{{$data->id}}">
                            <input type="hidden" value="1" name = "hqty" id="hqty">
                            <a href = "#" onclick = "addtocart({{$data->id}})" class="btn-theme-sm w-100 mb-1 text-center">Add To Cart
                            </a>
                       </div>
                       <div class="col-md-4" >
                      <div class="d-inline-flex shadow-sm rounded-pill p-1">
                                <button type="button" class="qty_btn" id="removebtn" onclick="cartqty(1)">
                                    <span class="material-icons d-block">remove</span>
                                </button>
                                <strong class="px-3" id="qty">
                                1
                                </strong>
                                <button type="button" class="qty_btn" id="addbtn" onclick="cartqty(2)">
                                    <span class="material-icons d-block">add</span>
                                </button>
                            </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>

@endsection
@section('footer')
 <!-- Main Close -->

    <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-center">
            <div class="col-4">
                <a href="home.html" class="btn d-flex flex-column active">
                    <span class="material-icons"> account_balance_wallet </span>
                    <span>Home</span>
                </a>
            </div>
            <div class="col-4">
                <a href="#" class="btn d-flex flex-column">
                    <span class="material-icons"> home </span>
                    <span>Wallet</span>
                </a>
            </div>
            <div class="col-4">
                <a href="#" class="btn d-flex flex-column">
                    <span class="material-icons"> schedule </span>
                    <span>Schedule</span>
                </a>
            </div>
        </div>
    </footer>
    <!-- Footer Close -->

@endsection
@section('script')
<script>
function occasional()
{
	$('#Occasional').hide();
	$('#cartCheckout').show();
}
    function cartqty($id)
{
	$qty = $('#hqty').val();

	if($id == 1)
	{
		if($qty == 1)
		{
			  toastr.error("Quantity must be greater then 1");
			return ;
		}
		$qty --
		$('#hqty').val($qty);
		$('#qty').html($qty);

	}
	else if ($id == 2)
	{
		$qty ++
		$('#hqty').val($qty);
		$('#qty').html($qty);
	}
}
function addtocart(productid)
{
	let quantity = $("#hqty").val();
	 window.location.assign("{{ url('product-cart') }}" + '/' + quantity +'/' + productid );
}
</script>
<script>
    function sendRequestForDetails(id)
    {
        $.ajax({
           type:'GET',
           url:"{{ route('customer.getproduct.details') }}" + '/' + id,
           async: false,
           data:{id : id},
           success:function(data){
            $("#productimagedetail").html(data);
            $('#exampleModal').modal('show');
           }
        });
    }

    function hideModel()
    {
        $('body').removeClass().removeAttr('style');$('.modal-backdrop').remove();
        $('#exampleModal').modal('hide');
    }
    </script>
@endsection
