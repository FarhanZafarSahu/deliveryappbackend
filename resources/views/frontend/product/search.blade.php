@extends('frontend.layouts.app')
@section('header')
@include('frontend.layouts.partials.header')
@include('frontend.layouts.partials.sidebar')
@include('frontend.layouts.partials.searchbar')
@endsection
@section('css')
<style>
    #hoverit:hover{
        transform: scale(1.1);
        transition: 500ms ease-in-out;
    }
</style>
@endsection
@section('content')

            <div class="card border-0">
                <div class="card-body">
                    <h4 class="card_heading">Product Related to : '<span style="font-style: italic;color:blue">{{ $queryy }}</span>'</h4>
                    <p class="text">Don't get the ideal result? Back to <a href="{{ route('homefeed') }}" style="text-decoration: none;">Home</a></p>
                    <div class="row g-3">
                        @foreach($products as $object=>$data)
                        <div class="col-4" id="hoverit">
                            <a href="{{route('product.detail',$data->id)}}">
                                <div class="card_list shadow-sm"
                                    style="background:url('{{asset('/storage/'.$data->image)}}') center center;background-size:cover;">
                                    @if($data->is_active==1)
                                    <span>{{ $data->product_title }}</span>
                                    @else
                                    <span> Comming Soon </span>
                                    @endif
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

@endsection
@section('footer')
@include('frontend.layouts.partials.footer')
@endsection
