@extends('frontend.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('product.detail',$product->id)}}" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>Subscribe</strong>
        </div>
        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->
@endsection
@section('content')
  <div class="card border-0">
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <a href="#"><img src="{{asset('/storage/'.$product->image)}}" alt="" class="card_list_img"></a>
                    </div>
                    <div class="col-8">
                        <div><strong>Amul</strong></div>
                        <h4 class="card_heading">{{$product->product_title}}</h4>
						<p class = "mb-0 mt-0">{{$product->description}}</p>
                        <div><strong id = "subscriptionPrice" ></strong></div>
                        <p class="small text-muted">* Price may change as per market changes</p>
                    </div>
                </div>
            </div>
        </div>

        <h3 class="sub_heading p-2">Pick Schedule</h3>
        <div class="card border-0">
            <div class="card-body">
				<input type = "hidden" name = "plandata" id = "plansdata" value = "1" />
                <button  id = "daily"    onclick = "plans(1)" class="mb-1 rounded-pill px-3 btn-active" style = "border: 1px solid grey !important;">Daily</button>
                <button id = "alernate" onclick = "plans(2)" class="mb-1  rounded-pill px-3" style = "border: 1px solid grey !important;">Alternet Day</button>
                {{-- <button id = "weekly"   onclick = "plans(3)" class="mb-1 border-theme rounded-pill px-3">Every Three Day</button> --}}
                <button id = "weekly"   onclick = "plans(4)" class="mb-1  rounded-pill px-3" style = "border: 1px solid grey !important;">Flexible</button>

                <div class="d-flex my-2">
                    <div class="d-inline-flex shadow-sm border-theme rounded-pill p-1">
                        <button type="button" class="qty_btn" onclick = "perdayqty(1)">
                            <span class="material-icons d-block">remove</span>
                        </button>
						<input type = "hidden" value = "1" id = "perdayqty" />
                        <strong class="px-3" id = "subscriptionqty">1</strong>
                        <button type="button" class="qty_btn" onclick = "perdayqty(2)">
                            <span class="material-icons d-block">add</span>
                        </button>
                        <strong class="px-2" id = "perplan">/ Day</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="card border-0">
            <div class="card-body">
                <div class="row mt-2">
                    <div class="col-4">
                        <img src="{{asset('public/frontend/images/bottle3.jpg')}}" alt="" class="card_list_img">
                    </div>
                    <div class="col-8">
                        <div><strong>Have Empty T&H water bottle/chamber?</strong></div>
                        <p class="small text-muted">if yes select the quantity below</p>
                        <div class="d-flex my-2">
                            <div class="d-inline-flex shadow-sm rounded-pill p-1">
                                <button type="button" class="qty_btn" onclick="cartqty(1)">
                                    <span class="material-icons d-block">remove</span>
                                </button>
								<input type = "hidden" value = "0" id = "hqty" />
                                <strong class="px-3" id = "qty" >0</strong>
                                <button type="button" class="qty_btn" onclick="cartqty(2)">
                                    <span class="material-icons d-block">add</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
 			<strong class="d-block mt-3 mb-0">Refundable Amount: <strong id = "refundamount"></strong></strong>
            </div>
        </div>
 <div class="card border-0" id = "div_advance_payment">
            <div class="card-body">
                <div class="row mt-2">
                    <div class="col-12">
                        <div><strong>Have Empty T&H water bottle/chamber?</strong></div>
                        <p class="small text-muted mb-0">if No then you have to pay advance payment</p>  
                    </div>
 					<strong class="d-block">Advance Payment : <strong id = "advancepayment"></strong></strong>
                </div>
            </div>
        </div>

        <h3 class="sub_heading p-2">Apply Coupons</h3>
        <form id = "coupon_addition_form" class = "coupon_addition_form" >
            <div class="input-group mb-2 p-1 bg-white border-theme rounded-pill overflow-hidden">
                <input type="text" class="form-control border-0" placeholder="Enter your coupon code" name = "couponNumber" id = "couponNumber" >
                <div class="input-group-append">
                    <button type="submit" class="input-group-text bg-theme rounded-pill px-4" id = "couponredeemBtn">APPLY</button>
                </div>
            </div>
        </form>

        <h2 id = "discountedAmount"></h2>
    {{-- <h3 class="sub_heading p-2">Personal Details</h3>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Your name" name = "buyer_name" id = "buyer_name" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Mobile Number" name = "buyer_contact_no" id = "buyer_contact_no" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 1" name = "buyer_address" id = "buyer_address" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 2" name = "buyer_address_2" id = "buyer_address_2" required>
            </div> --}}

        {{-- <h3 class="sub_heading p-2">Delivery Address</h3>
        <div class="row">
            <div class="col-9">
                <div class="d-flex flex-column">
                    <strong>Home</strong>
                    <p>Other building service is avaiable</p>
                </div>
            </div>
            <div class="col-3 ps-0  text-end">
                <button type="button" class="btn btn-theme w-100 fw-normal rounded-pill">Update</button>
            </div>
        </div> --}}

@endsection
@section('footer')
  <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-stretch">
            <div class="col-6">
                <a href="#" class="btn d-flex flex-column active w-100 border-0 shadow-none">
                    <small class="fw-bold"> Subscription Start Date </small>
                    <strong>{{\Carbon\Carbon::now()->isoFormat('dddd, MMM Do YYYY')}}</strong>
                </a>
            </div>
            <div class="col-6 bg-theme d-flex align-items-center text-center">
                <a href="" id = "customersubscription" class="btn w-100 text-white d-flex justify-content-center align-items-center border-0 shadow-none">
                    <h5 class="mb-0" >Start Subscription</h5>
                </a>
            </div>
        </div>
    </footer>
    <!-- Footer Close -->





@endsection
@section('script')
<script>
function changeborder(id)
{
	if(id == 1)
	{
		$("#daily").css({
	  		"border" : "1px solid blue"
    	});
	$("#alernate").css({
	  		"border" : "1px solid grey"
    	});
	$("#weekly").css({
	  		"border" : "1px solid grey"
    	});
	}
	else if (id == 2)
	{
	$("#daily").css({
	  		"border" : "1px solid grey"
    	});
	$("#alernate").css({
	  		"border" : "1px solid blue"
    	});
	$("#weekly").css({
	  		"border" : "1px solid grey"
    	});
	}
	else if (id == 4)
	{
	$("#daily").css({
	  		"border" : "1px solid grey"
    	});
	$("#alernate").css({
	  		"border" : "1px solid grey"
    	});
	$("#weekly").css({
	  		"border" : "1px solid blue"
    	});
	}
}
function plans(id)
{
	if(id == 1)
	{
		changeborder(1);
		$('#plansdata').val(1);
		$('#perplan').html('/ Day')
		console.log($('#plansdata').val(1));
	}
	else if (id == 2 )
	{
		changeborder(2);
		$('#perplan').html('/ Alternative')
		$("#plansdata").val(2)
	}
	// else if (id ==3 )
	// {
	// 	$('#perplan').html('/ every 3 day')
	// 	$("#plansdata").val(3);
	// }
    else if (id == 4 )
	{
		changeborder(4);
		$('#perplan').html('/ Flexible')
		$("#plansdata").val(4);
	}
}
function cartqty($id)
{
	$qty = $('#hqty').val();

	if($id == 1)
	{
		if($qty == 0)
		{
			toastr.error("Quantity must be greater then equal to 0 ");
			return ;
		}
		$qty --
		$refundAmount = $qty * 20 ;
		$('#refundamount').html('$'+ $refundAmount);
		$('#hqty').val($qty);
		$('#qty').html($qty);
		advancepayment(1);

	}
	else if ($id == 2)
	{
		$qty ++

		$refundAmount = $qty * 20 ;
		$('#refundamount').html('$'+ $refundAmount);
		$('#hqty').val($qty);
		$('#qty').html($qty);
		advancepayment(1);

	}
}
function perdayqty($id)
{
	$qty = $('#perdayqty').val();

	if($id == 1)
	{
		if($qty == 1)
		{
			toastr.error("Quantity must be greater then 1");
			return ;
		}
		$qty --
		$subscriptionPrice = ({{$product->price}} * $qty)
		$('#subscriptionPrice').html("Subcription Price: $" + $subscriptionPrice );
		$('#subscriptionPriceDetail').html("Subcription Price: $" + $subscriptionPrice );
		$('#perdayqty').val($qty);
		advancepayment($qty);
		$('#subscriptionqty').html($qty);

	}
	else if ($id == 2)
	{
		$qty ++
		$subscriptionPrice = ({{$product->price}} * $qty)
		$('#subscriptionPrice').html("Subcription Price: $" + $subscriptionPrice );
		$('#subscriptionPriceDetail').html("Subcription Price: $" + $subscriptionPrice );
		advancepayment($qty);
		$('#perdayqty').val($qty);
		$('#subscriptionqty').html($qty);
	}
}
</script>
<script>
 $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
     $('#couponredeemBtn').click(function (e) {
		productId  = {{$product->id}}
		qty = $('#perdayqty').val();
		couponNumber = $('#couponNumber').val();
        e.preventDefault();
        $.ajax({
          data: { productId: productId, qty : qty ,couponNumber : couponNumber},
          url: "{{ url('coupon-radeem') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
                  $('#discountedAmount').html('discount price : $ ' + response.data.result.coupon.amount)
                  toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
                 toastr.error(response.responseJSON.message);
          }
      });
    });

 $('#customersubscription').click(function (e) {
		productId  	 	 = {{$product->id}}
		qty 	   	 	 = $('#perdayqty').val();
		couponNumber 	 = $('#couponNumber').val();
		emptyblister 	 = $('#hqty').val();
		//buyer_name 	 	 = $('#buyer_name').val();
		//buyer_contact_no = $('#buyer_contact_no').val();
		//buyer_address 	 = $('#buyer_address').val() + $('#buyer_address_2').val();
		plan_id 		 = $('#plansdata').val();
        e.preventDefault();
		if(plan_id == 0 )
		{
            toastr.error("Please select any one plan.");
			return 0 ;
		}
        $.ajax({
          data: { productId: productId, qty : qty ,couponNumber : couponNumber , empty_jar_qty : emptyblister ,  plan_id : plan_id , is_advance_payment : {{$is_advance_payment}} , product_type : {{$product->product_type}}},
          url: "{{ url('customer/order/subscription') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
                  toastr.success(response.data.message);
 				  window.location.assign("{{ url('home-feed') }}");
              }
              else if (response.data.code == 400)
              {
				$('#plansdata').val(0);
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
              toastr.error(response.responseJSON.message);
          }
      });
    });

 });

</script>

<script>
$(document).ready(function(){
		$('#subscriptionPrice').html("Subcription Price: $" + {{$product->price}});
		$('#subscriptionPriceDetail').html("Subcription Price: $" + {{$product->price}});
		$('#refundamount').html('$0');
		advancepayment(1);
   $("#daily").css({
   "border" : "1px solid blue"
    });
  });

function advancepayment(qty)
{
	let empty_blister = $('#hqty').val();
if({{$is_advance_payment}} == 0 && empty_blister == 0)
	$('#advancepayment').html('$'+ ({{$advance_payment}} * qty));
else
	$('#advancepayment').html('$0');
}
</script>
@endsection
