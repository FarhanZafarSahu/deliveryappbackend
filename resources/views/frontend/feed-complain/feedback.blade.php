@extends('frontend.layouts.app')
@section('header')
    <!-- Header -->
    <header class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <button onclick="window.history.back()" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </button>
            <!-- <strong>My Cart</strong> -->
        </div>

        <div>

        </div>
    </header>
    <!-- Header  close-->

    <!-- Logo -->
    <div class="customer_searchbar">
        <div class="row text-center text-white">
            <div class="col-12">
                <img src="{{ asset('public/frontend/images/logo.png') }}" alt="" class="img-fluid">
                <h4 class="heading">Feedback</h4>
                <p> Hope You Are satisfied with our services </p>
            </div>
        </div>
    </div>
    <!-- Logo close -->
@endsection
@section('content')
<div class="row w-100">
    <div class="col-md-8 mx-auto">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('customer.feedback.save') }}" method="POST" class="card-box" autocomplete="off">
                    @csrf
                    <div class="form-group mt-2">
                        <label for="exampleInputName">Name</label>
                        <input type="name" name="name" class="form-control" id="exampleInputName"
                            placeholder="Enter Name" required value="{{ Auth::user()->name ?? '' }}">
                    </div>
                    <div class="form-group mt-2">
                        <label for="exampleInputName">Mobile</label>
                        <input type="name" name="phone_number" class="form-control" id="exampleInputNum"
                            placeholder="Enter Mobile number" required value="{{ Auth::user()->phone_number ?? '' }}">
                    </div>
                    <div class="form-group mt-2">
                        <label for="exampleInputBName">Your Feedback</label><br>
                        <textarea id="editor" placeholder="Enter Your Feedback" class="editor form-control" name="feedback" rows="4"></textarea>
                    </div>
            </div>
            <div class="form-group p-3">
               
                <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Register
                    Feedback</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('footer')
    <!-- Main Close -->
@endsection
