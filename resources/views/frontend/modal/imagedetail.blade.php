<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        {{-- <div class="modal-header">
          <h5 class="modal-title" id="exampleModal">{{ $product->product_title }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div> --}}
        <div class="modal-body">
            <div class="">
                <img src="{{asset('/storage/'.$product->image)}}" alt="" class="card_list_top_img">
            </div>
            <div class="pt-3">
                    <p>{{ $product->description }}</p>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="hideModel()">Close</button>
        </div>
      </div>
    </div>
  </div>

