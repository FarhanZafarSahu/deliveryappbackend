<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
   <title>Customer App</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <title>{{env('APP_NAME')}}</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
  @include('frontend.layouts.partials.css-links')
  @yield('css')

</head>
<body>
  	@yield('header')
 <!-- Main -->
    <main class="site_main">


      @yield('content')

    </main>
	@yield('footer')


  <!-- REQUIRED SCRIPTS -->
  @include('frontend.layouts.partials.javascript-links')
 <!-- ./wrapper -->
 @if(session('success'))
    <script type="text/javascript">
        toast_success("{{session('success')}}")
    </script>
    @endif

    @if(session('error'))
    <script type="text/javascript">
        toast_error("{{session('error')}}")
    </script>
    @endif

  @yield('script')

</body>
</html>
