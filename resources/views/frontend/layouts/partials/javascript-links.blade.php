 <!-- bootstrap.min.js -->
<script src="{{asset('public/frontend/js/bootstrap.min.js')}}" defer></script>
<!-- jQuery -->
<script src="{{asset('public/portal/plugins/jquery/jquery.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('public/portal/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Toastr -->
<script src="{{asset('public/portal/plugins/toastr/toastr.min.js')}}"></script>
<script type="text/javascript">
	var Toast = Swal.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timer: 3000
	});

	function toast_error(message){
		toastr.error(message)
	}

	function toast_success(message){
		toastr.success(message)
	}

	function toast_info(message){
		toastr.info(message)
	}

	function sweet_alert_info(message)
	{
		Toast.fire({
			icon: 'info',
			title: message
		});
	}
</script>
<!-- Bootstrap 4 -->
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> --}}
<script src="{{asset('public/portal/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>


