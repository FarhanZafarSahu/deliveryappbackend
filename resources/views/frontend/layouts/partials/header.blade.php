<!-- Header -->
    <header class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">menu</span>
            </a>
            <strong>Taste & Healthy</strong>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a>
            <a href="tel:03057301880" class="btn btn-sm text-white">
                <span class="material-icons d-block">call</span>
            </a>
        </div>
    </header>
    <!-- Header  close-->