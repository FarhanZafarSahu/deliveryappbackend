 <!-- Sidebar -->
 <div class="offcanvas offcanvas-start sidebar border-0" tabindex="-1" id="sidebar" aria-labelledby="sidebarLabel">
     <div class="sidebar_header">
         <h1 class="sub_heading">Hi, {{ \Auth::guard('customer')->user()->name }}</h1>
         <div class="text">+91{{ \Auth::guard('customer')->user()->phone }}</div>
         <div class="sidebar_divider"></div>
         <div class="d-flex align-items-center">
             <a href="#" class="d-flex align-items-center text-decoration-none text-white">
                 <span class="material-icons d-block text-white m-0">settings</span>
                 <strong class="ms-2">Profile Settings</strong>
             </a>
         </div>
     </div>
     <div class="offcanvas-body sidebar_main px-1">
         <ul class="nav flex-column">
             <li class="nav-item">
                 <a href="#" class="nav-link active"><span class="material-icons">home</span> Home</a>
             </li>
             <li class="nav-item">
                 <a href="{{ route('customer.subscription.history') }}" class="nav-link"><span
                         class="material-icons">shopping_cart</span> Subscription History</a>
             </li>
             <li class="nav-item">
                 <a href="{{ route('customer.jar.history') }}" class="nav-link"><span
                         class="material-icons">gradient</span> Jar/Camper History</a>
             </li>
             <li class="nav-item">
                 <a href="{{ route('customer.wallet.history') }}" class="nav-link"><span
                         class="material-icons">restaurant</span> Jar/Camper Wallet</a>
             </li>
             {{-- <li class="nav-item">
                 <a href="{{ route('customer.qr') }}" class="nav-link"><span class="material-icons"><span
                             class="material-icons">qr_code</span></span> QR Code</a>
             </li> --}}
              <li class="nav-item">
                 <a href="{{ route('customer.feedback') }}" class="nav-link"><span class="material-icons"><span
                             class="material-icons">qr_code</span></span> Feedback</a>
             </li> 
             <li class="nav-item">
                 <a href="{{ route('customer.complain') }}" class="nav-link"><span class="material-icons"><span
                             class="material-icons">qr_code</span></span>  Complain </a>
             </li>
         </ul>

         <div class="dropdown-divider"></div>
         <strong class="text-muted">Taste & Healthy Special</strong>
         <ul class="nav flex-column">
             {{-- <li class="nav-item">
                    <a href="#" class="nav-link"><span class="material-icons">send</span> Refer & Earn</a>
                </li> --}}


             <li>
                 <a class="nav-link" href="{{ url('logout') }}" onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();"><span class="material-icons">send</span>
                     Logout
                 </a>
                 <form id="logout-form" action="{{ url('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
             </li>
         </ul>
     </div>
 </div>
 <!-- Sidebar Close -->
