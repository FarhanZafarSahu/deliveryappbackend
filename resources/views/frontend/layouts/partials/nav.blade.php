{{-- Navbar --}}
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ url('home') }}" class="nav-link">Home</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item d-sm-inline-block">
            {{-- <a href="{{url('/')}}" class="nav-link">Logout</a> --}}
            <a class="nav-link" href="{{ url('logout') }}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle={{ checkDay() == true ? 'dropdown' : '' }} href="#"
                aria-expanded="true">
                <i class="far fa-bell"></i>
                @if (checkDay() == true)
                    <span class="blink badge badge-warning navbar-badge">1</span>
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-file mr-2"></i>Payment due today from Forrun. </br>Kindly check with the bank and
                    sheet</br>and add amount on CRM Gahhak.
                </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        @if (!auth()->user()->isMember() ?? null)
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
        @endif
    </ul>
</nav>
<!-- /.navbar
