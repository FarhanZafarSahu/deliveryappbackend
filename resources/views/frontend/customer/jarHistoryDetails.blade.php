 <div class="card border-0">
            <div class="card-header">
                <strong class="d-block mt-2 text-theme">Monthly Jar History</strong>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-sm small">
                    <thead class="bg-theme">
                        <tr>
                            <th>Balance(total number of zero)</th>
  							<th>Type</th>
                            <th width="50">0</th>
                            <th width="50">1</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th>Date</th>
							<th>Product Type </th>
                            <th>In</th>
                            <th>Out</th>
                        </tr>
                    </thead>
                    <tbody>
	@foreach($result['subscription'] as $index => $data)
	                    <tr>
                            <td>{{$data->created_at}}</td>
                           <td>{{$data->subscription->product->product_title}}
							 <td>{{$data->subscription->empty_jar_qty}}</td>
                            <td>{{$data->orderqty()}}</td>
                        </tr>
	@endforeach
                    </tbody>
                </table>
            </div>
        </div>
