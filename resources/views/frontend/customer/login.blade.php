@extends('frontend.layouts.app')
@section('header')
    <!-- Header -->
    <header class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            {{-- <a href="" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a> --}}
            <!-- <strong>My Cart</strong> -->
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                    <span class="material-icons d-block">notifications</span>
                </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                    <span class="material-icons d-block">search</span>
                </a> -->
        </div>
    </header>
    <!-- Header  close-->

    <!-- Logo -->
    <div class="customer_searchbar">
        <div class="row text-center text-white">
            <div class="col-12">
                <img src="{{ asset('public/frontend/images/logo.png') }}" alt="" class="img-fluid">
                <h4 class="heading">LOGIN</h4>
                <p>Login with your mobile number</p>
            </div>
        </div>
    </div>
    <!-- Logo close -->
@endsection
@section('content')
    <form action="{{ route('customer.login.attempt') }}" method="POST" class="p-4" id="inputform">
        @csrf
        <div class="form-group mb-2">
            <input type="text" class="form-control" placeholder="Enter Mobile Numnber" name="phone" required>
        </div>
        <h4 class="sub_heading text-center mt-4">Enter PIN</h4>
        <div class="row g-3 mb-2">
            <div class="col">
                <input type="text" required
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                    class="form-control text-center code-input" minlength="1" maxlength="1" id="pinval1" data-index="1">
            </div>
            <div class="col">
                <input type="text" required
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                    class="form-control text-center code-input" minlength="1" maxlength="1" id="pinval2" data-index="2">
            </div>
            <div class="col">
                <input type="text" required
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                    class="form-control text-center code-input" minlength="1" maxlength="1" id="pinval3" data-index="3">
            </div>
            <div class="col">
                <input type="text" required
                    oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"
                    class="form-control text-center code-input" minlength="1" maxlength="1" id="pinval4" data-index="4"
                    onkeyup="setotp()">
            </div>
        </div>
        <input type="hidden" name="password" id="password">
        <input type="submit" class="btn btn-theme shadow w-100" value="Login" id="login">
    </form>
    <p>Do not have an Account ? </p>
    <div class="col-12">
        <a href="verify_number.html">
            <a href="{{ route('customer.customer.register') }}" type="submit" class="btn btn-theme shadow w-100">Register
                Now</a>
        </a>
    </div>
@endsection
@section('footer')
    <!-- Main Close -->
@endsection

@section('script')
    <script>
        function setotp() {
            let val1 = $('#pinval1').val();
            let val2 = $('#pinval2').val();
            let val3 = $('#pinval3').val();
            let val4 = $('#pinval4').val();
            let otp = $('#password').val(val1 + val2 + val3 + val4);
        }

        const inputElements = [...document.querySelectorAll('input.code-input')]
        inputElements.forEach((ele, index) => {
            ele.addEventListener('keydown', (e) => {
                if (e.keyCode === 8 && e.target.value === '') inputElements[Math.max(0, index - 1)].focus()
            })
            ele.addEventListener('input', (e) => {
                const [first, ...rest] = e.target.value
                e.target.value = first ?? ''
                const lastInputBox = index === inputElements.length - 1
                const insertedContent = first !== undefined
                if (insertedContent && !lastInputBox) {
                    inputElements[index + 1].focus()
                    inputElements[index + 1].value = rest.join('')
                    inputElements[index + 1].dispatchEvent(new Event('input'))
                }
            })
        })
        // get all inputs and convert into array
        // for newer browser you can use Array.from()
        // fonr convertitng into array


        // document.getElementById('login').addEventListener('click', function() {

        //   console.log(
        //     inputs.map(function(ele) {
        //       return ele.value;
        //     }).join('')
        //   );
        //   console.log(
        //     inputs.reduce(function(str, ele) {
        //       return str + ele.value;
        //     }, '')
        //   );
        // });
    </script>
@endsection
