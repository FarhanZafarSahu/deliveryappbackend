@extends('frontend.layouts.app')
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('homefeed')}}" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>Subcription History</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
@endsection
@include('frontend.layouts.partials.sidebar')
@section('content')

     <!-- Main -->
    <main class="site_main">
        <div class="card border-0">
            <div class="card-header">
				<input type = "hidden" name = "balance_qty" id = "balance_qty" value = "{{$result['balance']}}" />
                <strong class="d-block mt-2 text-theme">Current Plan : {{$result['allSubscription']->plan->plan_name}}<span></strong>
                <strong class="d-block mt-2 text-theme">Plan end-Date : {{$result['allSubscription']->subscription_end_date}}<span></strong>
				 <strong class="d-block mt-2 text-theme">Total Jar/Camper: <span id = "balance" value = "{{$result['balance']}}">{{$result['balance']}}<span></strong>
            </div>
            <div class="card-header">
                <div class="row g-2">
                    <div class="col-2">

                         <strong class="d-block mt-2 text-theme">Schedule</strong>

                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <span class="input-group-text bg-white border-0">
                                <span class="material-icons">date_range</span>
                            </span>
                            <input type="date" class="form-control border-0" placeholder="dd-mm-yy" id = "date_order_placement">
                        </div>
                    </div>
					<div class="col-2">
                        <div class="input-group border-theme rounded" style="height:40px;">

                            <input type="number" class="form-control border-0" min="1" placeholder="1" name = "qty" id = "qty" value = "1">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <a  class="btn-theme-sm w-100 mb-1 text-center" id = "placedOrder" style="cursor: pointer">Schedule Now</a>
                        </div>
                    </div>


                </div>
            </div>

		</div>
 <h3 class="sub_heading p-2">Personal Details</h3>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Your name" name = "buyer_name" id = "buyer_name" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Mobile Number" name = "buyer_contact_no" id = "buyer_contact_no" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 1" name = "buyer_address" id = "buyer_address" required>
            </div>
            <div class="form-group mb-2">
                <input type="text" class="form-control rounded-pill" placeholder="Address 2" name = "buyer_address_2" id = "buyer_address_2" required>
            </div>


    </main>
    <!-- Main Close -->
@endsection
@section('footer')
   <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-stretch">
            <div class="col-6">
                <a href="#" class="btn d-flex flex-column active w-100 border-0 shadow-none">
                    <small class="fw-bold"> Subscription Start Date </small>
                    <strong>{{\Carbon\Carbon::now()->isoFormat('dddd, MMM Do YYYY')}}</strong>

                </a>
            </div>
            <div class="col-6 bg-theme d-flex align-items-center text-center">
                <a href="{{route('homefeed')}}"
                    class="btn w-100 text-white d-flex justify-content-center align-items-center border-0 shadow-none">
                    <h5 class="mb-0">Start Subscription</h5>
                </a>
            </div>
        </div>
    </footer>
    <!-- Footer Close -->





@endsection
@section('script')
<script>
 $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
     $('#placedOrder').click(function (e) {
        e.preventDefault();
		balance   		 = $('#balance_qty').val();
		date 	  		 = $("#date_order_placement").val();
		buyer_name 		 = $('#buyer_name').val();
		buyer_address 	 = $('#buyer_address').val();
		buyer_address_2  = $('#buyer_address_2').val();
		buyer_address 	 = buyer_address + buyer_address_2 ;
		buyer_contact_no = $('#buyer_contact_no').val();
		qty				 = $('#qty').val();
		if(balance <= 0 )
		{
		    toastr.error('Your Wallet Balance is Insufficient');
			return ;
		}
		id = {{$result['subscription']->id ?? 0}};
        $.ajax({
          data: { balance: balance, id : id , qty : qty , date : date , buyer_contact_no : buyer_contact_no , buyer_address : buyer_address ,buyer_name : buyer_name },
          url: "{{ url('placed_order-schedule') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
					$total  = $('#balance_qty').val();
					$qty = response.data.result;
					$remaining_balance = $total - $qty;
					$('#balance').html($remaining_balance);
					$('#balance_qty').val($remaining_balance);
					$('#buyer_name').val('');
					$('#buyer_address').val('');
					$('#buyer_address_2').val('');
					$('#buyer_contact_no').val('');
					$('#date_order_placement').val('');



                  toastr.success(response.data.message);
              }
              else if (response.data.code == 400)
              {
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
                 toastr.error(response.responseJSON.message);
          }
      });
    });
 });

</script>
@endsection
