@extends('frontend.layouts.app')
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('customer.customer.register')}}" class="btn btn-sm text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <!-- <strong>My Cart</strong> -->
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->

    <!-- Logo -->
    <div class="customer_searchbar">
        <div class="row text-center text-white">
            <div class="col-12">
                <img src="{{asset('images/logo.png')}}" alt="" class="img-fluid">
                <h4 class="heading">VERIFY NUMBER</h4>
                <p>OTP sent to your mobile number</p>
            </div>
        </div>
    </div>
    <!-- Logo close -->

@endsection
@section('content')

      <form action="{{ route('customer.otp.verify') }}" method="POST" class="p-4">
        @csrf
            <div class="row g-3 mb-2">
                <div class="col">
                    <input type="text" class="form-control text-center code-input"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength = "1" maxlength = "1" id="pinval1">
                </div>
                <div class="col">
                    <input type="text" class="form-control text-center code-input"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength = "1" maxlength = "1" id="pinval2">
                </div>
                <div class="col">
                    <input type="text" class="form-control text-center code-input"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" minlength = "1" maxlength = "1" id="pinval3">
                </div>
                <div class="col">
                    <input type="text" class="form-control text-center code-input"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');"  minlength = "1" maxlength = "1" id="pinval4">
                </div>
                <div class="col">
                    <input type="text" class="form-control text-center code-input" minlength = "1" maxlength = "1"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" id="pinval5" onkeyup="setotp()">
                </div>
            </div>
            <input type="hidden" name="otp" id="otp">
            <footer class="customer_footer fixed-bottom px-2 d-flex align-items-center justify-content-center">
                <div class="row text-center align-items-center w-100 g-2">
                    <div class="col-6">
                        <button type="" class="btn btn-theme shadow w-100" id="waitotp">RESEND IN  0:<span id="countdown"></span></button>
                        <a href="{{ route('customer.show.otppage') }}" class="btn btn-theme shadow w-100" id="resendotp" style="display: none">RESEND OTP NOW</a>
                    </div>
                    <div class="col-6">
                        <input type="submit" class="btn bg-white shadow w-100" value="ENTER OTP">
                    </div>
                </div>
            </footer>
        </form>
@endsection
@section('footer')
@endsection
@section('script')
<script>
    function setotp()
      {
        let val1 = $('#pinval1').val();
        let val2 = $('#pinval2').val();
        let val3 = $('#pinval3').val();
        let val4 = $('#pinval4').val();
        let val5 = $('#pinval5').val();
        let otp =  $('#otp').val(val1+val2+val3+val4+val5);
        console.log(otp);
      }

       //go to next line
       const inputElements = [...document.querySelectorAll('input.code-input')]
        inputElements.forEach((ele, index) => {
            ele.addEventListener('keydown', (e) => {
                if (e.keyCode === 8 && e.target.value === '') inputElements[Math.max(0, index - 1)].focus()
            })
            ele.addEventListener('input', (e) => {
                const [first, ...rest] = e.target.value
                e.target.value = first ?? ''
                const lastInputBox = index === inputElements.length - 1
                const insertedContent = first !== undefined
                if (insertedContent && !lastInputBox) {
                    inputElements[index + 1].focus()
                    inputElements[index + 1].value = rest.join('')
                    inputElements[index + 1].dispatchEvent(new Event('input'))
                }
            })
        })
</script>

<script type="text/javascript">
        var timeleft = 60;
        var downloadTimer = setInterval(function() {
            if (timeleft <= 0) {
                clearInterval(downloadTimer);
                $('#waitotp').hide();
                $('#resendotp').show();
            } else {
                document.getElementById("countdown").innerHTML = timeleft + "";
            }
            timeleft -= 1;
        }, 1000);
    </script>
@endsection
