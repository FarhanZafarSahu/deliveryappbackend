@extends('frontend.vendor.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" href="" 
                class="btn btn-sm shadow-none text-white ps-0" onclick="window.history.back()">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
   <strong>All Customers ({{$result['tnhCustomer']->count() + $result['personalCustomer']->count() }} Customers)</strong>
        <form action="" class="mt-2">
            <div class="input-group border-theme rounded p-1">
                <span class="input-group-text bg-white border-0 pe-0">
                    <span class="material-icons">search</span>
                </span>
                <input type="text" id = "myInput" class="form-control border-0" placeholder="Search by name or phone number">
            </div>
        </form>
        <div class="row g-0 mt-2">
            <div class="col-6 border-end">
                <div class="btn_filter shadow-sm d-flex justify-content-between align-items-center">
                    <strong class="me-2">Filter</strong>
                    <img src="{{asset('public/frontend/vendor/images/edit.png')}}" alt="" width="20">
                </div>
            </div>
            <div class="col-6">
                <div class="btn_filter shadow-sm px-0">
                    <select name="" id="" class="form-select border-0 shadow-none">
                        <option value="">Select Option</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="accordion mt-2" id="accordionID">
            <div class="accordion-item">
                <h2 class="accordion-header" id="accor1">
                    <button class="accordion-button collapsed shadow-none" type="button" data-bs-toggle="collapse"
                        data-bs-target="#collapseOne">
                        <strong class="text-theme">TNH Customer</strong>
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="accor1"
                    data-bs-parent="#accordionID">
                    <div class="accordion-body text-muted">
                         <div class="accordion-body" id = "listTnhCustomer">
						@forelse($result['tnhCustomer'] as $key => $customer)
							
                        <div class="row border-bottom pb-2" style = "align-items: center;">
                            <div class="col-2">
                                <img src="{{asset('public/frontend/vendor/images/man.png')}}" alt="">
                            </div>
                            <div class="col-8">
                                <strong>{{$customer->name}}</strong>
                                <p class="small mb-0">{{$customer->phone}}</p>
                            </div>
                            <div class="col-2 text-end">
                                {{-- <input type="radio" name="" id="" checked> --}}
                            <a href="{{route('vendor.customer.view',$customer->id)}}" title="View Details"><span class="iconify" data-icon="carbon:view-filled"></span></a>

                            </div>
                        </div>
						@empty
							No Customer Found.	
						@endforelse
                    </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="accor1">
                    <button class="accordion-button shadow-none" type="button" data-bs-toggle="collapse"
                        data-bs-target="#collapseOne">
                        <strong class="text-theme">Personal Customer</strong>
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="accor1"
                    data-bs-parent="#accordionID">
                    <div class="accordion-body" id = "listPersonalCustomer">
						@forelse($result['personalCustomer'] as $key => $customer)
							
                        <div class="row border-bottom pb-2">
                            <div class="col-2">
                                <img src="{{asset('public/frontend/vendor/images/man.png')}}" alt="">
                            </div>
                            <div class="col-8">
                                <strong>{{$customer->name}}</strong>
                                <p class="small mb-0">{{$customer->phone}}</p>
                            </div>
                            <div class="col-2 text-end">
                                {{-- <input type="radio" name="" id="" checked> --}}
                            <a href="{{route('vendor.customer.view',$customer->id)}}" title="View Details"><span class="iconify" data-icon="carbon:view-filled"></span></a>

                            </div>
                        </div>
						@empty
							No Customer Found.	
						@endforelse
                    </div>
                </div>
            </div>
        </div>
 <!-- Main Close -->


    <!-- Floating Button -->
    <div class="floating_btn position-fixed">
        <a href="#" class="d-flex flex-column justify-content-center align-items-center rounded-pill shadow text-decoration-none btn_circle">
            <span class="material-icons">add</span>
        </a>
        <div class="dropdown_floating position-absolute">
            <a href="{{route('vendor.add.customer')}}" class="newcontact bg-success text-white rounded-pill m-1 shadow text-decoration-none">
                <span class="material-icons">person_add</span>
            </a>
        </div>
    </div>
@endsection
@section('script')
<script>

$("#myInput").on("keyup", function() {
  var value = this.value.toLowerCase().trim();
  $("#listPersonalCustomer div").show().filter(function() {
    return $("#listPersonalCustomer div").text().toLowerCase().trim().indexOf(value) == -1;
  }).hide();
 $("#listTnhCustomer div").show().filter(function() {
    return $("#listTnhCustomer div").text().toLowerCase().trim().indexOf(value) == -1;
  }).hide();
});
</script>
@endsection