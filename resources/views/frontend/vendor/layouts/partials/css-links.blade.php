<!-- Font Awesome Icons -->
<link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/frontend/css/customer-style.css')}}">

<!-- Toastr -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/toastr/toastr.min.css')}}">

<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('public/portal/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

