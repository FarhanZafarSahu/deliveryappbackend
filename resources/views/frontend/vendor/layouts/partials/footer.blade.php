 <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-center">
            <div class="col-4">
                <a href="{{ route('homefeed') }}" class="btn d-flex flex-column active">
                    <span class="material-icons"> account_balance_wallet </span>
                    <span>Home</span>
                </a>
            </div>
            <div class="col-4">
                <a href="{{route('customer.wallet.history')}}" class="btn d-flex flex-column">
                    <span class="material-icons"> home </span>
                    <span>Wallet</span>
                </a>
            </div>
            <div class="col-4">
                <a href="{{route('customer.subscription.history')}}" class="btn d-flex flex-column">
                    <span class="material-icons"> schedule </span>
                    <span>Schedule</span>
                </a>
            </div>
        </div>
    </footer>
    <!-- Footer Close -->
