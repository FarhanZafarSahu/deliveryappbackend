@extends('frontend.vendor.layouts.app')
@section('css')
<style>
    #hoverit:hover{
        transform: scale(1.1);
        transition: 500ms ease-in-out;
    }
</style>
@endsection
@section('header')
  <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">menu</span>
            </a>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/whatsapp.png')}}" alt="" width="30"> <small>Help</small>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <span class="material-icons d-block">person</span> <small>Online</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
   <!-- Main -->
    <main class="site_main" style="background:url('./images/bg.png') no-repeat center top;">
        <div class="row g-2 text-center">
            <div class="col-6">
                <a href="#" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">delivery_dining</span>
                            </div>
                            <div class="mt-2 fw-bold"><a href = "{{route("vendor.todayDelivery")}}">Today Delivery</a></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6">
                <a href="#" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">people_alt</span>
                            </div>
                            <div class="mt-2 fw-bold"><a href = "{{route("vendor.allCustomer")}}">All Customers</a></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6">
                <a href="#" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">group</span>
                            </div>
                            <div class="mt-2 fw-bold"><a href = "{{route("vendor.allGroups")}}">Groups</a></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6">
                <a href="#" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">payments</span>
                            </div>
                            <div class="mt-2 fw-bold">Payment Entry</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-6">
                <a href="#" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">person</span>
                            </div>
                            <div class="mt-2 fw-bold"><a href = "{{route("vendor.allDeliveryBoy")}}">Employees</a></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </main>
@endsection