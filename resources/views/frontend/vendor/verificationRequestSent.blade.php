@extends('frontend.vendor.layouts.app')
@section('header')
      <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">menu</span>
            </a>
            <strong>My Profile</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->
@endsection
@include('frontend.vendor.layouts.partials.sidebar')

@section('content')
     <main class="site_main bg-white align-items-center justify-content-center mt-2">
       <img src="{{asset('public/frontend/vendor/images/approval.png')}}" alt="" class="">
    </main>
@endsection