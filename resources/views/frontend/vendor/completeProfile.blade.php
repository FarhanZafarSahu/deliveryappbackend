@extends('frontend.vendor.layouts.app')
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">menu</span>
            </a>
            <strong>My Profile</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->
@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
     <h4 class="sub_heading mb-3">Business Information</h4>
        <form action="{{route('completeProfile')}}" method = "post" class="mt-3">
			@csrf
			<input type = "hidden" name = 'id' value = {{$vendor->id}} >
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Mobile Number <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">phone_iphone</span>
                    </span>
                    <input type="text" class="form-control border-0"  name="phone_number" value = {{$vendor->phone_number}} placeholder="Mobile number">
                    <span class="input-group-text bg-white border-0 p-0">
                        <button class="btn bg-theme btn-sm fw-bold">CHANGE</button>
                    </span>
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Full Name <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Full Name" name = "name" value = "{{$vendor->name}}">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Brand Name <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">corporate_fare</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Brand Name" name = "brand_name">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Business Name <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">business</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Business Name" name = "business_name">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Business Address <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">location_on</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Business Address" value="" name = "business_address">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Business Pincode <span class="text-danger">*</span></label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person_pin_circle</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Business Pincode" name = "business_pin_code">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Email Address (Optional)</label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">email</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="Email Address" name = "email" value = "{{$vendor->email}}">
                </div>
            </div>
            <div class="border-theme bg-white shadow-sm p-3 mb-4 rounded position-relative">
                <label for="" class="float_label">Alternative phone number</label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">smartphone</span>
                    </span>
                    <input type="text" class="form-control border-0" placeholder="" name = "alternative_phone_number">
                </div>
            </div>
            <button class="btn btn-theme w-100" type="submit">Submit</button>
        </form>
@endsection
