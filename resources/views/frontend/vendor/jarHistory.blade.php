@extends('frontend.layouts.app')
@section('header')
 <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('homefeed')}}" class="btn btn-sm text-white ps-0" onclick="window.history.back()">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>Jar History</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->

    <!-- Searchbar -->
    <!-- <div class="customer_searchbar">
        <form action="">
            <input type="text" class="form-control" placeholder="Admadabaad, Gujrat, India, 123456">
        </form>
    </div> -->
    <!-- Searchbar close -->


@endsection
@include('frontend.layouts.partials.sidebar')
@section('content')
   <!-- Main -->
    <main class="site_main">
        <div class="card border-0">
            <div class="card-header">
                <strong class="d-block mt-2 text-theme">Jar Advance Payment History</strong>
            </div>
            <div class="card-header">
                <div class="row g-2">
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <select name="" id="plan_id" class="form-select shadow-none border-0">
                                <option value ="{{ url("customer/order/jar-history", 1)}}" @php if($result['filter'] == 1) echo "selected" @endphp  >Daily</option>
								<option value ="{{ url("customer/order/jar-history", 2)}}" @php if($result['filter'] == 2) echo "selected" @endphp >Alternate Day</option>
								{{-- <option value ="{{ url("customer/order/jar-history", 3)}}" @php if($result['filter'] == 3) echo "selected" @endphp >Every 3 Day</option> --}}
                                <option value ="{{ url("customer/order/jar-history", 4)}}" @php if($result['filter'] == 4) echo "selected" @endphp >Flexible</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <span class="input-group-text bg-white border-0">
                                <span class="material-icons">date_range</span>
                            </span>
                            <input type="date" class="form-control border-0" readonly value = "{{\Carbon\Carbon::now()->endOfMonth()->format('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <select name="" id="" class="form-select shadow-none border-0">
                                <option value="">Jar</option>
                                <option value="">Camper</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <input type="text" class="form-control border-0" placeholder="In"  value = "{{$result['in']}}" readonly>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <input type="text" class="form-control border-0" placeholder="Out" value = "{{$result['out']}}" readonly>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="input-group border-theme rounded" style="height:40px;">
                            <input type="text" class="form-control border-0" placeholder="Balance" value = "{{$result['in'] - $result['out'] }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-sm small">
                    <thead class="bg-theme">
                        <tr>
                            <th>#</th>
                            <th>Plan</th>
                            <th>Icon</th>
                            <th>Qty</th>
							<th>Subscription end date</th>
                            <th>Payment</th>
                        </tr>
                    </thead>
                    <tbody>
  					@foreach($result['subscription'] as $index => $data)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td>{{$data->plan->plan_name}}</td>
                            <td align="center">
                                <img src="{{asset('public/frontend/images/jar.png')}}" alt="">
                            </td>
                            <td align="center">{{$data->total_jar_qty}}</td>
  							<td align="center">{{$data->subscription_end_date}}</td>
                            <td>Cash</td>
                        </tr>
                     @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card border-0">
            <div class="card-header">
                <strong class="d-block mt-2 text-theme">Monthly Jar History</strong>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-sm small">
                    <thead class="bg-theme">
                        <tr>
                            <th>Balance(total number of zero)</th>
                            <th width="50">0</th>
                            <th width="50">1</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>In</th>
                            <th>Out</th>
                        </tr>
                    </thead>
                    <tbody>
	@foreach($result['subscription'] as $index => $data)
	@foreach($data->order as $index => $data)
                        <tr>
                            <td>{{$data->created_at}}</td>
                            <td>{{$data->subscription->empty_jar_qty}}</td>
                            <td>{{$data->orderqty()}}</td>
                        </tr>
	@endforeach
	@endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </main>
    <!-- Main Close -->

@endsection
@section('script')
<script type="text/javascript">
    $('#plan_id').on('change', function (e) {
        var link = $("option:selected", this).val();
        if (link) {
            location.href = link;
        }
    });
</script>
@endsection
