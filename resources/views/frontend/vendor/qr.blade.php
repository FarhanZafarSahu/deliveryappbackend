@extends('frontend.layouts.app')
@section('header')
 <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('homefeed')}}" class="btn btn-sm text-white ps-0" onclick="window.history.back()">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>Customer QR</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
    <!-- Header  close-->

    <!-- Searchbar -->
    <!-- <div class="customer_searchbar">
        <form action="">
            <input type="text" class="form-control" placeholder="Admadabaad, Gujrat, India, 123456">
        </form>
    </div> -->
    <!-- Searchbar close -->


@endsection
@include('frontend.layouts.partials.sidebar')
@section('content')
   <!-- Main -->
    <main class="site_main">
        <div class="card border-0">
            <div class="card-header">
                <strong class="d-block mt-2 text-theme">Customer QR</strong>
            </div>
            <div class="card-header">
                <div class="row g-2">
                     <div class="card-body text-center">
                    <img
                    src="https://api.qrserver.com/v1/create-qr-code/?data='{{ $customer->qr }}'&amp;size=300x300" />
                </div>
                    
                </div>
            </div>
          
        </div>

      
    </main>
    <!-- Main Close -->

@endsection