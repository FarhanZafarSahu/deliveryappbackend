@extends('frontend.vendor.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
<div class="bg-blue p-3 text-center shadow position-relative">
        <strong class="add_customer_title d-flex justify-content-center align-items-center w-50 shadow-sm">
            <span class="material-icons me-2">person_add</span> Add Delivery Boy
        </strong>
    </div>
  <main class="site_main bg-white pt-4">
     <strong>Delivery Boy  Information</strong>
        <form action="{{route('vendor.store.deliveryBoy')}}" method = "post" class="mt-3" enctype="multipart/form-data">
			@csrf
            <div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">File</label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="file" name = "image" class="form-control border-0"required>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Full Name </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "name" class="form-control border-0" placeholder="Enter Full Name" required>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Phone # </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "phone" class="form-control border-0" placeholder="Enter Phone # " required>
                </div>
            </div>
				<div class="">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
 <!-- Main Close -->
@endsection