@extends('frontend.vendor.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
<div class="bg-blue p-3 text-center shadow position-relative">
        <strong class="add_customer_title d-flex justify-content-center align-items-center w-50 shadow-sm">
            <span class="material-icons me-2">person_add</span> Assign Delivery Boy
        </strong>
    </div>
  <main class="site_main bg-white pt-4">
        <form action = "{{route('vendor.deliveryboy.post-assign-group')}}" method = "post">
				@csrf
				<input type = "hidden" name  = "user_id" value = "{{$deliveryBoy->id}}" >

			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Full Name </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "name" class="form-control border-0" value = "{{$deliveryBoy->name}}" readonly>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Select Group</label>
                <div class="input-group input-group-sm mt-2">
					<select class = "form-control border-0" name = "group_id">
					@foreach($groups as $key => $group)
                   <option value ="{{$group->id}}" class = "form-control border-0">{{$group->name}}</option>
					@endforeach
					</select>
                </div>
            </div>
				<div class="">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
 <!-- Main Close -->
@endsection