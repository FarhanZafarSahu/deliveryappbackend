@extends('frontend.vendor.layouts.app')
@section('css')
<style>
.discount-card {
    background-color: white;
    border-radius: 10px;
    border: 1px solid #DBDBDB;
}
.dis-des {
    font-weight: 700;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    display: inline-block;
    max-width: 165px;
}
.dis-percentage {
    font-weight: 700;
    margin-right: 25px;
}
.dis-img {
    width: 100%;
    height: 98px;
    border-right: 1px solid #DBDBDB;
    border-radius: 10px 0 0 10px;
    object-fit: cover;
}
.dis-price {
    font-size: 12px;
    font-weight: 600;
}
.dis-price-del {
    font-size: 12px;
    font-weight: 600;
    color: #DBDBDB;
}
.discount-btn {
    margin: 3px 23px 9px 1px;
    background-color: #FF5757;
    border: 1px solid #FF5757;
    color: white;
    font-size: 10px;
}
.card-description {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    margin: 0 14px 0 0;
}
.sold-overlay {
    background-color: #FE5654;
    height: 98px;
    width: 100%;
    z-index: 0;
    opacity: 0.3;
    position: absolute;
    border-radius: 10px 0px 0px 9px;
}
.sold-text {
    font-size: 21px;
    color: #FE5654;
    font-weight: bold;
    left: 0;
    right: 0;
    z-index: 1;
    position: absolute;
    align-items: center;
    margin-top: 2rem;
}
</style>
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
   <strong>All Delivery boy</strong>
        <form action="" class="mt-2">
            <div class="input-group border-theme rounded p-1">
                <span class="input-group-text bg-white border-0 pe-0">
                    <span class="material-icons">search</span>
                </span>
                <input type="text" id = "myInput" class="form-control border-0" placeholder="Search by name or phone number">
            </div>
        </form>
		<div class = "row"> 
		@foreach($result['deliveryBoys'] as $key => $data)
        <div class="col-md-4 p-2 parent-div">
                        <div class="discount-card">
                            <div class="row m-0">
                                <div class="col-4">
                                <div>
                                </div>
									 <img class="dis-img" src="{{asset('public/frontend/vendor/images/man.png')}}" alt="">
                                </div>
                                <div class="col-8 p-0">
                                    <div class="d-flex justify-content-between">
                                        <div class="dis-des mt-2 child-div"><span>{{$data->name}}</span></div>
                                        <div class="btn btn discount-btn"><span class="material-icons">mode_edit</span></div>
                                    </div>
                                    <span class="card-description">{{$data->business_address}}</span>
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <div class="dis-price theme-clr">{{$data->phone_number}} </div>
                                        </div>
										@if(!$data->group_id)
										<a href="{{route('vendor.assign-group',$data->id)}}" class="btn btn discount-btn">
                							<span class="material-icons">person_add</span>
            							 </a>
										@endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endforeach
</div>
 <!-- Main Close -->


    <!-- Floating Button -->
 <!-- Floating Button -->
    <div class="floating_btn position-fixed">
        <a href="#" class="d-flex flex-column justify-content-center align-items-center rounded-pill shadow text-decoration-none btn_circle">
            <span class="material-icons">add</span>
        </a>
        <div class="dropdown_floating position-absolute">
            <a href="{{route('vendor.add.deliveryboy')}}" class="newcontact bg-success text-white rounded-pill m-1 shadow text-decoration-none">
                <span class="material-icons">person_add</span>
            </a>
        </div>
    </div>
@endsection
@section('script')
<script>
$("#myInput").on("keyup", function() {
var g = $(this).val().toUpperCase();
$(".parent-div .child-div span").each( function() {
var s = $(this).text().toUpperCase();
if (s.indexOf(g)!=-1) {
$(this).parent().parent().parent().parent().parent().parent().show();
}
else {
$(this).parent().parent().parent().parent().parent().parent().hide();
}
});
});
</script>
@endsection