@extends('frontend.vendor.layouts.app')
@section('css')
 <link rel="stylesheet" href="{{asset('public/vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/vendor/css/customer-style.css')}}">
@endsection
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
    <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <div class="icon2 fs-3 me-3 pt-1"><a href="#" class="text-decoration-none"><i class="fa-solid fa-user-pen" style="color: #000;"></i></a></div>
                <div class="text-secondary">
                    <span><b>{{$result['customer']->name}}</b></span><br>
                    <span><b>{{$result['customer']->phone}}</b></span>
                </div>
            </div>
            <div class="pe-2">
                <a href="#"><img src="{{asset('public/vendor/images/barcode.webp')}}" alt="" width="30"></a>
                <span><a href="#" class="text-p text-decoration-none">Scan paytm/ PhonePe/ GPay</a></span>
            </div>
        </div>
	@if($result['active_subscription'] != null && $result['active_subscription']->paymentHistory == NULL)
        <div class="d-flex justify-content-between pt-3">
            {{-- <span><b>22 Dec 2021</b></span> --}}

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                <i class="fa-solid fa-file-circle-check pe-2"></i>
  <a href="javascript:void(0)" id="createNewAccount" class="float-right mr-2 btn btn-primary" onclick="loadPreviousPlans({{$result['active_subscription']->user_id}})">Past Payment</a>
            </button>

            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title w-75 d-flex justify-content-center ps-5 ms-5" id="staticBackdropLabel">Select Date</h5>
                            <hr>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
  			<form action = "{{route('vendor.customer.subscription.payment')}}" method = "post">
                @csrf
                        <div class="modal-body">
                            <div class="d-flex justify-content-center">
                                 <select id = "planList" name = "subscription_id" class="form-control">
                      			</select>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-center pt-4">
                                <input type="text" placeholder="Enter Payment" name = "payed_amount" class="int py-3 w-100">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
				</form>
                    </div>
                </div>
            </div>

        </div>
        <hr style="#0F0A44">
        <div class="bill">
            <div class="d-flex justify-content-between pt-3 px-3">
                <div class="d-flex">
                    <span><b>{{\Carbon\Carbon::now()->isoFormat('MMM YY')}} Bill</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end align-items-center">
                        <span>{{$result['active_subscription']->qty * $result['active_subscription']->product->price}}</span>  <span class="iconify" data-icon="mdi:currency-inr"></span>
                    </div>
                </div>
            </div>
            <div class="b-amount d-flex justify-content-between pt-2 px-3">
                <div class="d-flex">
                    <span><b>Balance Amount</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end align-items-center">
                        <span>{{$result['customer']->balance_amount ?? 0}}</span>  <span class="iconify" data-icon="mdi:currency-inr"></span>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between pt-2 px-3">
                <div class="d-flex">
                    <span><b>Total Amount</b></span><br>
                </div>
                <div class="d-flex align-items-center w-50">
                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                    <div class="w-25 d-flex justify-content-end align-items-center">
                        <span>{{($result['active_subscription']->qty * $result['active_subscription']->product->price)- ($result['customer']->balance_amount ?? 0)}}</span><span class="iconify" data-icon="mdi:currency-inr">
                    </div>
                </div>
            </div>
        </div>
		<form action = "{{route('vendor.customer.subscription.payment')}}" method = "post">
		@csrf
		<input type = "hidden" name = "subscription_id" value = "{{$result['active_subscription']->id}}">
        <div class="pt-4">
            <input type="text" name = "payed_amount" placeholder="Enter Payment" class="int w-100 py-3">
        </div>
        <div class="text-center pt-4">
            <button type = "submit" class="btn btn-theme w-50" type="submit">Submit</button>
        </div>
</form>
@endif
@if($result['subscription_history'] != [])
@foreach($result['subscription_history'] as $key => $data)
@if($data['paymentHistory'])
        <div class="accord pt-4">
            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            {{$data['created_at']->isoFormat('MMM YYYY')}}
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body">
                            <div class="d-flex justify-content-between pb-2 px-3">
                                <span>Billiing Period</span>
                                <span>{{$data['created_at']->format('Y-m-d')}}</span>
                            </div>
                            <div class="d-flex justify-content-between px-3">
                                <span>Billiing Date</span>
                                <span>{{$data['subscription_end_date']->format('Y-m-d')}}</span>
                            </div>

                            <div class="d-flex justify-content-between pt-3 px-3">
                                <div class="d-flex">
                                    <span class="pe-3">Per bottle Price</span>
                                    <span class="pe-3">x</span>
                                    <span>Total qty</span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <span class="pe-2">({{$data['paymentHistory']['per_unit_price']}}</span>
                                    <span class="pe-2">x</span>
                                    <span>{{$data['paymentHistory']['qty']}})</span>
                                </div>
                                <div class="d-flex align-items-center w-50">
                                    <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                                    <div class="w-25 d-flex justify-content-end  align-items-center">
                                        <span>{{$data['paymentHistory']['per_unit_price'] * $data['paymentHistory']['qty']}}</span><span class="iconify" data-icon="mdi:currency-inr">
                                    </div>
                                </div>
                            </div>
                            <div class="bill">
                                <div class="d-flex justify-content-between pt-3 px-3">
                                    <div class="d-flex">
                                        <span>Month Amount</span><br>
                                    </div>
                                    <div class="d-flex align-items-center w-50">
                                        <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                                        <div class="w-25 d-flex justify-content-end align-items-center">
                                            <span>{{$data['paymentHistory']['per_unit_price'] * $data['paymentHistory']['qty']}}</span><span class="iconify" data-icon="mdi:currency-inr">
                                        </div>
                                    </div>
                                </div>
                                <div class="b-amount d-flex justify-content-between pt-2 px-3">
                                    <div class="d-flex">
                                        <span>Balance Amount</span><br>
                                    </div>
                                    <div class="d-flex align-items-center w-50">
                                        <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                                        <div class="w-25 d-flex justify-content-end align-items-center">
                                            <span>{{$data['paymentHistory']['balance_amount']}}</span>  <span class="iconify" data-icon="mdi:currency-inr"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between pt-2 px-3">
                                    <div class="d-flex">
                                        <span>Total Amount</span><br>
                                    </div>
                                    <div class="d-flex align-items-center w-50">
                                        <div class="w-75 d-flex justify-content-end"><i class="fa-solid fa-indian-rupee-sign pt-1"></i></div>
                                        <div class="w-25 d-flex justify-content-end align-items-center">
                                            <span>{{$data['paymentHistory']['payed_amount']}}</span> <span class="iconify" data-icon="mdi:currency-inr"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endif
@endforeach
            </div>
        </div>
@else<h1>No Subscription History Found.</h1>
@endif


@endsection
@section('script')
<script>

$("#myInput").on("keyup", function() {
  var value = this.value.toLowerCase().trim();
  $("#listPersonalCustomer div").show().filter(function() {
    return $("#listPersonalCustomer div").text().toLowerCase().trim().indexOf(value) == -1;
  }).hide();
 $("#listTnhCustomer div").show().filter(function() {
    return $("#listTnhCustomer div").text().toLowerCase().trim().indexOf(value) == -1;
  }).hide();
});
</script>
 <script src="{{asset('public/vendor/js/bootstrap.min.js')}}" defer></script>
<script type="text/javascript">
function loadPreviousPlans(user_id){
  $.ajax({
    type : 'POST',
	data : {user_id:user_id},
    url: "{{ url('api/plans-list')}}",
    success : function(response)
    {
      let plans = response.data.result
      if(response.data.code == 200)
      {
        $('#planList').empty();
        $('#planList').append(`<option value="">Select Plan</option>`);
        for(let i=0; i<plans.length; i++)
        {
			let from_date = new Date(plans[i].created_at).toDateString();
			let to_date = new Date(plans[i].subscription_end_date).toDateString();
	
          $('#planList').append(`<option value="${plans[i].id}">${from_date} - ${to_date}</option>`);
        }
      }
      else if(response.data.code== 400)
      {
        toastr.error(response.data.message);
      }
    }
  });
 }
</script>
@endsection