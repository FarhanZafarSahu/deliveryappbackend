@extends('frontend.vendor.layouts.app')
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
<div class="bg-blue p-3 text-center shadow position-relative">
        <strong class="add_customer_title d-flex justify-content-center align-items-center w-50 shadow-sm">
            <span class="material-icons me-2">person_add</span> Add Customer
        </strong>
    </div>
  <main class="site_main bg-white pt-4">
     <strong>Customer Information</strong>
        <form action="{{route('vendor.store.customer')}}" method = "post" class="mt-3">
			@csrf
            <div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Full Name </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "name" class="form-control border-0" placeholder="Enter Full Name" required>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Email </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "email" class="form-control border-0" placeholder="Enter email" required>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Phone # </label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "phone" class="form-control border-0" placeholder="Enter Phone # " required>
                </div>
            </div>
			<div class="border-theme bg-white shadow-sm p-3 mb-3 rounded position-relative">
                <label for="" class="float_label">Address</label>
                <div class="input-group input-group-sm mt-2">
                    <span class="input-group-text bg-white border-0 p-0">
                        <span class="material-icons d-block m-0">person</span>
                    </span>
                    <input type="text" name = "address" class="form-control border-0" placeholder="Enter Address" required>
                </div>
            </div>
            <div class="d-flex align-items center mb-2">
                <strong>Receives Jars or Products?</strong>
                <span class="material-icons ms-1">info</span>
            </div>
            <p class="text-muted mb-1">Select Whether this Customer receives Jars or Products? (cann't be changed on
                customer
                is created)</p>
           
				<div class= "row">
					<div class="col-2" class="form-group mb-3 text-muted">
						<div class="btn_filter shadow-sm px-0">				
							<h5 style = "text-align: center;justify-content: center;"> Product : </h5> 
						</div>
					</div>
               		<div class="col-6">
                		<div class="btn_filter shadow-sm px-0">
                    		<select name="product_id" id="product_id" class="form-select border-0 shadow-none" required>
                        		<option value="">Select Product</option>
								@foreach($result['products'] as $key => $product)
                        		<option value="{{$product->id}}">{{$product->product_title}}</option>
								@endforeach
                    		</select>
                		</div>
            		</div>
               	</div>
				<div class= "row mt-4">
					<div class="col-2" class="form-group mb-3 text-muted">
						<div class="btn_filter shadow-sm px-0">				
							<h5 style = "text-align: center;justify-content: center;"> Plan : </h5> 
						</div>
					</div>
               		<div class="col-6">
                		<div class="btn_filter shadow-sm px-0">
                    		<select name="plan_id" id="plan_id" class="form-select border-0 shadow-none" required>
                        		<option value="">Select Plan</option>
								@foreach($result['plans'] as $key => $plan)
                        		<option value="{{$plan->id}}">{{$plan->plan_name}}</option>
								@endforeach
                    		</select>
                		</div>
            		</div>
               	</div>
				<div class="border-theme bg-white shadow-sm p-3 mb-3 mt-3 rounded position-relative">
                	<label for="" class="float_label">Enter Quantity </label>
                	<div class="input-group input-group-sm mt-2">
                    	<span class="input-group-text bg-white border-0 p-0">
                        	<span class="material-icons d-block m-0">person</span>
                    	</span>
                    	<input type="text" name = "quantity" class="form-control border-0" placeholder="Enter Quantity" required>
               		</div>
            	</div>
				<div class="">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
 <!-- Main Close -->
@endsection