@extends('frontend.vendor.layouts.app')
@section('css')
  <link rel="stylesheet" href="{{asset('public/vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/vendor/css/customer-style.css')}}">

@endsection
@section('header')
   <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">speed</span>
            </a>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
@forelse($customers as $key => $data)
  <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <div class="fs-3 pe-3 pt-1"><a href="#" class="text-decoration-none"><i class="fa-solid fa-user-pen" style="color: #000;"></i></a></div>
                <div class="text-secondary">
                    <span><b>{{$data->customer->name}}</b></span><br>
                    <span><b>{{$data->customer->phone_number}}</b></span>
                </div>
            </div>
            <div class="pe-2">
                <a href="#"><img src="{{asset('public/vendor/images/barcode.webp')}}" alt="" width="30"></a>
                <span><a href="#" class="text-p text-decoration-none">Scan paytm/ PhonePe/ GPay</a></span>
            </div>
        </div>
        <div class="balance">
            <div class="main-balance">

                <div class="icon d-flex">
                    <i class="fa-solid fa-house pt-1 pe-3"></i>
                    <span>Balance Jar</span>
                </div>
                <div class="numb d-flex align-items-center">
				@php 
					$used_jar = App\OrderDetail::whereIn('order_id',App\Order::where('is_accept',1)->where('subscription_id',$data->id)->pluck('id'))->sum('quantity')@endphp
                    <span class="pe-2">{{$data->total_jar_qty}}</span>
                    <span class="pt-1">*</span>
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <span class="pe-2">-</span>
                    <span class="pe-2">{{$used_jar}}</span>
                    <span class="pt-1">*</span>
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <span class="pe-2">=</span>
                    <span class="pe-2">{{$data->total_jar_qty - $used_jar}}</span>
                </div>
            </div>

        </div>
    <div class="past1">
            <span><b>Blue Chilled</b></span>
            <div class="d-flex justify-content-between py-3">
                <div class="d-flex">
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <i class="fa-solid fa-arrow-right pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Full Jar (Jar In)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="{{$data->empty_jar_qty}}">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-between">
                <div class="d-flex">
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <i class="fa-solid fa-arrow-left pt-2"></i>
                    <span class="ps-5 pt-1 text-secondary">Empty Jar (Jar Out)</span>
                </div>
                <div>
                    <div class="quantity">
                        <a href="#" class="quantity__minus"><span>-</span></a>
                        <input name="quantity" type="text" class="quantity__input" value="{{$data->qty}}">
                        <a href="#" class="quantity__plus"><span>+</span></a>
                    </div>
                </div>
            </div>
        </div><hr>
@empty
	<h1>No Today Delivery</h1>
@endforelse
@endsection  
@section('script')
  <script src="{{asset('public/vendor/js/bootstrap.min.js')}}" defer></script>
@endsection