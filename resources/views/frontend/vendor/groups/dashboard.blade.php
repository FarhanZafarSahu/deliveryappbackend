@extends('frontend.vendor.layouts.app')
@section('css')
<style>
    #hoverit:hover{
        transform: scale(1.1);
        transition: 500ms ease-in-out;
    }
</style>
@endsection
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas"onclick="window.history.back()" class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <span>Back</span>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
    <h4 class="sub_heading text-center mb-2">All Groups</h4>
        <div class="row g-3 text-center">
           @forelse($result['allGroups'] as $key => $group)
            <div class="col-6">
                <a data-bs-toggle="offcanvas" href="#sidebar" role="button" aria-controls="sidebar" class="text-decoration-none text-theme">
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
                            <div class="dashoard_icon">
                                <span class="material-icons">payments</span>
                            </div>
                            <div class="mt-2 fw-bold"><a href = "{{route('vendor.singleGroupDetail',$group->id)}}">{{$group->name}}</a></div>
                        </div>
                    </div>
                </a>
            </div>
  			@empty 
                    <div class="card border-0 shadow mb-0">
                        <div class="card-body">
							No Record Found!!
                        </div>
                    </div>
           
		   @endforelse
        </div>


    <!-- Floating Button -->
 <!-- Floating Button -->
    <div class="floating_btn position-fixed">
        <a href="#" class="d-flex flex-column justify-content-center align-items-center rounded-pill shadow text-decoration-none btn_circle">
            <span class="material-icons">add</span>
        </a>
        <div class="dropdown_floating position-absolute">
            <a href="{{route('vendor.add.group')}}" class="newcontact bg-success text-white rounded-pill m-1 shadow text-decoration-none">
                <span class="material-icons">person_add</span>
            </a>
        </div>
    </div>
 <!-- Main Close -->
@endsection