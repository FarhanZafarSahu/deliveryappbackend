@extends('frontend.vendor.layouts.app')
@section('css')
  <link rel="stylesheet" href="{{asset('public/vendor/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/vendor/css/customer-style.css')}}">
<style>

    #hoverit:hover{
        transform: scale(1.1);
        transition: 500ms ease-in-out;
    }
</style>
@endsection
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            {{--  href="all-group.html" --}}
            <a data-bs-toggle="offcanvass" href="#" onclick="window.history.back()"
                class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <span>Back</span>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
      <div class="d-flex justify-content-between">
            <div class="d-flex">
                <img src="{{asset('public/vendor/images/alls-groups.png')}}" alt="" width="30">
                <span class="ps-3">Daily Paying Customer Collection</span>
            </div>
            <div class="d-flex pe-3">
                <i class="fa-solid fa-indian-rupee-sign d-flex align-items-center pe-2 pt-1"></i>
                <span class="pt-1">0</span>
            </div>
        </div>
        <div class="d-flex justify-content-between pt-3">
            <div class="d-flex">
                <img src="{{asset('public/vendor/images/all-groups-calender%20(1).png')}}" alt="" width="30">
                <span class="ps-3">Monthly Paying Customer Collection</span>
            </div>
            <div class="d-flex pe-3">
                <i class="fa-solid fa-indian-rupee-sign d-flex align-items-center pe-2 pt-1"></i>
                <span class="pt-1">0</span>
            </div>
        </div>
        <div class="total-jar d-flex justify-content-between pt-3 ps-2 pe-3">
            <div><span>Total Jar Delivered</span></div>
            <div class="d-flex">
                <div class="container d-flex" style="border-right: 1px solid;">
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <span class="pt-1">0</span>
                </div>
                <div class="d-flex">
                    <img src="{{asset('public/vendor/images/water-bottles-computer-icons-clip-art-png-favpng-vTA0apPxPiF7QYgi2vi9pDm2V.jpg')}}" alt="" width="30">
                    <span class="pt-1">0</span>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between px-2 pt-3">
            <div>
                <a href="#" class="text-decoration-none">{{$result['group']->name}}</a><br>
                <span class="text-primary">({{App\Customer::whereIn('vendor_id',App\User::where('group_id',$result['group']->id)->pluck('id'))->count()}} Customers)</span>
            </div>
            <div class="d-flex">
                <a href="#" class="pe-3"><img src="{{asset('public/vendor/images/User_Circle.png')}}" alt="" width="30"></a>
                <a href="#"><img src="{{asset('public/vendor/images/mobile-data.png')}}" alt="" width="30"></a>
            </div>
        </div>
        <form action="" class="mt-3">
            <div class="input-group border-theme rounded p-1">
                <span class="input-group-text bg-white border-0 pe-0">
                    <span class="material-icons">search</span>
                </span>
                <input type="search" class="form-control border-0"  id = "search-function" placeholder="Search">
            </div>
        </form>
		@foreach($result['customer'] as $key => $data)
        <div class="d-flex justify-content-between pt-3 parent-item-list-div">
            <div class="d-flex">
                <div class="fs-3 pe-3"><i class="fa-solid fa-user"></i></div>
                <div class="text-secondary single-item-details-div">
                    <span><b>{{$data->name}}</b></span><br>
                    <b>{{$data->phone}}</b>
                </div>
            </div>
            <div class="pe-2">
                <div class="d-flex">
                    <img src="{{asset('public/vendor/images/User_Circle.png')}}" alt="" width="30">
                    <span class="pt-1 ps-2">0</span>
                </div>
                <span>Jar Bal.</span>
				@php 
					$subscription = App\UserSubscriptionPlan::where('user_id',$data->id)->where('subscription_end_date','>=',\Carbon\Carbon::now()->format('Y-m-d H:i:s'))->first();
					$used_jar = App\OrderDetail::whereIn('order_id',App\Order::where('is_accept',1)->where('subscription_id',$subscription->id ?? 0)->pluck('id'))->sum('quantity');
				@endphp

                <div class="d-flex">
                    <img src="{{asset('public/vendor/images/User_Circle.png')}}" alt="" width="30">
                    <span class="pt-1 ps-2">{{$subscription->total_jar_qty ?? 0 - $used_jar}}</span>
                </div>

            </div>
        </div>
		@endforeach
 <!-- Main Close -->

  <!-- Floating Button -->
    <div class="floating_btn position-fixed">
        <a href="#"
            class="d-flex flex-column justify-content-center align-items-center rounded-pill shadow text-decoration-none btn_circle">
            <span class="material-icons">add</span>
        </a>
        <div class="dropdown_floating position-absolute">
            <a href=""
                class="newcontact bg-success text-white rounded-pill m-1 shadow text-decoration-none">
                <span class="material-icons">person_add</span>
            </a>
        </div>
    </div>
@endsection
@section('script')
  <script src="{{asset('public/vendor/js/bootstrap.min.js')}}" defer></script>
<script>

$("#search-function").on("keyup", function() {
var g = $(this).val().toUpperCase();
$(".parent-item-list-div .single-item-details-div span").each( function() {
var s = $(this).text().toUpperCase();
if (s.indexOf(g)!=-1) {
$(this).parent().parent().parent().removeClass('d-none');
}
else {
$(this).parent().parent().parent().addClass('d-none');

}
});
});
</script>

@endsection