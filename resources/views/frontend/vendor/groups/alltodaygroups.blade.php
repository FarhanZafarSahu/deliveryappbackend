@extends('frontend.vendor.layouts.app')
@section('css')
<style>
    #hoverit:hover{
        transform: scale(1.1);
        transition: 500ms ease-in-out;
    }
</style>
@endsection
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a data-bs-toggle="offcanvas" onclick="window.history.back()" class="btn btn-sm shadow-none text-white ps-0">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <span>Back</span>
        </div>

        <div>
            <a href="#" class="btn btn-sm text-white d-inline-flex flex-column">
                <img src="{{asset('public/frontend/vendor/images/youtube.png')}}" alt="" width="30"> <small>Help</small>
            </a>
        </div>
    </header>
    <!-- Header  close-->

@endsection
@include('frontend.vendor.layouts.partials.sidebar')
@section('content')
 <h4 class="sub_heading text-center mb-3">All Groups</h4>
        <div class="input-group border-theme p-2 mb-2 rounded">
            <span class="input-group-text bg-white border-0">
                <span class="material-icons">search</span>
            </span>
            <input type="text" class="form-control border-0" id = "search-criteria" placeholder="Search">
        </div>

        <div class="row">
            <div class="col-12">
                <ul class="list-group">
				@foreach($result['allGroups'] as $key => $data)	
                    <li class="list-group-item">
                        <div class="row align-items-center">
                            <div class="col-1">
                                <span class="material-icons">group</span>
                            </div>
                            <div class="col-9 ps-4 list-data">
                               <a href = "{{route('vendor.grouptodayDelivery',$data->id)}}" <strong class="d-block mb-1">{{$data->name}}</strong></a>
                                <span class="text-muted">{{ $data->customerUser()->count() }} Customer </span>
                            </div>
                            <div class="col-2 text-end">
                                {{-- <span class="material-icons text-danger">delete</span> --}}
                            </div>
                        </div>
                    </li>
				@endforeach
                </ul>
            </div>
        </div> 
 <!-- Main Close -->
@endsection
@section('script')
<script>

$("#search-criteria").on("keyup", function() {
var g = $(this).val().toUpperCase();

$(".list-group .list-data a").each( function() {
var s = $(this).text().toUpperCase();
if (s.indexOf(g)!=-1) {
$(this).parent().parent().parent().show();
}
else {
$(this).parent().parent().parent().hide();
}
});
});
</script>
@endsection