@extends('frontend.layouts.app')
@section('header')
    <!-- Header -->
    <header
        class="w-100 position-sticky top-0 start-0 end-0 d-flex align-items-center justify-content-between text-white">
        <div class="d-flex align-items-center">
            <a href="{{route('homefeed')}}" class="btn btn-sm text-white ps-0" onclick="window.history.back()">
                <span class="material-icons d-block">arrow_back</span>
            </a>
            <strong>Subcription History</strong>
        </div>

        <div>
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">notifications</span>
            </a> -->
            <!-- <a href="#" class="btn btn-sm text-white">
                <span class="material-icons d-block">search</span>
            </a> -->
        </div>
    </header>
@endsection
@include('frontend.layouts.partials.sidebar')
@section('content')

     <table class="table table-bordered table-sm small">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Plan</th>
                    <th>Status</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
            </thead>
            <tbody>
 		 @foreach($result['subscription'] as $index => $data)
                <tr>
                    <td>1</td>
                    <td><a href = "#"    onclick="subscriptionHistory('{{$data->id}}')" >{{$data->plan->plan_name ?? ''}}</a></td>
                    <td class="text-success">@php if(\Carbon\Carbon::now() > $data->subscription_end_date)  echo "Complete" ;else echo "Running"; @endphp</td>
                    <td>{{$data->updated_at}}</td>
                    <td>{{$data->subscription_end_date}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
 <div id = "jarhistory">
	
</div>
@endsection
@section('footer')
   <!-- Footer -->
    <footer class="customer_footer fixed-bottom">
        <div class="row text-center align-items-stretch">
            <div class="col-6">
                <a href="#" class="btn d-flex flex-column active w-100 border-0 shadow-none">
                    <small class="fw-bold"> Subscription Start Date </small>
                    <strong>{{\Carbon\Carbon::now()->isoFormat('dddd, MMM Do YYYY')}}</strong>

                </a>
            </div>
            <div class="col-6 bg-theme d-flex align-items-center text-center">
                <a href="{{route('homefeed')}}"
                    class="btn w-100 text-white d-flex justify-content-center align-items-center border-0 shadow-none">
                    <h5 class="mb-0">Start Subscription</h5>
                </a>
            </div>
        </div>
    </footer>
    <!-- Footer Close -->





@endsection
@section('script')
<script>
function subscriptionHistory(id)
{
 $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
        $.ajax({
          data: { id: id},
          url: "{{ route('customer.subscription.detail') }}",
          type: "POST",
          dataType: 'json',
          success: function (response)
          {
              if(response.data.code == 200)
              {
                  $('#jarhistory').html(response.data.result)
              }
              else if (response.data.code == 400)
              {
                toastr.error(response.data.message);
              }
          },
          error: function (response)
          {
              console.log('Error:', response);
                 toastr.error(response.responseJSON.message);
          }
      });
      });
}
</script>
@endsection