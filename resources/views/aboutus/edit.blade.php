@extends('portal.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">About Us</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">About Us </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->

                <!-- general form elements -->
                <div class="card card-primary" style="width:100%">
                    <div class="card-header">
                        <h3 class="card-title">Edit About Us </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('save.setting') }}" method="POST" enctype="multipart/form-data" >
                        @csrf
						<input type ="hidden" name = "id" value = "{{$setting->id}}" />
                        <div class="card-body">

                            <div class="form-group">
                                <label for="exampleInputName">About Image</label>
                                <input type="file" name="about_img" class="form-control" id="exampleInputName">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputName">About Image</label>
                                <textarea class="form-control" name="about_description" >{{ $setting->about_description ?? '' }}</textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>

    </section>


@endsection
@section('script')

@endsection
