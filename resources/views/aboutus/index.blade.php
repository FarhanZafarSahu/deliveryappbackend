@extends('portal.layouts.app')

@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
		<h1>About Us</h1>
            </div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Image</th>
                                    <th>About Description</th>
                                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
              
                <tr>
                  <td>
                                        <img src="{{asset('/storage/'.$setting->about_img)}}" style="height: 50px;width:50px;">
                                    </td>
                                    <td>{{ $setting->about_description }}</td>
                                    <td><a href="{{ route('aboutus.edit',$setting->id) }}" class="btn btn-sm btn-primary">Edit</a></td>
                              
                </tr>
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
    
@endsection