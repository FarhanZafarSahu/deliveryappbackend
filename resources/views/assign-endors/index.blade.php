@extends('portal.layouts.app')
@section('content')
<section class="content-header">
</section>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <div>
	
              <h4 class="float-left">Customer Details</h4>
              <div class="float-right"style="display: inline-flex;">
              <select class="form-control" id="filter" name = "filter">
                    <option   value =""selected disabled>Select Filter</option>
                    <option   value ="{{ url("new/customers", 1)}}" @php if($filter == 1) echo "selected" @endphp>New Customer</option>
                    <option  value ="{{ url("new/customers", 2)}}" @php if($filter == 2) echo "selected" @endphp>All Customer</option>
				</select>
              </div>
    	</div>
          </div>
          <div class="card-body">
            <table id="table-data" class="table table-bordered table-striped">
              <thead>
                <tr>
		  <th>Mobile #</th>
                    <th>Customer Name</th>
                    <th>Vendor Assigned</th>
                    <th>Customer email</th>
                    <th>Customer Address</th>
                    <th>Customer Land Mark</th>
  					<th>Customer Payment Remarks</th>
                    <th>Customer QR</th>

                    <th>Action</th>

                </tr>
              </thead>
              <tbody>
  @foreach($customers as $index => $data)
                    <tr>
                      <td>{{$data->phone}}</td>
                       <td>{{$data->name}}</td>
                      <td>{{$data->name}}</td>
                      <td>{{$data->email}}</td>
                      <td>{{$data->address}}</td>
                      <td>{{$data->land_mark}}</td>
 					 <td>Customer->Admin->??{{$data->vendor->name ?? ''}}</td>
                      <td>
                         <img src="https://api.qrserver.com/v1/create-qr-code/?data='{{$data->qr}}'&amp;size=100x100"></td>
                       <td>
					@if($filter == 1)
                        <a href="{{route('newcustomer.view',$data->id)}}"  class="btn btn-sm btn-info" >assign vendor</a>
					@endif
 					<a href="{{ route('customer.edit', $data->id) }}" class="" id=""><i class="fa fa-edit mr-1 edit-btn"></i></a>
					<a href="{{ route('customer.view', $data->id) }}" class="" id=""><i class="fa fa-eye mr-1 eye-btn"></i></a>
 					<a title="Delete"><i onclick="delete_confirmation('{{url('/customer/delete',$data->id)}}')" class="fa fa-trash mr-1 red" style="cursor:pointer;"></i></a>
                    </td>
                    </tr>
                    @endforeach
                               </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
@section('script')
<script type="text/javascript">
    $('#filter').on('change', function (e) {
        var link = $("option:selected", this).val();
        if (link) {
            location.href = link;
        }
    });
</script>
@endsection