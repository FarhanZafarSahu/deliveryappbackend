<?php

use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Delivery\DeliveryBoyController;
use App\Http\Controllers\FrontEndController;
use App\Http\Controllers\Frontend\Vendor\VendorController;
use FontLib\Table\Type\name;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/phpinfo', function () {
    return phpinfo();
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/portal-login', function () {
    return view('auth.login');
});

Route::get('/', function () {
    return view('frontend.customer.login');
});

// Route::get('/home-feed', function () {
//     return view('frontend.homefeed');
// });

Route::prefix('/customers')->middleware('guest')->name('customer.')->namespace('Customer')->group(function () {
    Route::get('/login', [CustomerController::class, 'show_login'])->name('login')->middleware('guest');
    Route::post('/login', [CustomerController::class, 'login'])->name('login.attempt');
    Route::get('/register', [CustomerController::class, 'show_register'])->name('customer.register');
    Route::post('/register', [CustomerController::class, 'register'])->name('register.attempt');
    Route::get('/verify-otp', [CustomerController::class, 'show_otp'])->name('show.otppage');
    Route::get('/getproduct-details/{id?}', [FrontEndController::class, 'getProductDetails'])->name('getproduct.details');
    Route::post('/verification-otp', [CustomerController::class, 'verifyOtp'])->name('otp.verify');
    Route::post('/logout', [CustomerController::class, 'logout'])->name('logout');
});

Route::get('/home-feed', 'FrontEndController@homeFeed')->name('homefeed')->middleware('auth:customer', 'verified');
Route::middleware('guest')->group(function () {
    Route::get('/category-products/{name?}/', 'FrontEndController@categoryDetail')->name('category.detail');
    Route::get('/product-detail/{id?}/', 'FrontEndController@productDetail')->name('product.detail');
    Route::get('/product-cart/{qty}/{productid}', 'FrontEndController@productCart')->name('cart.index');
    Route::get('/product-subscribe/{id}', 'FrontEndController@productSubscribe')->name('subscribe.index');
    Route::post('/product-placed-order', 'FrontEndController@productPlacedOrder')->name('placed.order');
    Route::post('/coupon-radeem', 'FrontEndController@redeemCoupon')->name('coupon.radeem');
    Route::post('customer/order/subscription', 'FrontEndController@subscription');
    Route::get('customer/order/subscription-history', 'FrontEndController@subscriptionHistory')->name('customer.subscription.history');
    Route::Post('customer/order/subscription-detail', 'FrontEndController@subscriptionDetail')->name('customer.subscription.detail');
    Route::get('customer/order/jar-history/{id?}', 'FrontEndController@jarHistory')->name('customer.jar.history');
    Route::get('/customer-qr-show', 'Customer\CustomerController@showQr')->name('customer.qr');
    Route::get('customer/wallet-history', 'FrontEndController@walletHistory')->name('customer.wallet.history');
    Route::post('placed_order-schedule', 'FrontEndController@placedOrderSchedule')->name('');
    Route::get('/product-result', 'FrontEndController@search')->name('product.search');
    Route::get('/customer/feedback', 'CustomerController@feedback')->name('customer.feedback');
    Route::post('/customer/feedback/save', 'CustomerController@saveFeedback')->name('customer.feedback.save');
    Route::get('/customer/complain', 'CustomerController@complain')->name('customer.complain');
    Route::post('/customer/complain/save', 'CustomerController@saveComplain')->name('customer.complain.save');
});

// home feed products list



Route::get('/git-pull', 'Api\CommandController@gitpull');
Route::get('/artisan/{name}', 'Api\CommandController@artisanCommand');


Auth::routes();
Route::middleware('auth:web')->group(function () {
    Route::get('/home', 'DashboardController@index')->name('dashboard');
    Route::get('/profile', 'DashboardController@profile')->name('profile');
    Route::post('/profile-save', 'DashboardController@general')->name('profile.save');
    Route::post('/password-save', 'DashboardController@password')->name('password.save');

    // Coupon
    Route::prefix('/coupon')->group(function () {
        Route::get('/', 'CouponController@index')->name('coupon');
        Route::post('/store', 'CouponController@store');
        Route::post('/show', 'CouponController@show');
        Route::get('/{id}/edit', 'CouponController@edit');
        Route::post('/update', 'CouponController@update');
        Route::get('/delete/{id}', 'CouponController@delete');
    });

    // Vendor
    Route::prefix('/vendors')->name('vendor.')->group(function () {
        Route::get('/', 'VendorController@list')->name('list');
        Route::get('/add', 'VendorController@add')->name('add');
        Route::get('/edit/{id?}', 'VendorController@edit')->name('edit');
        Route::post('/save/{id?}', 'VendorController@save')->name('save');
        Route::get('/delete/{id}', 'VendorController@delete')->name('delete');
        Route::get('/approve/{id}', 'VendorController@isApprove')->name('is-approve');
        Route::get('/active/{id}', 'VendorController@isActive')->name('is-active');
        Route::get('/document/view/{id}', 'VendorController@documentView')->name('document.view');
        Route::get('/details/{id}', 'VendorController@detail')->name('detail');
    });
    //complains
    Route::prefix('/complains')->name('complain.')->group(function () {
        Route::get('/', 'ComplainController@list')->name('list');
        Route::get('/add', 'ComplainController@add')->name('add');
        Route::post('/save', 'ComplainController@save')->name('save');
        Route::get('/reply/{id}', 'ComplainController@reply')->name('reply');
        Route::post('/response/{id}', 'ComplainController@response')->name('response');
        Route::get('/delete/{id}', 'ComplainController@delete')->name('delete');
        Route::get('/approve/{id}', 'ComplainController@isApprove')->name('is-approve');
    });

    // QR Code
    Route::prefix('/QR')->group(function () {
        Route::get('/list/{id?}', 'QrCodeController@index');
        Route::get('/Reset/{id}', 'QrCodeController@Reset');
        Route::get('/create', 'QrCodeController@create')->name('QR.CREATE');
    });
    Route::prefix('/customer')->group(function () {
        Route::get('/', 'CustomerController@index')->name('customer.list');
        Route::get('/create', 'CustomerController@create')->name('customer.create');
        Route::post('/store', 'CustomerController@store')->name('customer.store');
        Route::get('/edit/{id}', 'CustomerController@edit')->name('customer.edit');
        Route::get('/view/{id}', 'CustomerController@view')->name('customer.view');
        Route::post('/update', 'CustomerController@update')->name('customer.update');
        Route::get('/delete/{id}', 'CustomerController@delete')->name('customer.delete');
        Route::post('/post-assign-group', 'CustomerController@postAssignGroup')->name('post-assign-group');
    });

    Route::prefix('new/customers')->name('newcustomer.')->group(function () {
        Route::get('/{id?}', 'AssignVendorController@list')->name('list');
        Route::get('/view/{id}', 'AssignVendorController@view')->name('view');
        Route::post('/assign-vendor{id}', 'AssignVendorController@assignVendor')->name('assign');
    });
    //invoice
    Route::prefix('/invoice')->name('invoice.')->group(function () {
        Route::get('/list', 'InvoiceController@index')->name('list');
        Route::get('/view', 'InvoiceController@view')->name('view');
    });
    //group
    Route::prefix('/group')->name('group.')->group(function () {
        Route::get('/list', 'GroupController@index')->name('list');
        Route::get('/create', 'GroupController@create')->name('create');
        Route::post('/store', 'GroupController@store')->name('store');
        Route::get('/member/{id}', 'GroupController@groupCustomer')->name('customer');
        Route::post('/change-member-position', 'GroupController@ChangeMemberPosition')->name('change.member.position');
    });
    //category
    Route::prefix('/category')->name('category.')->group(function () {
        Route::get('/list', 'CategoryController@index')->name('list');
        Route::get('/create', 'CategoryController@create')->name('create');
        Route::post('/store', 'CategoryController@store')->name('store');
    });
    //product
    Route::prefix('/product')->name('product.')->group(function () {
        Route::get('/list', 'ProductController@index')->name('list');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::Post('/store', 'ProductController@store')->name('store');
    });
    //product
    Route::prefix('/deliveryboy')->name('deliveryboy.')->group(function () {
        Route::get('/list', 'DeliveryBoyController@index')->name('list');
        Route::get('/create', 'DeliveryBoyController@create')->name('create');
        Route::Post('/store', 'DeliveryBoyController@store')->name('store');
        Route::get('/assign-group/{id}', 'DeliveryBoyController@assignGroup')->name('assign-group');
        Route::post('/post-assign-group', 'DeliveryBoyController@postAssignGroup')->name('post-assign-group');
        Route::get('/delete/{id}', 'DeliveryBoyController@delete')->name('delete');
        Route::get('/edit/{id}', 'DeliveryBoyController@edit')->name('edit');
        Route::post('/update', 'DeliveryBoyController@update')->name('update');
    });
    //feedback
    Route::prefix('/feedback')->name('feedback.')->group(function () {
        Route::get('/list', 'FeedbackController@list')->name('list');
        Route::get('/view', 'FeedbackController@view')->name('view');
        Route::get('/add', 'FeedbackController@add')->name('add');
        Route::post('/save', 'FeedbackController@save')->name('save');
        Route::get('/reply/{id}', 'FeedbackController@reply')->name('reply');
        Route::post('/response/{id}', 'FeedbackController@response')->name('response');
    });
    // New Order
    Route::prefix('/order')->group(function () {
        Route::get('list/{id?}', 'OrderController@index')->name('order.list');
        Route::get('accept/{id}', 'OrderController@accept')->name('order.list');
        Route::get('reject/{id}', 'OrderController@reject')->name('order.list');
    });
    //order deliveries
    Route::prefix('/deliveries')->name('deliveries.')->group(function () {
        Route::get('/list', 'OrderDeliveryController@index')->name('list');
    });
    //route
    Route::prefix('/route')->name('route.')->group(function () {
        Route::get('/create/{id}', 'RouteController@create')->name('create');
        Route::get('/show/{id}', 'RouteController@show')->name('show');
        Route::Post('/store', 'RouteController@store')->name('store');
        Route::get('/list', 'RouteController@index')->name('list');
        Route::get('/delete/{id}', 'RouteController@delete')->name('delete');
        Route::get('/create/assign-delivery-boy-route/{id}', 'RouteController@assignDeliveryRoute')->name('assign-deliveryboy-route');
        Route::post('/store/assign-delivery-boy-route', 'RouteController@storeAssignDeliveryRoute')->name('store.assign-deliveryboy-route');
    });

    Route::get('/about-us', 'SiteSettingController@aboutus')->name('aboutus');
    Route::get('/aboutus/{id}', 'SiteSettingController@editAbout')->name('aboutus.edit');
    Route::get('/contact-us', 'SiteSettingController@contactus')->name('contactus');
    Route::get('/contactus/{id}', 'SiteSettingController@editContact')->name('contactus.edit');
    Route::get('/term-condition', 'SiteSettingController@termAndCondition')->name('terms');
    Route::get('/terms-and-conditions/{id}', 'SiteSettingController@editTermAndCondition')->name('terms.edit');
    Route::post('/site-setting', 'SiteSettingController@saveSiteSetting')->name('save.setting');
    Route::get('/delivery/lock', 'SiteSettingController@lock')->name('delivery.lock');
    Route::get('/delivery/lock-edit', 'SiteSettingController@edit')->name('delivery.lock.edit');
    Route::post('/delivery/locked/{id}', 'SiteSettingController@locked')->name('delivery.locked');

    Route::get('/slider', 'SliderController@list')->name('slider.list');
    Route::get('/add/slider', 'SliderController@create')->name('slider.create');
    Route::post('/save-slider', 'SliderController@save')->name('slider.save');
});





// Vendor App 


Route::prefix('/vendorApp')->group(function () {

    Route::get('/', function () {
        if (\Auth::guard('web')->check()) {
            return redirect()->route('vendor-homefeed');
        }
        return view('frontend.vendor.login');
    });

    Route::middleware('auth:web')->group(function () {

        Route::get('/vendor-home-feed', [VendorController::class, 'home_feed'])->name('vendor-homefeed');
        Route::get('/vendor-today-delivery', [VendorController::class, 'todayallGroups'])->name('vendor.todayDelivery');
        Route::get('/vendor-group-delivery/{group_id}', [VendorController::class, 'todayDelivery'])->name('vendor.grouptodayDelivery');
        Route::get('/complete-profile', [VendorController::class, 'complete_profile'])->name('completeProfile');
        Route::post('/complete-profile', [VendorController::class, 'complete_profile_post'])->name('completeProfile');
        Route::get('/all-customer', [VendorController::class, 'allCustomer'])->name('vendor.allCustomer');
        Route::get('/all-delivery-boy', [VendorController::class, 'allDeliveryBoy'])->name('vendor.allDeliveryBoy');
        Route::get('/customer-view/{id}', [VendorController::class, 'customerView'])->name('vendor.customer.view');
        Route::post('/subscription-payment', [VendorController::class, 'subscriptionPayment'])->name('vendor.customer.subscription.payment');
        Route::get('/add-customer', [VendorController::class, 'addCustomer'])->name('vendor.add.customer');
        Route::get('/add-deliveryboy', [VendorController::class, 'addDeliverBoy'])->name('vendor.add.deliveryboy');
        Route::get('/add-group', [VendorController::class, 'addGroup'])->name('vendor.add.group');
        Route::post('/post-add-group', [VendorController::class, 'postAddGroup'])->name('vendor.group.postaddGroup');
        Route::get('/assign-group/{id}', [VendorController::class, 'assignGroup'])->name('vendor.assign-group');
        Route::post('/post-assign-group', [VendorController::class, 'postAssignGroup'])->name('vendor.deliveryboy.post-assign-group');

        Route::post('/store-customer', [VendorController::class, 'storeCustomer'])->name('vendor.store.customer');
        Route::post('/store-delivery-boy', [VendorController::class, 'storeDeliveryBoy'])->name('vendor.store.deliveryBoy');
        Route::get('/all-groups', [VendorController::class, 'allGroups'])->name('vendor.allGroups');
        Route::get('/group-detail/{id}', [VendorController::class, 'singleGroupDetail'])->name('vendor.singleGroupDetail');
    });
    Route::get('/vendor-login', [VendorController::class, 'show_login'])->name('vendorlogin')->middleware('web');
    Route::post('/vendor-login', [VendorController::class, 'vendorlogin'])->name('vendor.login.attempt');
    Route::get('/register', [VendorController::class, 'show_register'])->name('vendor.register');
    Route::post('/register', [VendorController::class, 'register'])->name('register.attempt');
    Route::get('/verify-otp', [VendorController::class, 'verification_page'])->name('vendor.verification');
    Route::get('/getproduct-details/{id?}', [VendorController::class, 'getProductDetails'])->name('getproduct.details');
    Route::post('/verification-otp', [VendorController::class, 'verifyOtp'])->name('vendor.otp.verify');
    Route::post('/vendor-logout', [VendorController::class, 'logout'])->name('vendorLogout');
});

// routes for the delivery boy

Route::prefix('/delivery-boy')->middleware('guest')->name('delivery-boy.')->group(function () {
    Route::get('/', [DeliveryBoyController::class, 'home'])->name('home');
    Route::get('/login', [DeliveryBoyController::class, 'show_login'])->name('login')->middleware('guest');
    Route::post('/login', [DeliveryBoyController::class, 'login'])->name('login.attempt');
    Route::get('/register', [DeliveryBoyController::class, 'show_register'])->name('register');
    Route::post('/register', [DeliveryBoyController::class, 'register'])->name('register.attempt');
    Route::get('/verify-otp', [DeliveryBoyController::class, 'show_otp'])->name('show.otppage');
    Route::post('/verification-otp', [DeliveryBoyController::class, 'verifyOtp'])->name('otp.verify');

    //todays delivery
    Route::get('/today-delivery/{query?}', [DeliveryBoyController::class, 'todayDelivery'])->name('today.delivery');
    Route::get('/payments/{query?}', [DeliveryBoyController::class, 'payments'])->name('payments');
    Route::get('/new/jar-entry', [DeliveryBoyController::class, 'newJarEntry'])->name('new.jar.entry');
    Route::get('/new/billing', [DeliveryBoyController::class, 'newBilling'])->name('new.billing');
    Route::post('/logout', [DeliveryBoyController::class, 'logout'])->name('logout');
});
