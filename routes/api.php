<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/list-city', 'UserController@listCity');
Route::post('/login', 'Api\AuthController@login');
Route::post('/logout', 'Api\AuthController@logout');
Route::group(['middleware' => 'JWT'], function () // Jwt Middle Ware To verify token
{
Route::prefix('/vendor')->group(function () {
			Route::post('/store', 'Api\VendorController@store')->withoutMiddleware(['JWT']);
			Route::post('/edit-profile', 'Api\VendorController@editProfile');
			Route::post('/edit-bank_details', 'Api\VendorController@editBankDetails');
			Route::post('/list-customers', 'Api\VendorController@listCustomers');
		});

	 	Route::prefix('/customer')->group(function () {
			Route::post('/store', 'Api\VendorController@customerStore');
		});


//customer APis
Route::prefix('/customer')->group(function () {
			Route::post('/register', 'Api\CustomerController@register')->withoutMiddleware(['JWT']);
			Route::post('/login', 'Api\CustomerController@login')->withoutMiddleware(['JWT']);
			Route::post('/logout', 'Api\CustomerController@logout');
			Route::post('/order', 'Api\CustomerController@order');
			Route::post('/search', 'Api\CustomerController@search');	
			Route::post('/order/subscription', 'Api\CustomerController@subscription');			
			});
Route::prefix('/order')->group(function () {
			Route::post('/list', 'Api\OrderController@list')->withoutMiddleware(['JWT']);
			});





});
Route::post('/plans-list', 'Api\CustomerController@plansList');




Route::post('/list-group', 'GroupController@listgroup');