<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
     const CREATED_AT = 'created_at';
     const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
    ];

 	public function customer()
    {
        return $this->hasMany(User::class,'group_id','id');
    }
	public function customerUser()
    {
        return $this->hasMany(Customer::class,'group_id','id');
    }
}
