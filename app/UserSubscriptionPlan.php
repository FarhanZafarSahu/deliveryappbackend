<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class UserSubscriptionPlan extends Model
{

 	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        		'user_id',
				'plan_id',
				'qty',
				'empty_jar_qty',
				'per_unit_empty_jar_price',
				'product_id',
				'is_today_delivery',
				'next_delivery_date',
				'total_jar_qty',
				'subscription_end_date',
				'advance_payment',
    ];
public function getArrayResponse() {
        return [
            'id'               			 => $this->id,
            'user_id'          			 => $this->user_id,
			'plan_id'      	   			 => $this->plan_id,
			'qty'              			 => $this->qty,
			'empty_jar_qty'	  			 => $this->empty_jar_qty,
			'per_unit_empty_jar_price' 	 => $this->per_unit_empty_jar_price,
			'product_id'   				 => $this->product_id,
			'is_today_delivery'			 => $this->is_today_delivery,
			'next_delivery_date'		 => $this->next_delivery_date,
			'advance_payment'			 => $this->advance_payment,
        ];
    }
	protected $dates = ['subscription_end_date'];
     public static function store($customer_id, $plan_id, $qty, $empty_jar_qty, $per_unit_empty_jar_price, $product_id , $next_delivery_date , $isAdvancePayment, $productType)
	{
			$todate  	= \Carbon\Carbon::now()->today();
			$endofmonth = \Carbon\carbon::now()->endOfMonth();
			$days 		= $endofmonth->diffInDays($todate);

			$advancePaymentRate = ($productType == 1) ? config('constant.ADVANCE_PAYMENT_CHILLED_WATER') : config('constant.ADVANCE_PAYMENT_REGULAR_WATER');
			$advancePayment = ($isAdvancePayment == 0 && $empty_jar_qty == 0 ) ? $qty*$advancePaymentRate : 0 ;
		 return self::create([
								"user_id"           	   => $customer_id,
								"plan_id"            	   => $plan_id,
								"qty"       		       => $qty ,
								"empty_jar_qty"            => $empty_jar_qty,
								"per_unit_empty_jar_price" => $per_unit_empty_jar_price,
								"product_id"  		       => $product_id,
								"is_today_delivery"        => 1,
								"next_delivery_date"	   => $next_delivery_date,
								"total_jar_qty"			   => $days * $qty ,
								"subscription_end_date"	   => $endofmonth,
								"advance_payment"		   => $advancePayment
								]);
	}
public function plan()
    {
        return $this->hasOne(Plan::class,'id','plan_id');
    }
public function order()
    {
        return $this->hasMany(Order::class,'subscription_id','id');
    }
public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

public function customer()
    {
        return $this->hasOne(Customer::class,'id','user_id');
    }
public function paymentHistory()
	{
        return $this->hasOne(SubscriptionPaymentHistory::class,'subscription_id','id');
	}
}
