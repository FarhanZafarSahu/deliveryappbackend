<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Constants\Message;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class Customer extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

	// public function getJWTIdentifier()
    // {
    //     return $this->getKey();
    // }

    // public function getJWTCustomClaims()
    // {
    //     return [];
    // }
	const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'email',
        'phone',
        'land_mark',
		'is_active',
		'image',
		'qr',
		'vendor_id',
		'group_id',
		'password',
    ];
	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {

        return [
            'name'             => $this->name,
            'email'              => $this->email,
            'address'               => $this->address,
            'land_mark'                => $this->land_mark,
            'phone'          => $this->phone,
            'is_active'          => $this->is_active,
            'qr'          => $this->qr,
            'image'          => $this->image,
			'vendor_id' => $this->vendor_id,
               ];
    }
public function vendor()
    {
        return $this->belongsTo(User::class,'vendor_id','id');
    }
public function assignedBy()
    {
        return $this->belongsTo(User::class,'assigned_by','id');
    }
public function subscriptions()
    {
        return $this->hasOne(UserSubscriptionPlan::class,'user_id','id')->where('subscription_end_date','>=',\Carbon\Carbon::now()->format('Y-m-d H:i:s'));
    }
public static function Search($searchValue = NULL, $user_id = NULL)
    {
        $searchValue = escape_like($searchValue);
        $result = Customer::where( 'vendor_id' , $user_id);
        if(customIsset($searchValue))
        {
            $result = $result->where(function($q) use ($searchValue) {
                        $q->where( 'name' , 'like' , '%'.$searchValue.'%')
                        ->orWhere( 'phone' , 'like' , '%'.$searchValue.'%');
                    });
        }
        $result = $result->get();
        $result = [
            'products' => $result,
            'total_found' =>count($result),
        ];
        return $result;
    }
 public static function Subscription()
    {
		$subscriptions		  = UserSubscriptionPlan::where('is_today_delivery',1)->whereDate('next_delivery_date','=' , Carbon::now()->today()->format('Y-m-d'))->whereDate('subscription_end_date', '>=', Carbon::now()->today()->format('Y-m-d H:i:s'))->get();
		foreach($subscriptions as $subscription)
		{
			$product_details  = Product::where('id',$subscription->product_id)->first();
			$deliveryCharges  = ($subscription->qty* config('constant.DELIVERY_CHARGES'));
			$totalOrderAmount = $subscription->qty * $product_details->price + $deliveryCharges ;
			$orderTime  	  = \Carbon\Carbon::now();
			$orderNo 		  = 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			$order_detail 	  = Order::where('customer_id',$subscription->user_id)->where('subscription_id',$subscription->id)->first();
			$order 			  = Order::store($order_detail->no_of_items, $totalOrderAmount, 0, $orderTime, $orderNo, $order_detail->delivery_mobile_number, $order_detail->delivery_name, $order_detail->delivery_address, $subscription->user_id,$orderTime,$subscription->qty , $subscription->id);

			OrderDetail::store($order->id, $product_details->id, $subscription->qty, $product_details->price);

			$next_delivery_date = Order::CalculateNextDeliveryDate($subscription->plan_id);
			$subscription->next_delivery_date = $next_delivery_date ;
			$subscription->save();

		}
		echo "Reminder Sent";
    }
}

