<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPaymentHistory extends Model
{
      protected $fillable = [
        		'subscription_id',
				'total_amount',
				'balance_amount',
				'payed_amount',
    ];
public function getArrayResponse() {
        return [
            'id'               			 => $this->id,
            'subscription_id'          			 => $this->subscription_id,
			'total_amount'      	   			 => $this->total_amount,
			'balance_amount'              			 => $this->balance_amount,
			'payed_amount'	  			 => $this->payed_amount,
        ];
    }
}
