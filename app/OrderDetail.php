<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    const CREATED_AT = 'created_at';
     const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'per_unit',
       
    ];

    public static function store($order_id, $product_id, $quantity, $per_unit)
	{
		 return self::create([
								"order_id"            => $order_id,
								"product_id"          => $product_id,
								"quantity"            => $quantity,
								"per_unit"       	  => $per_unit ,
							]);
	}

 public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
}
