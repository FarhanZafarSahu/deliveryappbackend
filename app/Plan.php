<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_name', 'is_active'
    ];
public function getArrayResponse() {
        return [
            'id'               			 => $this->id,
            'plan_name'          			 => $this->plan_name,
			'is_active'      	   			 => $this->is_active,
		];
    }

    // public function UserSubscriptionPlan()
    // {
    //     return $this->hasMany(UserSubscriptionPlan::class);
    // }
}
