<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    protected $fillable = ['about_description','about_img','contact_mobile','contact_address','t_description','lock_time'];
}
