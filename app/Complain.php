<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $fillable = ['vendor_id','customer_id','name','phone_number','message','status','complain_reply'];
}
