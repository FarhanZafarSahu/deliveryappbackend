<?php

namespace App\Console\Commands;

use App\Customer;
use Illuminate\Console\Command;

class CustomerSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Customer:Subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run according to Customer Subscription Plan ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
         Customer::Subscription();
    }
}
