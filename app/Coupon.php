<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'customer_id',
        'user_id',
        'amount',
        'coupon_code',
		'is_percentage',
		'city_id',
		'coupon_expiry',
    ];

    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {
        
        return [
            'product_id'             => $this->product_id,
            'customer_id'              => $this->customer_id,
            'user_id'               => $this->user_id,
            'amount'                => $this->amount,
            'coupon_code'          => $this->coupon_code,
			'city_id'				=> $this->city_id,
			'coupon_expiry'      => $this->coupon_expiry,
               ];
    }

}
