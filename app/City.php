<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'city';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'countryId',
        'city',
        'isActive',
        'zone',
        'pickup'
    ];

    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {
        
        return [
            'id'        => $this->id,
            'countryId' => $this->countryId,
            'city'      => $this->city,
            'isActive'  => $this->isActive,
            'zone'      => $this->zone,
            'pickup'    => $this->pickup
        ];
    }
}
