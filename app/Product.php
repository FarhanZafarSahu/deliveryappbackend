<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      const CREATED_AT = 'created_at';
     const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_title',
        'description',
		'price',
		'is_active',
		'image',
		'unit',
		'category_id',
		'product_type',
    ];
public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
