<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','photo','phone_number','brand_name','business_name','business_address','pin_code','alternative_phone_number','role_id','is_approved','is_active','license_no','sale_price','document','land_mark','group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
  public function getArrayResponse() {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
			'email'      	   => $this->email,
			'phone_number'     => $this->phone_number,
			'photo'	 		   => $this->photo,
			'brand_name' 	   => $this->brand_name,
			'business_name'    => $this->business_name,
			'business_address' => $this->business_address,
			'pin_code' 		   => $this->pin_code,
			'alternative_phone_number' => $this->alternative_phone_number,
			'role_id' 		   => $this->role_id,
			'is_approved' 	   => $this->is_approved,
			'is_active'        => $this->is_active,
        ];
    }
 public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

  public function bankDetails()
    {
        return $this->hasOne(VendorBankDetail::class,'user_id','id');
    }
	public function group()
    {
        return $this->hasOne(Group::class,'id','group_id');
    }

	public function location()
    {
        return $this->hasMany(Route::class,'user_id','id');
    }
	public function orders()
    {
        return $this->hasMany(Order::class,'vendor_id','id');
    }
	public function locationAsc()
	{
        return $this->hasMany(Route::class,'user_id','id')->orderby('route_sequence','asc');
	}
 	public function orderDetails()
    {
       return $this->hasManyThrough(
            Order::class,
            OrderDetail::class,
            'order_id', // Foreign key on the OrderDetails table...
            'vendor_id', // Foreign key on the Orders table...
            'id', // Local key on the User table...
            'order_id' // Local key on the OrderDetails table...
        );
    }
}
