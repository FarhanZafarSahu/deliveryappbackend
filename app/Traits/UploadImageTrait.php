<?php
namespace App\Traits;
use Illuminate\Http\Request;
trait UploadImageTrait {
    /**
     * @param Request $request
     * @return $this|false|string
     */
public function uploadImageTrait(Request $request, $file = 'image', $directory = 'public/propertyThumbnail' ) 
{
       if ($request->hasFile($file))
    	{		
				$extension = $request->file($file)->getClientOriginalExtension();
				$fileName  = $file . time() . md5($request->file($file)) . rand(1111111111,9999999999). carbonDate(\Carbon\Carbon::now(),"s"). '.'  . $extension;
                if (!\Storage::exists($directory))
				{
                    \Storage::makeDirectory($directory);
                }
                try
				{
					return $request->file($file)->storeAs($directory ,$fileName);
				}
  				catch (\Exception $e) 
                {
                  logError(__METHOD__, __LINE__ , $e);
            	}
        }
		return null ;
}
}