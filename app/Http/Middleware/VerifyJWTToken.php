<?php

namespace App\Http\Middleware;

use Closure;
use App\Constants\Message;
use App\Constants\ResponseCode;
use JWTAuthException;
use JWTAuth;
class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = invalidTokenResponse();
        try
        {      
            $token = is_null($request->header('Authorization'))?$request->input('token'):substr($request->header('Authorization'),7);
            $user = JWTAuth::user();
            if(empty($user))
            {
                $response = makeResponse(ResponseCode::FAIL,Message::INVALID_TOKEN,false);
                return response()->json($response);
            }
        }
        catch (JWTException $e) 
        {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) 
            {
                $response = makeResponse($e->getStatusCode(),Message::TOKEN_EXPIRED,false);
                return response()->json($response);
            }
            else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) 
            {
                $response = makeResponse($e->getStatusCode(),Message::INVALID_TOKEN,false);
                return response()->json($response);
            }
            else
            {
                $response = makeResponse($e->getStatusCode(),Message::TOKEN_EXPIRED,false);
                return response()->json($response);
            }
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) 
        {
            $response = makeResponse($e->getStatusCode(),Message::TOKEN_EXPIRED,false);
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) 
        {
            $response = makeResponse($e->getStatusCode(),Message::INVALID_TOKEN,false);
            return response()->json($response);
        }
        catch (\Tymon\JWTAuth\Exceptions\JWTException $e) 
        {
            $response = makeResponse($e->getStatusCode(),Message::TOKEN_NOT_FOUND,false);
            return response()->json($response);
        }
        return $next($request);
    }
}