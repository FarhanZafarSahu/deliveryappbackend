<?php

namespace App\Http\Resources\Order;

use App\Models\{Country,ProductRecipt,City,PreOrderRequestPayment};
use Illuminate\Http\Resources\Json\JsonResource;

class OrderCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id'                    	=> $this->id,
            'no_of_items'           => $this->no_of_items,
            'total_price'           => $this->total_price,
            'discounted_price'      => $this->discounted_price ,
            'discount_amount'       => $this->discount_amount,
            'delivery_charges'      => $this->delivery_charges ,
            'order_time' 			=> $this->order_time,
			'order_no' 				=> $this->order_no,
            'delivery_name'         => $this->delivery_name,
            'delivery_address'      => $this->delivery_address,
 			'delivery_mobile_number'=> $this->delivery_mobile_number,
			'pickup_name'    		=> $this->pickup_name,
            'pickup_address'        => $this->pickup_address,
            'pickup_mobile_number'  => $this->pickup_mobile_number,
            'status'    			=> $this->status,
            'customer_id'         	=> $this->customer_id,
			'order_details'			=> $this->orderDetails,
		              ];
    }
}
