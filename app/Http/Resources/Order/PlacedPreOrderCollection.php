<?php

namespace App\Http\Resources\Order;

use App\Models\{Country,City,PreOrderRequestPayment};
use App\Models\ProductRecipt;
use Illuminate\Http\Resources\Json\JsonResource;

class PlacedPreOrderCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'orderNo'               => $this->orderNo,
            'status'                => $this->status,
            'discountedPrice'       => (($this->per_unit_price * $this->quantity) + $this->delivery_charges),
            'currency_symbol'       =>  Country::where('country_code',"PK")->value('currency_symbol'),
            'noOfItems'             => 1 ,
            'paymentType' 			=> isset($this->paymentMethod) ? $this->paymentMethod->title : '',
            'varianceAmount'        => 0,
          	'status_label'          => isset($this->Orderstatus) ? $this->Orderstatus->label : '',
 			'status_label_color'    => isset($this->Orderstatus) ? $this->Orderstatus->color : '',
            'delivery_dukanName'    => $this->user->dukanName,
            'delivery_name'         => $this->full_name,
            'delivery_address'      => $this->address,
            'delivery_contactNo'    => $this->contactNo,
            'delivery_city'         => City::where('id',$this->city_id)->value('city'),
            'delivery_country'      => Country::where('id',City::where('id',$this->city_id)->value('countryId'))->value('country'),
            'pickup_dukanName'      => $this->product->user->dukanName,
            'pickup_name'           => $this->product->user->fullName,
            'pickup_address'        => $this->product->user->address,
            'pickup_phone'          => $this->product->user->contactNo,
            'pickup_city'           => $this->product->user->city ,
            'pickup_country'        => $this->product->user->country,
            'totalPrice'            => (int)(($this->per_unit_price * $this->quantity) + $this->delivery_charges), //price without discount and deliverycharges
            'discountAmount'        => 0,
            'deliveryCharges'       => (int)$this->delivery_charges,
            'sellerId'              => $this->product->user->id,
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'product_details'       => $this->product,
			'orderTime'             => $this->created_at->format('Y-m-d H:i:s'),
			'orderType'             => 7,
			'size'				    =>$this->size,
			'quantity'       		=> $this->quantity,
			'currency_rate'    		=> 	'1.00',
			"pre_order_history"     => PreOrderHistoryCollection::collection(PreOrderRequestPayment::where('pre_order_request_id',$this->id)->with('paymentMethod')->get()),
			'payment_type_id' 		=> $this->payment_type,
			'firstInstallment'      => (($this->quantity * $this->per_unit_price/100) * $this->advance_percentage),

        ];
    }
}
