<?php

namespace App\Http\Resources\SubscriptionHistory;

use App\Order;
use App\OrderDetail;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSubscriptionHistoryCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          	'id'                    => $this->id,
            'total_jar_qty'			=> $this->total_jar_qty,
			'used_jar'				=> OrderDetail::whereIn('order_id',Order::where('is_accept',1)->where('subscription_id',$this->id)->pluck('id'))->sum('quantity') ,
			'total_price'			=> $this->qty * $this->product->price,
			'created_at'			=> $this->created_at,
			'subscription_end_date'	=> $this->subscription_end_date,
			'price'	=> $this->product->price,
			'qty'	=> $this->qty,
			'paymentHistory' => $this->paymentHistory,
		    ];
    }
}
