<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Slider;
use Illuminate\Http\Request;

//constant
use App\Constants\Message;
use App\Coupon;
use Symfony\Component\HttpKernel\Event\RequestEvent;

//Constant
use App\Constants\ResponseCode;
use App\UserSubscriptionPlan;
use Carbon\Carbon;
use Symfony\Component\Console\Input\Input;

class FrontEndController extends Controller
{
    public function homeFeed(Request $request)
	{

        $products = Product::orderBy('created_at','DESC')->get();
        $categories = Category::orderBy('created_at','DESC')->get();
        $slider = Slider::first();
	    return view('frontend.homefeed',compact('products','categories','slider'));
	}

 	public function categoryDetail(Request $request, $name=null)
	{
            $category = Category::where('name',$name)->first();
            $product = Product::where('category_id',$category->id)->get();
	         return view('frontend.product.detail',compact('product'));
	}
    public function productDetail($id)
    {
            $product = Product::where('id',$id)->get();
            return view('frontend.product.detail',compact('product'));
    }

    public function getProductDetails($id)
    {
        $product = Product::find($id);
        $filterdata = view('frontend.modal.imagedetail')->with('product', $product)->render();
        return response()->json($filterdata);

    }
	public function productCart($quantity , $productid)
	{
 		$product = Product::where('id',$productid)->first();
	    return view('frontend.cart.index')->with(['qty' => $quantity , 'product' => $product]);
	}

	public function productSubscribe($id)
	{
 		$user =  \Auth::guard('customer')->user();
		$product = Product::where('id',$id)->first();
		$user_subscription = UserSubscriptionPlan::where('user_id',$user->id)->latest()->first();
		$is_advance_payment = $user_subscription->advance_payment ?? 0;
		if($product->product_type == 1)
			$advance_payment = config('constant.ADVANCE_PAYMENT_CHILLED_WATER');
		else
			$advance_payment = config('constant.ADVANCE_PAYMENT_REGULAR_WATER');
	    return view('frontend.subscribe.index')->with(['product'=>$product , 'is_advance_payment' => $is_advance_payment , 'advance_payment' => $advance_payment]);
	}
	 public function redeemCoupon(Request $request)
    {
        $user =  \Auth::guard('customer')->user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'REDEEM_COUPON');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        {
            $couponNumber = $request->couponNumber ;
			$qty 		  = $request->qty ;
			$productId	  = $request->productId ;
            $coupon = Coupon::where('coupon_code',$couponNumber)->first();
			$product = Product::where('id',$productId)->first();
            if(empty($coupon))
                return $response = makeResponse(ResponseCode::FAIL,"Coupon Doesn't Match \n Please Enter Valid Coupon.",false);
            $totalCartAmount = ($product->price * $qty) + config('constant.DELIVERY_CHARGES');
            if( $totalCartAmount - $coupon->amount <= 0)
                return $response = makeResponse(ResponseCode::FAIL,"Coupon Not Verified. \n This coupon can't be availed \n Please Contact Seller.",false,null);
            \DB::commit();
            $result = [
                'coupon' => $coupon,
            ];
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
            }
            catch (\Exception $e)
            {
                $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
            }

        return $response;
    }
	 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function productPlacedOrder(Request $request)
	{
        $validator = validateData($request,'ORDERPLACED');
        if ($validator['status'])
			return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
       try
        {
            $user              =  \Auth::guard('customer')->user();
            $customer_id       =  isset($user) ? $user->id : 2;
            $deliveryAddress   = $request->buyer_address;
            $deliveryName      = $request->buyer_name;
            $buyerContactNo    = $request->buyer_contact_no;
			$product		   = Product::where('id',$request->productId)->first();
			$delivery_date     = Carbon::parse($request->delivery_date);
			$product = Product::where('id',$request->productId)->first();

			$totalOrderAmount = ($product->price * $request->quantity) +($request->quantity * config('constant.DELIVERY_CHARGES'));
			$no_of_items 	   = 1;
			$discountAmount    = 0;
			$orderTime 		   = \Carbon\Carbon::now();
			$orderNo = 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			$order = Order::store($no_of_items, $totalOrderAmount, $discountAmount , $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer_id , $delivery_date ,$request->quantity);
			OrderDetail::store($order->id, $product->id, $request->quantity, $product->price);
			\DB::commit();
			$result = [
			'order' => $order,
		];
			return \Redirect()->route('homefeed')->with('success', Message::ORDER_PLACE);
        }
       catch (\Exception $e)
        {
  			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
        }
	}
public function customerLogin(Request $request)
    {
		return view('frontend.customer.login');
    }
public function verifyOtp(Request $request)
    {
		return view('frontend.customer.verifyOtp');
    }
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscription(Request $request)
    {
  		$user =  \Auth::guard('customer')->user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'SUBSCRIPTION_NEW');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
       try
        {

            $user             		= \Auth::guard('customer')->user();
			$subscription = UserSubscriptionPlan::where("user_id",$user->id)->whereDate('subscription_end_date' ,'>=', \Carbon\Carbon::now()->today()->format('Y-m-d H:i:s'))->first();
			if($subscription != null)
				return makeResponse(ResponseCode::FAIL,Message::ALREADY_SUBSCRIPTION,false,null);
			$customer_id       		= isset($user) ? $user->id : 59 ;
            $deliveryAddress   		= $user->address;
            $deliveryName      		= $user->name;
            $buyerContactNo    		= $user->phone;
			$product 				= Product::where('id',$request->productId)->first();
			$quantity          		    = $request->qty;
		   	$totalOrderAmount 		= ($product->price * $quantity) + ($quantity * config('constant.DELIVERY_CHARGES'));
			$no_of_items 	   		= 1;
 			$coupon 				= Coupon::where('coupon_code',$request->couponNumber)->first();
			$discountAmount    		= isset($coupon) ? $coupon->amount : 0 ;
			$orderTime 		   		= \Carbon\Carbon::now();
			$plan_id  		   		= $request->plan_id;

			$emptyJarQty       		= $request->empty_jar_qty;
			$perUnitEmptyJarPrice 	= 20;
			$perUnit 		   		= $product->price;
			$productId 		   		= $request->productId;
			$isAdvancePayment		= $request->is_advance_payment;
			$productType			= $request->product_type;
			$orderNo 				= 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;

			// calculate next date of delivery.
			$next_delivery_date = Order::CalculateNextDeliveryDate($plan_id);
			$subscription 		= UserSubscriptionPlan::store($customer_id, $plan_id, $quantity, $emptyJarQty, $perUnitEmptyJarPrice, $productId, $next_delivery_date , $isAdvancePayment, $productType);
			if($plan_id != 4)
			{
				$delivery_date = \Carbon\Carbon::now()->today();
				$order 	= Order::store($no_of_items, $totalOrderAmount, $discountAmount , $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer_id , $delivery_date , $quantity , $subscription->id);
				OrderDetail::store($order->id, $productId, $quantity, $perUnit);
			}
			\DB::commit();
			$result = [
			'order' => $order,
		];
		$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
        }
        catch (\Exception $e)
        {
  			\DB::rollBack();
            return makeResponse(ResponseCode::ERROR,$e->getMessage(),false, $e->getMessage());
        }
        return $response;
    }
	public function subscriptionHistory()
	{
  		$user =  \Auth::guard('customer')->user();
		$subscription = UserSubscriptionPlan::with('plan')->where('user_id',$user->id)->get();
		$result = [
		'subscription' => $subscription,
			];
		return view('frontend.customer.subscriptionHistory')->with(['result' => $result]);
	}
	public function subscriptionDetail(Request $request)
	{
  		$response = defaultErrorResponse();
		$validator = validateData($request,'SUBSCRIPTIONSDETAILS');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        {
			$id = $request->id;
			$subscription = Order::where('subscription_id',$id)->with('orderDetails')->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get();
			$result = [
			'subscription' => $subscription,
				];
		$data =  view('frontend.customer.jarHistoryDetails')->with(['result' => $result])->render();		
			$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$data);
 		}
        catch (\Exception $e)
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
	}
	public function jarHistory($id = 1)
	{
  		$user =  \Auth::guard('customer')->user();
		$subscription = UserSubscriptionPlan::where('plan_id',$id)->where('user_id',$user->id)->with(
								['order' => function ($query) {
            							$query->with('orderDetails')->get();
								}])->get();
		$out = Order::where('is_accept',1)->where('customer_id',$user->id)->with(['subscription' => function($query) use ($id){
								$query->where('plan_id',$id);
								}])->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get();
		$result = [
		'subscription'  => $subscription	,
		'out'		    => $out->sum('quantity'),
		'in'			=> $subscription->sum('total_jar_qty'),
		'filter'		=> $id,
			];
		return view('frontend.customer.jarHistory')->with(['result' => $result]);
	}
	public function walletHistory()
	{
  		$user =  \Auth::guard('customer')->user();
		$subscription = UserSubscriptionPlan::where('user_id',$user->id)->where('subscription_end_date', '>=', \Carbon\Carbon::now()->today()->format('Y-m-d H:i:s'))->first();
		$id = isset($subscription) ? $subscription->id : 0 ;
		$out = Order::where('subscription_id',$id)->where('customer_id',$user->id)->with(['subscription' => function($query) use ($id){
								$query->where('id',$id);
								}])->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get()->sum('quantity');
		$allSubscription = UserSubscriptionPlan::where('user_id',$user->id)->where('subscription_end_date', '>=', \Carbon\Carbon::now()->today()->format('Y-m-d H:i:s'))->latest()->first();
		$result = [
		'allSubscription' => $allSubscription,
		'subscription' => $subscription,
		'balance'		   => (isset($subscription) ? $subscription->total_jar_qty : 0) - $out ,
			];
		return view('frontend.customer.walletHistory')->with(['result' => $result]);
	}
	public function placedOrderSchedule(Request $request)
	{
		$response = defaultErrorResponse();
        $validator = validateData($request,'PLACED_ORDER_SCHEDULE');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
		try
		{
			$subscription  = UserSubscriptionPlan::where('id',$request->id)->first();
			$product_details  = Product::where('id',$subscription->product_id)->first();
			$deliveryCharges  = ($request->qty * config('constant.DELIVERY_CHARGES'));
			$totalOrderAmount = $request->qty * $product_details->price + $deliveryCharges ;
			$orderTime  	  = $request->date;
			$delivery_address = $request->buyer_address;
			$delivery_mobile_number = $request->buyer_contact_no;
			$delivery_name = $request->buyer_name;
			$orderNo 		  = 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			$order 			  = Order::store(1, $totalOrderAmount, 0, $orderTime, $orderNo, $delivery_mobile_number, $delivery_name, $delivery_address, $subscription->user_id,$orderTime,$request->qty , $subscription->id);
			OrderDetail::store($order->id, $product_details->id, $request->qty, $product_details->price);
			$subscription->next_delivery_date = null ;
			$subscription->save();
            \DB::commit();
			$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null , $request->qty);
		}
		catch (\Exception $e)
		{
			\DB::rollBack();
			return makeResponse(ResponseCode::ERROR,$e->getMessage(),false, $e->getMessage());
		}
		return $response;
	}

    public function search(Request $request)
    {
        $queryy = $request->queryy;
        if($request->has('queryy'))
        {
            $products = Product::where('product_title','LIKE','%'.$request->queryy.'%')->get();
        }
        else{
            $products = Product::orderBy('created_at','DESC')->get();
        }
        return view('frontend.product.search',compact('products','queryy'));
    }
}
