<?php

namespace App\Http\Controllers;

use App\QrCode;
use Illuminate\Http\Request;
// Constant
use App\Constants\ResponseCode;

class QrCodeController extends Controller
{
  public function index($batch = NULL)
    {
        $user = \Auth::user();
        try 
        {   
                $QrCode = QrCode::all();
                return view('qrCode.index')->with(['Qr'=>$QrCode]); 
        }
        catch (\Exception $e) 
        {
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
    public function Reset($qr)
    {
        $response = defaultErrorResponse();

        \DB::beginTransaction();
        try 
        { 
            QrCode::where('qr',$qr)
            ->update([
                "NameOfBuyersDukan"         => null,
                "address"                   => null,
                "item"                      => null,
                "price"                     => 0,
                "reciptId"                  => 0,
                "isUpdated"                 => 0,
            ]);
            \DB::commit();
            return \Redirect::back()->with('success', 'QR Code Reset!');
        }
        catch (\Exception $e) 
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function create(Request $request)
    {
        $user =  \Auth::user();
        $response = defaultErrorResponse();

        \DB::beginTransaction();
        try 
        {
            $qrnumber = [];
            isset($request->qty) ? $qty = $request->qty : $qty = 1;
            $result = [];
            for($i=0;$i<$qty;$i++)
            {
                $sec      = str_pad(rand(1,999999999999), 7, '0', STR_PAD_LEFT);;
                $qrcode  = QrCode::create([
                                        "qr_number"    => $sec,
										"vendor_id"   => $user->id,
                                    ]);
                                    $qrnumber[] =    $qrcode;       
            
// 				$file = base64_encode(\Qr::format('png')->merge('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAACWCAYAAAB3jPUmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAyMDowNzoxNyAxMjo0NTowMkxbzg0AABt6SURBVHhe7d1bjGVVncfx5f1+BcE7QoPQ3BEBAZFG8QJvJmJ8ME6iJBPmbWJm3ubBTEwmMw/zNpOZp3ESE+MlRk1UpEXEFuUmgjatgKigKCji/a4z/WHVog/FOXuvU+ecqr32+X+TnaqurrNr7121fud/X4/7v4OkIAiCAfD4jY9BEAQ7TghSEASDIQQpCILBEIIUBMFgCEEKgmAwhCAFQTAYQpCCIBgMIUhBEAyGEKQgCAZDCFIQBIMhBCkIgsEQghQEwWAIQQqCYDCEIAVBMBhi/Eir/OlPKX33uyndfHM+7rsvpb/+NaVnPCOlE05I6fWvT+mUU1J68pM3XhAEwycspBYhRnffndJtt6V04EBKP/pRFiP85jcp3XtvSt/5Tko//nFKf/5z/noNv/hFFrb770/pD39IKd6rgm0mLKSW8Kv64x9TuuuulPbtS+nb307ppz/NX5uEVXTUUSldcEFK556b0jOfefA3/biN/9yEc/761yn95CfZ4vLxqU9N6UUvyud47nPzv4NgGwhBaonf/S6L0A03pHTrrSn9/OfZMtr8KyQ+ROh1r0vpkktSOuyw2YL0y1+mdPvt+fjBD/K/CZrXn3hiSscfn9Lu3RvfHASrJVy2VmDFcNGuuSalr30tpZ/9LKW//GW6W+VrXDWuXdf7DffuG99I6YtfTOmmm7Kbx/3j8hE+Vti11258cxCsnhCkocMCeuihlG68MaWrr07plluyZdQlNE94QkoveEFKRx6Z0pOetPHFCbyWa3bddVngvvnNLHDiRkSOkPmcxfTlL2+8KAhWTwjS0BFo3r8/pS99KX8kFH087WkpHXNMSq94xWPjP8SIwBG2vXuzq0aAZrE5PhUEKyQEacj86lcpffWrWThk1WozZmJG4j8vf3mOB03Gj1hXxM05WUAsoiAYCCFIQ4ULxaUSw7njjpR+//uN/+iAq/bKV+bsGkESmH78xq+YZXTPPVmMxIzEiUKMgoERgjQ0CAc3jXBceWVK3/veoRqjLlhBL3tZSq99bS6KPPzw/LUSaxKsFqT+3Ofy5+XrXSiyJHBBsE2EIA2NH/4wpauuSun663PguUY4nvjEnJqX5j/ttBxDKhAl7h6rSHZO/KjmnE95Skonn5zSRRdtfCEIVk/UIQ0FwWpi9JWvzCdGrJjjjssFkCed9OiaI24eC0scihjVnNNrn/e8fE7WljaUZz1r4z+DYLWEIA2B3/42V0kreNSX9uCD/cIhNiRGxDI677wsHM9+dhYULp7s2Le+ldP2MmliUjVipFzg9NNTOuOM7K4RvCDYJkKQdhpWjCJEQqTwkRVTE2x+/vOzYBTLiHAUy0gmjcARI+dUANl3TgLHuiJEr3lNztBNnjMItoEQpJ2EZcR6kU0jSjVWDASszzwzpVe/OtcbPf3ph4RDb5vqay4aC0mFd985Zede/OKUXvWqlM46K/ewTSuoDIIVE4K0UxCOO+/M8R19aTVpfYFmYiRwzU07+ugsJlDcSNCcS1U3MeoqeCx4vQJKltE556T0kpccKhUIgm0m/vJ2ApYRwShtGzXV1+BScaf27MlWTBEjaIr9+tdzuYC6pRoxgq5+4uZ44QtDjIIdZXELSc2MgjtVxdLNTH89VMF0WEbiOiwjzazEqeZXwDUjRqeemuuNJsXI89cK4nDOGjHi4h17bEpnn53dNL+ziBcFO8xigmSYl9iHd2aZIVkf4yoEW72bC4oSKXUy6462DwFrwWtpfbVBNcFrz48bdf752aWSki/CQXiUCjgfN612IBvXb9eubGkJiAuQhxgFA2AxQfrwh3McRBsCt4PwWDD+wL3jegf2h+/zyXf0daR0zgs4l+fVB+Eg7oLX4kZS8uU5EjPWkOC1Q/U1Mer7dRIeVhYxUmvk97Xuv5tgMCwmSO99bw6kCsiW0/jjlqHRZf7Sl+Y/eh/FJ1hNJhCuE+qBWDFcNFbMtAmP0yBGaovEdtQaCWaX+A7hESdSt0TgWEY11pY6Jb8PvW6qsCOtHwyMxQTpHe/Y+GQGxKmIELeDtcRq8jWCxaIa84Iw4dG42TLhsbb6Whpfcyw3jUulUtpz8lpixk3WdCswXlNECb8DBY/cPgIXY2mDAbJaQZrEApBeFpx1sJgsEu/aY9wZg0tGOMR3TGOUBat51J6HOJy+NLuGTAqHBILYkyZZsaiaUgFCRtCIEcuIC8j6CoIBspggvfvdOUtUi4Xg3d8CkSnyTs0tEWMam7Uknf+FL+ShaoSk5jELYCt4vPji/HwmCx5ZW6ws5yRKNQWPIHCyaKwtdUsEbsxWadA0iwnS+9+fF4fWhHlhFWlPsPAcZZeLySxSi5Tqa02ygs2EpAYBa4FrwsGKmQw0s66k9FlGtj2qHdR2xBFZjJQLsEojeB0MnMUE6dOfzpaARTIZ2J4XFpNFyDoQOyFKLKaWFpB7JxRKID7zmZwBq8mkEV8xNZXSb3xjFuciyM5J7ImROUa1pQKSCs95Tu5zY20RprCKggZYTJBkj4iRimPB261isVhAYkoWJNdCMJcF1cpCYgmJFZk7ZOQHIal5tCwjmTRiLMY2GU9jGbG0nFd5RU3MCLKazsk6YnlGHVjQCIsJEh54ILsmLCW1NrXd6rOwIC2iyfiSRavocqhIu7t/LhXhqLl/1p97UylNPIjIZNuG50iItIKoW6qpvhajI2rqlqL6OmiQxQXJy00htCgFcNXFsJy4L45pGxn2YWESIJk4FhNriUvHtds8tH4nITyEQyaNJaOFo2/crGt3f+JlXCqumnabIkaExznVLTmIfM05iZGyCjEosaiovg4aZHFBgoVJfIiSVPf3v58XlT3iFU7WNnpOYoGyImSeLDSxJR8JFLEawmJTHV1aQdxzTcGj+A4rRj2QcR+svzLqwzPk7olDqV1y/ppn53koeHQ+YiQmFQHsoEGWI0gF7+SGg2m0VZEssCu2RKjEQyzYvnf7aRAlgW4xJQtOlXEZYr8TEGCWi8pr7mqtS8XCI0ayXmqMiFG5BzEoolbGkXhmNb8a5/Q8CBwrsvUsZbDWLFeQNuMd3js+S4kbpxlXzKm2hmYz3vVVenNz9GOp/t7uIj+ZM/fEKpL9qhEOAiFgz/VUoEhUJ608Ik68nY/A1QziZ0GyhFiOxMi5o+AxaJzVCZLTsobKQZy886vRYQkUUZonAG4Bl4XIyrAjhjjTdlkExIiwattgybAE+66/uJ1EyNB8gqqHrMSMWEalbslHRZQ1z0TBo8ycmJFzEqOwjILGWa2FNAlRElfSe8VSMvOZMLEMtnIJslIyVBa5rNyqFyMxIhgyX2I884ybJUauVZyHOJVr5cZKBBAjzbK1Fd3OSZBZigoenTMIRsD2CJIfMSkY/k2MLEJ1TASKxWSB1gbAnY/LdtllOcVdLI5V4NqIkdnXsog1lekso1J9LZtGjCYDzc5R9uw3sK2miFI9kewZMdLrRogjeB2MiBWu4gmmWS8WE6vhbW9L6e1vz67HPJaO77OoaycubhVumdiOHjJiVNu7R4zEdjS0ygxOCqbrLS4sS7EmOwfuqcpr54wZU8EI2R5B2gwxEfMQ6FWDowDS4uV+KYisKYK0qFkVRSBWIUqlHohLpeCxVvzUGJXRsNO2E2Jxycw5atw0YqbkQcxM3Ihwx64gwQjZGUGaxEK1YMVCVCxzRbg3vtaHhcxKEhjeSjnBLLiNsmcyaSwjbmVNKwiRIBwsGPeiBWbaaBXCRuy4qGqPuhAfks6/8MLs+hHwaAUJRsrOC1KBMAnWSueLu0zW6HRBjCzsZQkSMZJJU5goZiTF72fUiBHLiBixjlhGs1wq90VUiFVX7IsYmY3knNpBosYoGDnDEaQCd03xILdkmnUxCZEgFqyXecoHulAzVcSopm0DZgyxhrhUxEhZQpfQuMcyoG6WtUN4irVFoH1viFEwcoYnSCwNmSQZNBMAuhZhcdnEZJYhSALMXDQxo9pWELEwldJvfnMWI1ZMX7CZ5UN0WT8swc3iRbBYRMaRKKScnKcdBCNmeH/lRMbiYxE4uvC9RnIUQepzq2bByiJARoc4ZMD6zkUoiVFxqaTitXHUWDG+R5ZMdTUXlXtnZhHh8bmvESOiVBNLC4KRsH2FkbW4HK0UxnmYs6T+Z9olWtTcHa6S7BzrpM+imoVh+Z//fA5eK3iscdNYQawXwWbCMjlutgb3REy1iQhwE1UBbhXYrCZW4mQRZRCsAcMUJFkoRZN79+berlmXSBTEmgiS7Ny8QV9lA34Oq0itEfevBpab5lgiyF1btFLa/XEPfYwWkGCNGZ7LZjEKZnNV+qwOlgzxUrw4T5bNwpeZY30RPbVGfWLkOkrtFFdKzIiFtKgYoZw7BvAHa87wLCQQF3Gcz342u1JdYkMQxG/e+tb6UgHukX40s4y4aTXFiX6OQLuaIIIkdjTCSukDBw66r0tm9+4TNj7LdP2Mzd87Se3rVn1+rPo5bfUeWmeYggQiYROBT32qW5AIkMFk73xnXec/y4gY2dZaVo11VfMIxHYUb3LR1BsRKDVLfYWNtZTrXtavw3mcU9aScLI4Bd0nx55M4eyzLtz4bHnccONBl3iCrp+x+XsnqX3dqs+PVT+nrd7DPFxzzbXpyCOPGJTADVeQxHfs3vGRj/Sn9LWevOc9eQLArMVG1ASPNbJy0cSOaht5wX1kgamU5l65Pq8f6ON7BIF/B0ElpEaVyPDNaD1Z9ULDqgVj1efHqp/TVu+hFmL0j//wT+kD//tfIUhVEKErr0zpgx/sFySWy+WX54zbLEGSzeKiydypvp63TMB5J4+hC9FmSkZSIJ7LOcO9XfVCw6oFY9Xnx6qf01bvoYYiRhiaIA232s5iEdyuSadzm6TQu1w7c5euvjrXG/n+eQXF9zs/IfN6H1s6WHRc1NIOsyxXM2iKSTEaIsMu/+UaqS2a1V5RsODEgiy6zRASh5lLdgWZx00bG54PQS67wgRrxdDFCMMWJBaSIOyMeMcjsFyk/y24zZaPf/u67xlhVmwuWJpEnjVZnkmwFrQgRmjDZWMhdbltLCTtH9MsJK9T32MhxgLMAs06KoWYc3DJpRfPPIJDTHs+5dgJWhEjDDeo7bK0dBior43EtkqzLlWbhRYOAVuZpGmNqM7x8Y/n/fGJ1zpCnJUrvOlNKb3lLVNbbXY6ILyMn7HO97CZPjGKoPY8sGykq2tcNsWOXXOLCNWePTkjx1rivo3lIMBdFuQkvl9NEmGqfU3QJC1ZRoXhu2xiSDVB7RJDmhWstQj1n6nq1gyrFsehF04FdkuHWigfTQbwsTTi9o0o8f+lQLLve4OmaVGMMFyXDTrvDda/6qru7ZLEmRT8cdtUbVucs3BObSk+iqNYmISvJTwHlg6hVnlud2CFnjbh7Moiek5aXy65JIvyFKFfZ3dnDPeAWjE64ojD0/984L/T4YcftvGVnWfYgqTh1UL75CdzPGnWpXLBWApveEPeLIBVVQNXr3VLwagWWynt25dLG7oEiTVo4wHTEVS1T7n3dV7MY7iHecToP/7z3w8a2Qet7AEx7NXIcuFi1LhsJZVNtGo1dgxuC0Fi8WmL6aotKpM4iZK4XMSPRkfrYoRhr0hCJN5RI0iC2n3V2mPEfctAFjGeBUEykdIecUQ+BGlUjEGMMGxBsmgsnr4Yj4XIvVtHQTIVoWZigfiRzQeIUl/WMmiKsYgRhu+zCN7WZJBYSS103y8L90mIBOe7yh0K4mo1GxAETTEmMcLwBYkQeXcXuO5D1qxmp5AxwBIUO7KhJeuwS5BYmDYR6Bg7ErTH2MQIbQhSxWCxhxFH4bb1YfFa0A6ft3bAfWqUlVnrumfPjJtm2yUD7EKQRsEYxQjDFyQLUBypJttmYSqQ7IojES2ujvlIMlQtHlxT128jSwHtruwaQRc7UkypVSRctuYZqxhh2HVIICB2kjVYTQFgV1Oo/cwM3zdmdpqLR7CM3uDmiLtYnK1lm0qg/8EH8xhes4263FT3qO7IPm8GtPUI0jrX8LRyDzW0KEYYviBZbPv3Z0HSIMsCmnXJqpAVR55xxmM3bVTFbJa2IWU+d17WQ2uCBDEhgkpc+wLaao4009omqmIThK6FoBFzFn/zrr/d+OyxzLOYl/Ezxn4PfbQqRhi+IHFPtI3YO80IWnU3s1wyFkBpHynbT7s9i9DrP/axXEA48FteGqyhXbtSuuiilM4887EiPYVFFsIs5lnMW2VZi3kWrdxDy2KE4ceQoOvf0WfREC9u2WT63+dctHvvzX1f6yJGUC4hs6ZCmwvbojUYVNO6GGH4gkSEZNhquv6JEQtK3KlYUQSJiyb46/N1wjNjNUr5R3Zt1IxBjNCGhVSybH0ZInGhUrGtULLAMlhH60D8SKuI3XbDOho1V1xxefNihDYsJK4HQepz2QiSIK+PxUJiVQnmWpzrQnlGLCRiJAgegjRq3ve+f3m4HKB1hi9IZSGp1uZ2dC2sUmNElIogWYzqcAR3FQYSN+cgbiyuFo8+cXFvAthqj4iSfwejR21S66I0/CxbQcr/Qx/KwWnCMwsVyQaQbU79C2zbsVbavxQT9llcQ8F1+jW5by6pOJnrn/WrE8C2m6+M4+mnHxLhCiLLNp2W7uFf/+2f0549r9v4V1u0I0i3357SRz+aSwC6BIkVdOmlB3+jZz96iH0Zc0uQiJqFzZ1rwXpg5bH4CKl2kXL9s351rEl1R2aIG8TWlwyYoGshbF6Uk8zzulX/jLHfQw2tilI7gnTnnSl94hNZmAjLLNQf2VHj3HNzunuzZSDgza2TcWvFleGqEtRSj2V6pvuYBcvwsstSOv/8HHubg7Ev5jHcg4zaAw8cfHPqoUVRaie4YFGKh/QFaOmrxTqreFI9kyC3+AprqoVDDEzqntC4t77yBd/nHt1rMDqk94lSHy3GlNoRJG6HWEjfImNJcOkm0/5jgBCVYWyzxBYsImI7p2UUtIP0/lhFqT0LiSh1uVosJIIk9T8mBLK1vfT1rhnCJrDPSmrFJQ3mZqyi1JYgWWQCtlLfs2A9sCa6YiytQYDuvz8303YF9AkQV+2449ar7mpNGaMotSVI3BAuW58gWbQEqZF4fSfugXWk/UXpQt/OIuJNtoQi3C2UNAQLMTZRakeQxJCKIPl81mIrFlIpkIRF3doxGSdyL2Wroy5XlDvLZfOcQozWhjGJUjuCJLsmhmSxdVlIgtmCv2p17r47T1X0ufqdlg6D11hEhMh93HNPtpSI1TRYRzr7ZeXmqDsKxsFYRKmdOiRYoFdfndJ11+WJidMunViJn3BbNJaKOxEpFkdLVoPrLe4pUT1wYPYsJ/fFMjIHSkGkNpku0e5g7DU8Y7+Hew6+cf3dFX/fbJ1SW4JkntHevXl6ZNfitBgtZlZVsRYaus1HEKR2P1xQldmz2kXcr4rsCy5I6bzzsjhtUXzXeTGP4R7Qsii147JBj5aDezILC9bC5d7Ys0ww2EHAWjtk1liFZbD/LFElSLr6bZPNImzJEgyWTsvuW1uCZKERpIiRPBqWIKtIq8wWXbVgXLQqSm0JEguBdRSL7tFI8Uv3z9HVH4yfFkWpTQuJRRBkiLPsWrSLBFNoTZTaEyQjRbgnXXGkdcLzOOqonFUUyA8LKdhES6LUliDJOhEjiy/iJRmCxDryPCK2FsygFVFqT5AsvOOPzzVGQS6E9CyiOjvooQVRas9lEz/Sza4I8MQTDw2xZy35/7Ef7pO7qon2tNPyiFqiFNZiUMHQRaktQSpoITnllLyPv1G1XDgLlDtX0t9jOsp9OWTTDv5RPbwTrVG9Zod7HkFQyZBFqa1K7Um0Vih+vO++Q3v1l4ZU1kI54Ba1j5RO+Z2+Za6neI/rY/W4Htfm+mcNlivX7LWC1wSYq7bkrv51rnIewz3MwxArutsVpMKk0BSXxlH612ARO+DrDre9nbfu2lyDjyX47Poc5brRd33lPoqgBcGIaF+QNlMWsr43Yzt8LubCrWFNFFHYCcqjJjhGo7g+lp0YmJaPvtEqQTByxiNILA0iNNm35t8gSNLjdiQRkykD87dz4XvMDz2U+9Jcm+t0fYbJKfZ0feJDXLESNwphCtaMcQiSRW1Ex/79ebskC198qYyx5dqwPowlccjOyVARqO2o3WERub7bbsuzjuyvZmaT6/Z/5fpYcUTp6KNTOvXUHKyPAtBgjWhbkFy6LnjDzG6+OaXrr88d8n23ZMHbs+zkk1e/6O0hxxq66aa8c6652H3X55pk0WQQjRUhVkGwBmxESBuFm2aSooFtFjtXqEZfWSvXXpsFjLWyKk1m/RCjG29M6YYb6sQSvs/17duX78l5gmANaFuQWB/2/Gd9WPi1C5dVZaytXXBtrc11WoUouT4u5K23ZhHs2+CxIGso1mRK5F13ZfduVaIZBAOiXUEiIiyJO+7Ig9i2smBZHxY8MSslAsuE9WX7a+ffyvW5L4LGzasVsyBomHYFidXAXduq9eA10u7OwVpapiA5N+uIkBC9rmmPXSgNcA6D/ksJQxCMmDYFiWvGpeEGEaStQijKzh7LjNNI17s+VeQ2I9iqdcN1K9dnpnYIUjBy2hQkC5UFIr3PdVsElscqYjQEhCi5zkWsL+UL6pXGsvFlEHTQrstmcVrojkUWajnPKijXtaiQeP2i5wiCBmhTkLhEigknx45sFYWRqxiJ67qce9FWFa+PvrVgTWhTkCxOA8m0Vxhsv1UIhcpt5ylNq8uiVF37uIiYlOpyxZGLCFsQNEC7gqT3q+xDthUsbj1kBuTrH1u2ILmu0pemEnwrYsJycw69d7GjSLAGtClIYHkYVLbVJlkxGSJkptCxxy7fJSIgJjkSpK3GgLhqrs+EzO1uBg6CHaBdQeLCGG5vciQrZ97FahQJITrppCway7aQwLLZvTsLyrz9cgTS6zQCu89osg3WgHYFCawQs7U1yurct2gJyyxx8nX/T8yOOSalc87JjbarECOwagiSn8PSqb2+Yhm5N6IZ7lqwJrQ/fkRBo0prjbLaNEqx5LRCxzJ3qIz3YB2J0SzbXZtEzZRqbdMIbrklt5NoCZlWLEmwbFrA4jOJgCCJk4V1FKwJ45iH5BZUM2uWtfD1pyko3HxrrChbKFnoLJftHIKm4to8JNeo/05R5+brI0YnnJDFyEeu2qqstyAYIOP4aycqrAoxF7u4miEkVV7EhgVEfLhBrCMf/f92ogRg1658jX6+6+GagegQI3OQuJKO7RTLIBgI47CQgiAYBeEPBEEwGEKQgiAYDCFIQRAMhhCkIAgGQwhSEASDIQQpCILBEIIUBMFgCEEKgmAwhCAFQTAYQpCCIBgMIUhBEAyGEKQgCAZDCFIQBIMhBCkIgsEQghQEwWAIQQqCYDCEIAVBMBhCkIIgGAgp/T9PYXNd9yX2lgAAAABJRU5ErkJggg==', 0.5, true)
//                         ->size(230)->errorCorrection('H')
//                         ->generate($qrcode));
// $output_file = '/qrCode/img-' . time() . '.png';
// \Storage::disk('public')->put($output_file, $file); 
			}        
			\DB::commit();
            $pdf = \PDF::loadView('qrCode.qrCodeFile',compact('qrnumber'));
            return $pdf->download("QR.pdf");
        }
        catch (\Exception $e) 
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;         
    }  
}
