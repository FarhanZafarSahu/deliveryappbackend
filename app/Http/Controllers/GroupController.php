<?php

namespace App\Http\Controllers;

use App\Constants\Message;
use App\Constants\ResponseCode;
use App\Customer;
use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = \Auth::user();
         $groups = Group::where('user_id',$user->id)->get();
		$result = [
			'groups' => $groups,
			];

		return view('group.index')->with(['result'=> $result]);
    }
 	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }
 	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_GROUP');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
            $customer =   Group::create([
                                "name"            => $request->name,
								"user_id"        => $user->id,
							]);
            \DB::commit();
            return \Redirect()->route('group.list')->with('success', 'Group Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }


    public function listGroup(Request $request)
    {


        $user = \Auth::user();     
        $response = defaultErrorResponse();
        try 
        {

            $groups = Group::where('user_id',$request->user_id)->orderBy('id','DESC')->get();
             $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$groups); 
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }

        return $response;    
    }
    public function groupCustomer($id)
    {
         $customer = Customer::where('group_id',$id)->orderby('serial_number','asc')->get();
         $result = [
			'customer' => $customer,
			
			];
		return view('group.customerlist')->with(['result'=> $result]);
    }
	public function ChangeMemberPosition(Request $request)
	{
	    $response = defaultErrorResponse();
        \DB::beginTransaction();
        try
        {
			$customer = Customer::where('group_id',$request->group_id)->orderby('serial_number','desc')->first();
			if($request->serial_number > 0  &&  $request->serial_number <= $customer->serial_number)
			{
				$customers = Customer::where('id','!=',$request->customer_id)->where('group_id',$request->group_id)->where('serial_number','>=',$request->serial_number)->orderby('serial_number','asc')->get();
				foreach($customers as $customer)
				{
					Customer::where('id',$customer->id)->update([
                               "serial_number" => $customer->serial_number + 1,
							]);
				}
				Customer::where('id',$request->customer_id)->update([
                               "serial_number" => $request->serial_number,
							]);
			}
			else
			{
				return makeResponse(ResponseCode::FAIL,"Invalid Number",false,null); 
			}
 			\DB::commit();
             return makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null); 
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
             return makeResponse(ResponseCode::FAIL,$e->getMessage(),false,null); 

        }
        return $response;
	}
}
