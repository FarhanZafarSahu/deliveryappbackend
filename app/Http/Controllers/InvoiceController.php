<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
//Models 
use App\Order;
use App\User;
//Carbon Helper
use Carbon\Carbon ;
class InvoiceController extends Controller
{
    public function index()
    {
		$orders = Order::where('is_accept',1)->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get();
		$result = [
		'orders' => $orders,
		];
		return view('invoice.invoice')->with(['result' =>  $result]);
    }

    public function view()
    {
        return view('invoice.view');
    }
}
