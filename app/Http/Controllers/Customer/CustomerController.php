<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    public function show_login()
    {
        if (Auth::guard('customer')->check()) {
            return redirect()->intended('homefeed');
        }
        return view('frontend.customer.login');
    }


    public function login(Request $request)
    {
        $credentials = [
            'phone'       => $request->phone,
            'password'  => $request->password,
        ];
        if (Auth::guard('customer')->attempt($credentials)) {
            return redirect()->route('homefeed');
        } else {
            return back()->with('error', 'Invalid Credentials.');
        }
    }

    public function show_register()
    {
        if (Auth::guard('customer')->check()) {
            return redirect()->intended('homefeed');
        }
        return view('frontend.customer.register');
    }

    public function register(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:customers',
        ]);
        if ($valid->fails()) {
            return back()->with('error', 'Phone Number Already Exist');
        }

        \Session::put('regdata', $request->all());

        $otp = rand(1000, 9999);
        $sendOtp = sendSMS($otp, $request->phone);
        if (property_exists($sendOtp, 'return')) {
            if ($sendOtp->return) {
                session()->put('otp', $otp);
                return redirect()->route('customer.show.otppage')->with('success', 'OTP Send Successfully');
            } else {
                return back()->with('error', $sendOtp->message);
            }
        } else {
            return back()->with('error', 'Otp can not be sent, Please try Again');
        }

        //  $validator = validateData($request,'REGISTER_CUSTOMER_NEW');
        //  if ($validator['status'])
        //      return back()->with('error',$validator['errors']->first());
        //  \DB::beginTransaction();
        //  try
        //  {
        //      $password       = $request->password;
        //      $name     		   = $request->name;
        //      $image            = "frontend/images/logo.png";
        //     $cust =  Customer::create([
        //          "password"     => bcrypt($password),
        //          "name"        	  => $name,
        //          "image"       	  => $image,
        //          "phone"	  	  => $request->phone ?? NULL,
        //          "qr"       		=> str_pad(rand(1,999999999999), 7, '0', STR_PAD_LEFT),
        //      ]);
        //     // SendEmailJob::dispatch($name);
        //      \DB::commit();
        //      Auth::guard('customer')->login($cust);
        // return redirect()->route('customer.show.otppage');
        //     }
        //  catch (\Exception $e)
        //  {
        //       \DB::rollBack();
        //       return back('error','Something went wrong');
        //  }
    }

    public function show_otp()
    {
        return view('frontend.customer.verifyOtp');
    }
    public function verifyOtp(Request $request)
    {
        $otp = $request->otp;
        $existOtp = session()->get('otp');
        // $existOtp = '12345';
        //  try{
        if ($otp == $existOtp) {
            $data = Session::get('regdata');
            $customer = new Customer();
            $customer->name = $data['name'];
            $customer->phone = $data['phone'];
            $customer->address = $data['address'];
            $customer->password = Hash::make($data['password']);
            $customer->email_verified_at = now();
            $customer->save();
            Auth::guard('customer')->login($customer);
            Session::forget('regdata');
            return redirect()->route('homefeed');
        } else {
            dd("didnt matched");
        }


        //  catch(\Exception $e){
        //      return back()->with('error','Something went wrong.');
        //  }
    }

    public function showQr()
    {
        $customer = Auth::guard('customer')->user();
        return view('frontend.customer.qr', compact('customer'));
    }
}
