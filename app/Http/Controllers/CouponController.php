<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
// Constants
use App\Constants\ResponseCode;
use App\Constants\Message;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupon = Coupon::all();
		$result = [
			'coupon' => $coupon,
			];

		return view('coupon.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_COUPON', ['min'  => 'The :attribute must be greater then :min.']);
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        {
            $coupon =   Coupon::create([
                                "amount"            => ($request->is_percentage == 0 ) ? $request->couponAmount : $request->amountPercentage,
                                "coupon_code"      => $request->couponCode,
								"user_id"   => $user->id,
								"is_percentage" => $request->is_percentage,
								"city_id"   => $request->cityId,
								"coupon_expiry" => $request->expiryDate,
							]);
            \DB::commit();
        	$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$coupon);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $coupon = Coupon::where('id',$id)->first();
        return response()->json($coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'UPDATE_COUPON');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
       \DB::beginTransaction();
        try
        {
            $id 		  = $request->id;
			$couponAmount = $request->couponAmount;
			$couponCode   = $request->couponCode;
            $coupon   	  = Coupon::where('id',$id)->first();
            if(empty($coupon))
                return $response = makeResponse(ResponseCode::FAIL, "Coupon not found", false, null);
            Coupon::where('id',$id)
                    ->update([
                        "coupon_code"   => $couponCode,
                        "amount"            => ($request->is_percentage == 0 ) ? $request->couponAmount : $request->amountpercentageedit,
                        "user_id"       => $user->id,
						"is_percentage" => $request->is_percentage,
						"city_id"   => $request->cityId,
					"coupon_expiry" => $request->expiryDate,

                    ]);

            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS, "Coupon updated successfully.", true , null);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        //
    }

 	/**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request , $id)
    {
        $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
            $coupon = Coupon::where('id',$id)->first();
            if(empty($coupon))
               return \Redirect::back()->with('error', 'coupon not found.');
            $coupon->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'coupon deleted');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

}
