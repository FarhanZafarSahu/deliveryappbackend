<?php

namespace App\Http\Controllers\Delivery;

use App\Customer;
use App\Group;
use App\Http\Controllers\Controller;
use App\User;
use App\UserSubscriptionPlan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DeliveryBoyController extends Controller
{


    public function home()
    {
        if (auth('deliveryboy')->check()) {
            return view('delivery-boy.delivery-boy');
        } else {
            return redirect()->intended(route('delivery-boy.login'));
        }
    }
    public function show_login()
    {
        if (auth('deliveryboy')->check()) {
            return redirect()->intended(route('delivery-boy.home'));
        }
        return view('delivery-boy.auth.login');
    }

    public function login(Request $request)
    {
        $credentials = [
            'phone_number'       => $request->phone,
            'password'  => $request->password,
        ];
        if (Auth::attempt($credentials)) {

            return redirect()->route('delivery-boy.home');
        } else {
            return back()->with('error', 'Invalid Credentials.');
        }
    }

    public function show_register()
    {
        if (Auth::check()) {
            return redirect()->intended(route('delivery-boy.home'));
        }
        return view('delivery-boy.auth.register');
    }

    public function register(Request $request)
    {
        $valid = Validator::make($request->all(),[
            'name' => 'required',
            'phone' => 'required | unique:users',
        ]);
        if ($valid->fails()) {
            return back()->with('error', 'Phone Number is required and must be unique');
        }

        \Session::put('regdata', $request->all());
        $otp = rand(1000, 9999);
        $sendOtp = sendSMS($otp, $request->phone);
        if (property_exists($sendOtp, 'return')) {
            if ($sendOtp->return) {
                session()->put('otp', $otp);
                return redirect()->route('delivery-boy.show.otppage')->with('success', 'OTP Send Successfully');
            } else {
                return back()->with('error', $sendOtp->message);
            }
        } else {
            return back()->with('error', 'Otp can not be sent, Please try Again');
        }
        // return redirect()->route('delivery-boy.show.otppage');

    }

    public function show_otp()
    {
        return view('delivery-boy.auth.verifyOtp');
    }
    public function verifyOtp(Request $request)
    {
        $otp = $request->otp;
        $existOtp = session()->get('otp');
        //  try{
        if ($otp == $existOtp) {
            $data = Session::get('regdata');
            $boy = new User();
            $boy->name = $data['name'];
            $boy->phone_number = $data['phone'];
            $boy->password = Hash::make($data['password']);
            $boy->email_verified_at = now();
            $boy->role_id = 5;
            $boy->save();
            Auth::login($boy);
            Session::forget('regdata');
            return redirect()->route('delivery-boy.home');
        } else {
            return back()->with('error', 'Invalid OTP');
        }


        //  catch(\Exception $e){
        //      return back()->with('error','Something went wrong.');
        //  }
    }

    public function todayDelivery(Request $request)
    {
        $user = Auth::guard('deliveryboy')->user();
        $group = Group::where('id', $user->group_id)->with('customerUser')->first();

        $userIds = Customer::where('group_id', $user->group_id)->pluck('id');
        // dd($userIds);
        $customers = UserSubscriptionPlan::whereIn('user_id', $userIds)->where('next_delivery_date', \Carbon\Carbon::now()->format('y-m-d'));
        // ->with('customer')->get();
        if ($request->has('search')) {
            //input search in customers table
            $customers = $customers->whereHas('customer', function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%');
            });
        }

        $customers = $customers->with('customer')->get();

        return view('delivery-boy.today-delivery')->with(['group' => $group, 'customers' => $customers, 'search' => $request->search]);
    }

    public function payments(Request $request)
    {
        $user = Auth::guard('deliveryboy')->user();
        $group = Group::where('id', $user->group_id);

        if ($request->has('search')) {
            $group = $group->whereHas('customer', function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('phone_number', 'like', '%' . $request->search . '%');
            });
        }

        $group = $group->with('customerUser')->first();

        return view('delivery-boy.payment')->with(['group' => $group, 'search' => $request->search]);
    }

    public function logout(Request $request)
    {
        if (auth('deliveryboy')->check()) {
            Auth::guard('deliveryboy')->logout();
            return redirect()->intended(route('delivery-boy.login'));
        } else {
            return back()->with('error', 'You are not logged in.');
        }
    }

    public function newJarEntry()
    {
        return view('delivery-boy.delivery-jar');
    }

    public function newBilling()
    {
        return view('delivery-boy.billing-new');
    }
}
