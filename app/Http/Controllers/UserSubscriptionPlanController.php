<?php

namespace App\Http\Controllers;

use App\UserSubscriptionPlan;
use Illuminate\Http\Request;

class UserSubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function show(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserSubscriptionPlan  $userSubscriptionPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserSubscriptionPlan $userSubscriptionPlan)
    {
        //
    }
}
