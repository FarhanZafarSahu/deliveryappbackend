<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderDeliveryController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
		
		if($user->role_id == 3)
		{
		$customersIds = Customer::where('vendor_id',$user->id)->pluck('id');
        $orders = Order::whereIn('customer_id',$customersIds)->whereDate('order_time', Carbon::today()->format('Y-m-d'))->get();
		}
		else if($user->role_id == 1)
		{
	        $orders = Order::whereDate('order_time', Carbon::today()->format('Y-m-d'))->get();
		}
		$result = [
			'orders' => $orders,
			];

		return view('orders.index')->with(['result'=> $result]);
    }
}
