<?php

namespace App\Http\Controllers;

use App\SiteSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Sabberworm\CSS\Settings;
use Illuminate\Auth\EloquentUserProvider;
class SiteSettingController extends Controller
{
    public function aboutus()
    {
        $setting = SiteSetting::first();
        return view('aboutus.index',compact('setting'));
    }
    public function editAbout($id)
    {
        $setting = SiteSetting::find($id);
        return view('aboutus.edit',compact('setting'));
    }
    public function contactus()
    {
        $settings = SiteSetting::first();
        return view('contactus.index',compact('settings'));
    }
    public function editContact($id)
    {
        $setting = SiteSetting::find($id);
        return view('contactus.edit',compact('setting'));
    }
    public function termAndCondition()
    {
        $setting = SiteSetting::first();
        return view('terms.index',compact('setting'));
    }
    public function editTermAndCondition($id)
    {
        $setting = SiteSetting::find($id);
        return view('terms.edit',compact('setting'));
    }
    public function saveSiteSetting(Request $request)
    {
  		$user =  \Auth::user();
        \DB::beginTransaction();
        try
        {
   			$setting = SiteSetting::find($request->id);
			$about_img = $setting->about_img;
    		if ($request->hasFile('about_img'))
			{
				$propertyThumbnailDirectory = 'public/aboutus';
				if (!\Storage::exists($propertyThumbnailDirectory))
				{
					\Storage::makeDirectory($propertyThumbnailDirectory);
				}
				$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('about_img'));
				$image = $thumbnailUrl;
			    $about_img = str_replace(array("public/"), "", $image);
			}

        $about_description = isset($request->about_description) ? $request->about_description : $setting->about_description;
        $contact_mobile     = isset($request->contact_number) ? $request->contact_number : $setting->contact_mobile;
        $contact_address   = isset($request->contact_address) ? $request->contact_address : $setting->contact_address;
        $t_description        = isset($request->t_description) ? $request->t_description : $setting->t_description;
 		SiteSetting::where('id',$request->id)
                    ->update([
                      		 	 "about_description"   => $about_description,
                                "about_img"      	       => $about_img,
								"contact_mobile"  	    => $contact_mobile,
								"contact_address"	   => $contact_address,
								"t_description"           => $t_description,
					]);
            \DB::commit();
            return \Redirect()->back()->with('success', 'Site Setting Updated.');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
               return \Redirect::back()->with('error', $e->getMessage());
        }

    }
    public function lock()
    {
        $setting = SiteSetting::first();
        return view('lock_delivery.index',compact('setting'));
    }
    public function edit()
    {
        $setting = SiteSetting::first();
        return view('lock_delivery.edit',compact('setting'));
    }
 public function locked(Request $request ,  $id )
    {
        $lock_time = Carbon::parse($request->lock_time)->format('H:i A');
		// $start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s');
		// $end_date = Carbon::parse($request->end_date)->format('Y-m-d H:i:s');

        $setting = SiteSetting::first();
		if($id == 1 )
		{
			$setting->lock_time = $lock_time;
			// $setting->end_date = $end_date;
			$setting->save();
		}
		else
		{
			$setting->lock_time = null;
			$setting->save();
		}
            return \Redirect()->route('delivery.lock')->with('success', 'Site Setting Updated.');
	}
}
