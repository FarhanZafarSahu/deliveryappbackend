<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
         $categories = Category::where('is_active', 1)->get();
		$result = [
			'categories' => $categories,
			];

		return view('category.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_CATEGORY');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
			if ($request->hasFile('image'))
				{
					$propertyThumbnailDirectory = 'public/category';
					if (!\Storage::exists($propertyThumbnailDirectory))
					{
						\Storage::makeDirectory($propertyThumbnailDirectory);
					}
					$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
					$image = $thumbnailUrl;
			    	$image = str_replace(array("public/"), "", $image);
				}
            $category =   Category::create([
                                "name"         => $request->name,
								"image"        => $image,
							]);
            \DB::commit();
            return \Redirect()->route('category.list')->with('success', 'Category Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
