<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user = \Auth::user();
         $products = Product::all();
		$result = [
			'products' => $products,
			];

		return view('product.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
 		$categories = Category::all();
		$result = [
			'categories' => $categories,
			];
         return view('product.create')->with(['result'=> $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_PRODUCT');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
			if ($request->hasFile('image'))
				{
					$propertyThumbnailDirectory = 'public/product';
					if (!\Storage::exists($propertyThumbnailDirectory))
					{
						\Storage::makeDirectory($propertyThumbnailDirectory);
					}
					$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
					$image = $thumbnailUrl;
			    	$image = str_replace(array("public/"), "", $image);
				}
            Product::create([
                                "product_title"       => $request->product_title,
                                "description"         => $request->description,
                                "price"         	  => $request->price,
                                "unit"         		  => $request->unit,
                                "category_id"         => $request->category_id,
								"image"        		  => $image,
								"is_active"			  => 1,
								"product_type"		  => $request->product_type,
							]);
            \DB::commit();
            return \Redirect()->route('product.list')->with('success', 'Product Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
