<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    public function list()
    {
        $feedback = Feedback::all();
        return view('feedback.list', compact('feedback'));
    }
    public function add()
    {
        $user = Auth::user();
        return view('feedback.add',compact('user'));
    }
    public function save(Request $request)
    {

        $complain =   Feedback::create([
            "user_id"        => Auth::id(),
            "name"           => $request->name,
            "phone_number"   => $request->phone_number,
            "feedback"        =>  $request->feedback
        ]);

        return redirect()->route('feedback.list');
    }
    public function reply(Request $request, $id)
    {
        $feedback = Feedback::find($id);
        $replyto = User::where('id',$feedback->user_id)->get();
        return view('feedback.reply', compact('feedback','replyto'));
    }
    public function response(Request $request, $id)
    {
        $feedback = Feedback::find($id);
        $feedback->feedback_reply = $request->feedback_reply;
        $feedback->save();
        return redirect()->route('feedback.list');
    }



}
