<?php

namespace App\Http\Controllers;

use App\Route;
use App\User;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
		$deliveryBoys = User::where('role_id',5)->get();
		$result = [
			'deliveryBoys' => $deliveryBoys,
			];

		return view('route.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
		$deliveryBoys = User::where('id',$id)->where('role_id',5)->with('location')->first();
		$result = [
			'deliveryBoys' => $deliveryBoys,
			];
          return view('route.create')->with(['result'=> $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_ROUTE');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
			$found = false;
			$routes = Route::where('user_id',$request->user_id)->orderby('route_sequence','asc')->get();
 			$route = Route::where('user_id',$request->user_id)->orderby('route_sequence','desc')->first();
			$sequence = empty($route->route_sequence) ? 0 : $route->route_sequence ;  
			$route_sequence  = ($request->route_sequence == 0 ) ? ($sequence + 1) : $request->route_sequence ;
			foreach($routes as $route)
			{
				if($route->route_sequence == $request->route_sequence)
				{		
						$found = true ;
						$route_sequence = $route->route_sequence;
				}
				if($found == true)
				{
						Route::where('id',$route->id)->update([
                               "route_sequence" => $route->route_sequence + 1,
							]);
				}
			}
			Route::create([
                                "route_name"     => $request->route_name,
								"user_id"        => $request->user_id,
								"route_sequence" => $route_sequence,
							]);
            \DB::commit();
            return \Redirect()->route('route.list')->with('success', 'Route Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deliveryBoys = User::where('id',$id)->where('role_id',5)->with('locationAsc')->first();
		$result = [
			'deliveryBoys' => $deliveryBoys,
			];
          return view('route.view')->with(['result'=> $result]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function edit(Route $route)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Route $route)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
			$route = Route::where('user_id',$id)->first();
            if(empty($route))
               return \Redirect::back()->with('error', 'Route for this delivery boy is not found.');
            Route::where('user_id',$id)->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'Route  deleted for this customer.');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
 /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Route  $route
     * @return \Illuminate\Http\Response
     */
    public function assignDeliveryRoute($id)
    {
         $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
			$deliveryBoys = User::where('role_id',5)->where('id','!=',$id)->get();
			$selectedDeliveryBoy = User::where('id',$id)->first();
			$result = [
				'deliveryBoys' => $deliveryBoys,
				'selectedDeliveryBoy' => $selectedDeliveryBoy
				];
          	return view('route.assignDeliveryRoute')->with(['result'=> $result]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function storeAssignDeliveryRoute(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ASSIGN_DELIVERY_ROUTE');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
			$routes = Route::where('user_id',$request->assign_d_id)->orderby('route_sequence','asc')->get();
		   	Route::where('user_id',$request->user_id)->delete();	
			foreach($routes as $route)
			{
				Route::create([
                                "route_name"     => $route->route_name,
								"user_id"        => $request->user_id,
								"route_sequence" => $route->route_sequence,
							]);
            	
			}
			\DB::commit();
            return \Redirect()->route('route.list')->with('success', 'Route Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }
}
