<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AssignVendorController extends Controller
{
    public function list($filter = 1)
    {
		if($filter == 1){
        $customers = Customer::where('vendor_id',null)->get();
		}
		else
		{
        	$customers = Customer::whereNotNull('vendor_id')->with(['assignedBy' => function ($query)
			{
			$query->whereIn('role_id',[1,2]);
			}])
			->get();
		}
        return view('assign-endors.index', get_defined_vars());
    }

    public function view($id)
    {
        $customer = Customer::find($id);
        $vendors = User::where('role_id',3)->get();
        return view('assign-endors.view',get_defined_vars());
    }

    public function assignVendor(Request $request, $id)
    {
            $customer = Customer::find($id);
            $customer->vendor_id = $request->vendor_id;
            $customer->assigned_by= Auth::user()->role_id;
            $customer->save();
            return redirect()->route('newcustomer.list');
    }
}
