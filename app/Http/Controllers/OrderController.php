<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
     public function index(Request $request ,$filter = 1)
    {

        try 
        {
            if($filter == 1)
            {
                $orders     = Order::orderBy('created_at','desc')->where('is_accept' , 0 )->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get();
            }
            else if($filter == 2)
            {
                $orders = Order::orderBy('created_at','desc')->where('is_accept' , 1 )->withCount([
								'orderDetails as quantity' => function ($query) {
            							return $query->select(\DB::raw("SUM(quantity)"));
								}])->get();
            }
            $result = [
                'orders' => $orders,
                'filter' => $filter,
            ];
            return view('orders.neworder')->with(['result'=>$result]);  
        }
        catch (\Exception $e) 
        {
            return \Redirect::back()->with('error',$e->getMessage());
        }
    }
 public function accept(Request $request ,  $id)
	{
    	 \DB::beginTransaction();
        try
        {

			 $order = Order::where('id',$id)->first();
            if(empty($order))
               return \Redirect::back()->with('error', 'order not found.');
            $order->is_accept = 1 ;
			$order->save();
            \DB::commit();
			$orders     = Order::orderBy('created_at','desc')->where('is_accept' , 0 )->get();
 			$result = [
                'orders' => $orders,
                'filter' => 1,
            ];
            return \Redirect::back()->with(['success' => 'Order Accepted.' , 'result' => $result]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

 public function reject(Request $request ,  $id)
	{
    	 \DB::beginTransaction();
        try
        {
            $order = Order::where('id',$id)->first();
            if(empty($order))
               return \Redirect::back()->with('error', 'order not found.');
            $order->is_accept = 2 ;
			$order->save();
            \DB::commit();
 			$orders     = Order::orderBy('created_at','desc')->where('is_accept' , 0 )->get();
 			$result = [
                'orders' => $orders,
                'filter' => 1,
            ];
            return \Redirect::back()->with(['success' => 'Order Rejected.' , 'result' => $result]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
}
