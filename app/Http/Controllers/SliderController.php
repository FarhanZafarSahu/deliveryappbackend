<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function list()
    {
        $sliders = Slider::orderBy('created_at','DESC')->get();
         return view('slider.index',compact('sliders'));
    }

    public function create()
    {
        return view('slider.create');
    }

    public function save(Request $request)
    {
        try{
            $request->validate([
                'image' => 'required',
                'name' => 'required',
              ]);
        }catch(\Exception $e){
            return back()->with('error',$e->getMessage());
        }

        if ($request->hasFile('image'))
				{
					$propertyThumbnailDirectory = 'public/product';
					if (!\Storage::exists($propertyThumbnailDirectory))
					{
						\Storage::makeDirectory($propertyThumbnailDirectory);
					}
					$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $request->file('image'));
					$image = $thumbnailUrl;
			    	$image = str_replace(array("public/"), "", $image);
				}

                $slider = new Slider();
                $slider->image = $image;
                $slider->name = $request->name;
                $slider->save();
                return redirect()->route('slider.list');

    }
}
