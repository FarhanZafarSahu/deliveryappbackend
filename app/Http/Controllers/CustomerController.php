<?php

namespace App\Http\Controllers;

use App\Complain;
use App\Constants\Message;
use App\Constants\ResponseCode;
use App\Customer;
use App\Feedback;
use App\Group;
use App\Order;
use App\OrderDetail;
use App\Plan;
use App\Product;
use App\User;
use App\UserSubscriptionPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = \Auth::user();
         $customer = Customer::where('vendor_id',$user->id)->get();
        // $customer = Customer::orderBy('created_at','DESC')->get();
		$result = [
			'customer' => $customer,
			'user_id'	=> $user->id,
			];
		return view('customer.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = User::where('role_id',3)->get();
		$groups = Group::where('user_id',Auth::user()->id)->get();
        return view('customer.create',compact('vendors'))->with(['plans'=>Plan::all(),'products'=>Product::all(),'groups' => $groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_CUSTOMER');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
 			$qr         			= str_pad(rand(1,999999999999), 7, '0', STR_PAD_LEFT);
			$plan_id    			= $request->plan_id; 	
			$quantity 			    = $request->qty;
			$emptyJarQty		    = 0;
			$perUnitEmptyJarPrice	= 0 ;
			$productId 				= $request->productId;
			$product 				= Product::where('id',$productId)->first();
			$no_of_items 			= 1 ;
			$deliveryCharges 		= ($quantity*config('constant.DELIVERY_CHARGES'));
			$totalOrderAmount 		= $product->price * $quantity + $deliveryCharges; 
			$discountAmount 		= $product->price * $quantity + $deliveryCharges ;
			$orderTime 				= \carbon\Carbon::now();
			$buyerContactNo 		= $request->phone;
			$deliveryName 			= $request->name;
			$deliveryAddress 		= $request->address;
			$isAdvancePayment		= 0;
			$productType			= $product->product_type;

			$oldcustomer = Customer::where('vendor_id',$request->vendor_id)->latest()->first();
			if($oldcustomer != NULL)
			{
				$serial_number =  $oldcustomer->serial_number + 1 ;
			}
			else
			{
				$serial_number = 1 ;
			}
            $customer   = Customer::create([
                                "name"           => $request->name,
                                "email"          => $request->email,
								"address"        => $request->address,
								"land_mark"    	 => $request->land_mark,
								"phone"          => $request->phone,
								"qr"             =>$qr,
                                "vendor_id"    	 => $request->vendor_id,
								'group_id'		=> $request->group_id,
								'serial_number'	=> $serial_number ,
							]);
			$orderNo    		= 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			// calculate next date of delivery.
			$next_delivery_date = Order::CalculateNextDeliveryDate($plan_id);	
			$subscription 		= UserSubscriptionPlan::store($customer->id, $plan_id, $quantity, $emptyJarQty, $perUnitEmptyJarPrice, $productId, $next_delivery_date, $isAdvancePayment, $productType);
			$order 				= Order::store($no_of_items, $totalOrderAmount, $discountAmount , $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer->id ,  $orderTime, $quantity, $subscription->id);

			OrderDetail::store($order->id, $productId, $quantity, $product->price);

            \DB::commit();
            return \Redirect::back()->with('success', 'Customer Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
         $customer = Customer::find($id);
        return view('customer.view')->with(['customer' => $customer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,  $id)
    {
         $user =  \Auth::user();
        try
        {
            $customer = Customer::where('id',$id)->first();
            if(empty($customer))
               return \Redirect::back()->with('error', 'Customer not found.');
            return view('customer.edit')->with(['customer'=> $customer]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'UPDATE_CUSTOMER');
        if ($validator['status'])
            return \Redirect::back()->with('error', $validator['errors']);
       \DB::beginTransaction();
        try
        {
            $id  = $request->id;
			$name = $request->name;
			$email   = $request->email;
			$address   = $request->address;
			$phone   = $request->phone;
			$land_mark   = $request->land_mark;

            $customer   	  = Customer::where('id',$id)->first();
            if(empty($customer))
                return $response = makeResponse(ResponseCode::FAIL, "Customer not found", false, null);
            Customer::where('id',$id)
                    ->update([
                      		 	 "name"            => $name,
                                "email"      => $email,
								"address"   => $address,
								"land_mark"   => $land_mark,
								"phone"   => $phone,
					]);

            \DB::commit();
            return \Redirect::back()->with('success', "Customer Updated");
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request ,  $id)
    {
        $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
            $customer = Customer::where('id',$id)->first();
            if(empty($customer))
               return \Redirect::back()->with('error', 'Customer not found.');
            $customer->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'Customer deleted');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function feedback()
    {
        return view('frontend.feed-complain.feedback');
    }

    public function complain()
    {
        
        return view('frontend.feed-complain.complain');      
    }

    public function saveComplain(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required',  
            'message' => 'required',
        ]);
        // dd($request->all());
        $complain =   Complain::create([
            "customer_id"      => Auth::id(),
            "name"           => $request->name,
            "phone_number"   => $request->phone_number,
            "message"        =>  $request->message
        ]);

        return redirect()->route('homefeed');
    }

    public function saveFeedback(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone_number' => 'required',  
            'feedback' => 'required',
        ]);
        $complain =   Feedback::create([
            "user_id"        => Auth::id(),
            "name"           => $request->name,
            "phone_number"   => $request->phone_number,
            "feedback"        =>  $request->feedback
        ]);

        return back();
    }

  public function postassignGroup(Request $request)
    {
        $user = \Auth::user();
        $response = defaultErrorResponse();
        \DB::beginTransaction();
        try
        {
            $customer_id    = $request->customer_id;
			$groupId = $request->group_id;
            $customer   	  = Customer::where('id',$customer_id)->first();
            if(empty($customer))
                return $response = makeResponse(ResponseCode::FAIL, "Customer not found", false, null);
			$lastGroupMember = Customer::where('id',$customer_id)->where('group_id',$groupId)->latest()->first();
			$srNumber = isset($lastGroupMember) ? $lastGroupMember->serial_number + 1 : 1 ;
            Customer::where('id',$customer_id)
                    ->update([
                      		 	 "group_id"            => $groupId,
								 "serial_number"       => $srNumber,
					]);

            \DB::commit();
             return makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null); 

        }
        catch (\Exception $e)
        {
            \DB::rollBack();
             return makeResponse(ResponseCode::FAIL,$e->getMessage(),false,null); 

        }
        return $response;
    }
}
