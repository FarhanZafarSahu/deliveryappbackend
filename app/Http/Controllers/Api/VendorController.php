<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// Constants
use App\Constants\ResponseCode;
use App\Constants\Message;
use App\Customer;
use App\User;
use App\Jobs\SendEmailJob;
use App\VendorBankDetail;
use Tymon\JWTAuth\Facades\JWTAuth;

class VendorController extends Controller
{
     public function store(Request $request)
    {
        $response = defaultErrorResponse();
		$request->contactNo = substr($request->contactNo , -10);
        $validator = validateData($request,'REGISTER');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        { 
            $password     = $request->password;
            $name     = $request->name;
            $roleId       = 3;
        	$request->phone_number = substr($request->phone_number,-10);
            $sub_total    = "public/propertyThumbnail/userImage.jpeg";  
            $user =  User::create([
                "password"        => bcrypt($password),
                "name"        	  => $name,
                "roleId"          => $roleId,
                "photo"       	  => $sub_total,
           		'phone_number'		  => $request->phone_number ?? NULL,
			]);
           // SendEmailJob::dispatch($name);
            \DB::commit();
 			$credentials = [
					'phone_number' => $request->phone_number,
					'password' => $request->password,	
					];
			return attemptJWTLogin($request->only('phone_number', 'password'), 0 ,  $request->FCMToken, $request->macAddress);
        }
        catch (\Exception $e) 
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
  public function customerStore(Request $request)
    {
		$user = JWTAuth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'VENDOR_CUSTOMER');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        { 
            $name     = $request->customerName;
			$isJar = $request->isJar;
			$paymentType = $request->paymentModeId;
			$phoneNumber = substr($request->phoneNumber , -10 );
            $sub_total    = "public/propertyThumbnail/userImage.jpeg";  
			$qr      = str_pad(rand(1,999999999999), 7, '0', STR_PAD_LEFT);
            $customer = Customer::create([
                "name"        	  => $name,
                "image"       	  => $sub_total,
				"isJar"           => $isJar,
           		'phone'  		  => $phoneNumber ?? NULL,
				"paymentType"     => $paymentType,
				"vendorId"		  => $user->id,
				"qr" 			  => $qr,
			]);
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,$customer);
        }
        catch (\Exception $e) 
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
public function editProfile(Request $request)
    {
		$user = JWTAuth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'VENDOR_EDIT_PROFILE');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        { 
        	$name = $request->name;
			$phoneNumber = substr($request->phoneNumber , -10) ;
			$brandName   = $request->brandName ;	
			$businessName = $request->businessName ;
			$businessAddress = $request->businessAddress ;
			$pinCode = $request->pinCode;
			$alternativePhoneNumber = substr($request->alternativePhoneNumber , -10);
			$email = $request->email;	
            $customer = User::where('id',$user->id)->update([
                "name"        	  => $name,
				"email"   => $email,
				"phone_number"   => $phoneNumber,
				"brand_name"   => $brandName,
				"business_name"   => $businessName,
				"business_address"   => $businessAddress,
				"pin_code"   => $pinCode,
				"alternative_phone_number"   => $alternativePhoneNumber,
              ]);
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$customer);
        }
        catch (\Exception $e) 
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
public function editBankDetails(Request $request)
    {
		$user = JWTAuth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'VENDOR_BANK_DETAILS');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        { 
		  $bankName = $request->bankName;
		  $accountHolder = $request->accountHolder;
		  $accountNumber = $request->accountNumber;
		  $IFSCCode = $request->IFSCCode;
		  $accountType = $request->accountType;
		  $branchName	= $request->branchName;
            $customer = VendorBankDetail::updateOrCreate(['user_id'=>$user->id],
				[
                "bank_name"        	  => $bankName,
				"account_holder"      => $accountHolder,
				"account_number"      => $accountNumber,
				"IFSC_code"           => $IFSCCode,
				"account_type"        => $accountType,
				"branch_name"         => $branchName,
				"user_id"             => $user->id,
              ]);
            \DB::commit();
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,$customer);
        }
        catch (\Exception $e) 
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
public function listCustomers(Request $request)
    {
		$user = JWTAuth::user();        
		$response = defaultErrorResponse();
		try 
        {
            $customers  = Customer::where('vendorId',$user->id)->with('vendor')
                             ->orderBy('created_at','desc')
                             ->get();
            if($customers == NULL)
                return $response = makeResponse(ResponseCode::FAIL,"NO Customer found.",false); 

            $response = makeResponse(ResponseCode::SUCCESS,"List Customers.",true,null,$customers); 
        }
        catch (\Exception $e) 
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
}
