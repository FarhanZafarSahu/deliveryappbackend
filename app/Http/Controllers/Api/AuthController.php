<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Constants\ResponseCode;
class AuthController extends Controller
{
     public function login(Request $request)
    {
		$request->phone_number = substr($request->phone_number , -10);
		if(!isset($request['phone_number'],$request['password']))
            return makeResponse(ResponseCode::FAIL,'Invalid '.isset($credentials) ? "Contact # " : "email" .' or Password',false);
	    $credentials = [
					'phone_number' => $request->phone_number,
					'password' => $request->password,	
					];
		return attemptJWTLogin($credentials, 0, $request->FCMToken, $request->macAddress);
    }
  public function logout(Request $request)
    {
        $user =  \JWTAuth::user();
        $response = defaultErrorResponse();
        isset($request->FCMToken)? $FCMToken = $request->FCMToken : $FCMToken = null;
        \DB::beginTransaction();
        try
		{
          	auth()->logout();
        	$response = makeResponse(ResponseCode::SUCCESS,"Logout Successfully.",true);
        }
        catch (\Exception $e) 
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }

}
