<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    public function gitpull()
	{
		return shell_exec('git pull https://FarhanZafarSahu@bitbucket.org/FarhanZafarSahu/deliveryappbackend.git master');
	}
 	public function artisanCommand($name)
	{
		 \Artisan::call($name);
		return \Artisan::output();
	}
}
