<?php

namespace App\Http\Controllers\Api;

use App\Constants\Message;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;

use App\Constants\ResponseCode;
use App\Customer;
use App\User;
use App\UserSubscriptionPlan;
use Carbon\Carbon;

class CustomerController extends Controller
{
// function __construct()
// {
//     \Config::set('jwt.user', Customer::class);
// 	\Config::set('auth.providers', ['users' => [
//             'driver' => 'eloquent',
//             'model' => Customer::class,
//         ]]);
// }
     public function register(Request $request)
    {
        $response = defaultErrorResponse();
		$request->contactNo = substr($request->contactNo , -10);
        $validator = validateData($request,'REGISTER_CUSTOMER');
		if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        {
            $password       = $request->password;
            $name     		= $request->name;
            $roleId         = 4;
        	$request->phone_number = substr($request->phone_number,-10);
            $sub_total    = "public/propertyThumbnail/userImage.jpeg";
            Customer::create([
                "password"        => bcrypt($password),
                "name"        	  => $name,
                "image"       	  => $sub_total,
           		'phone'	  		  => $request->phone_number ?? NULL,
			]);
           // SendEmailJob::dispatch($name);
            \DB::commit();
 			$credentials = [
					'phone' => $request->phone_number,
					'password' => $request->password,
					];
 			return attemptJWTLogin($credentials, 0 ,  $request->FCMToken, $request->macAddress);
        }
        catch (\Exception $e)
        {
 			\DB::rollBack();
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function order(Request $request)
    {
        $response = defaultErrorResponse();
        $validator = validateData($request,'Order');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
       try
        {
            $user              =  \JWTAuth::user();
            $customer_id       =  $user->id;
            $deliveryAddress   = $request->buyer_address;
            $deliveryName      = $request->buyer_name;
            $buyerContactNo    = $request->buyer_contact_no;
			$totalOrderAmount  = $request->sub_total;
			$no_of_items 	   = $request->no_of_items;
			$discountAmount    = $request->discount_amount;
			$cartObject 	   = json_decode($request->cart_object);
			$orderTime 		   = $request->order_time;
			$orderNo = 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			$order = Order::store($no_of_items, $totalOrderAmount, $discountAmount , $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer_id, $orderTime, 0);
			foreach($cartObject as $cart)
			{
				OrderDetail::store($order->id, $cart->product_id, $cart->quantity, $cart->per_unit);
			}
			\DB::commit();
			$result = [
			'order' => $order,
		];
		$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
        }
        catch (\Exception $e)
        {
  			\DB::rollBack();
            return makeResponse(ResponseCode::ERROR,$e->getMessage(),false, $e->getMessage());
        }
        return $response;
    }
  	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscription(Request $request)
    {
		dd($request->all());
        $response = defaultErrorResponse();
        $validator = validateData($request,'SUBSCRIPTION');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
       try
        {
            $user             		=  \JWTAuth::user();
            $customer_id       		=  $user->id;
            $deliveryAddress   		= $request->buyer_address;
            $deliveryName      		= $request->buyer_name;
            $buyerContactNo    		= $request->buyer_contact_no;
			$totalOrderAmount  		= $request->sub_total;
			$no_of_items 	   		= 1;
			$discountAmount    		= $request->discount_amount;
			$orderTime 		   		= $request->order_time;
			$plan_id  		   		= $request->plan_id;
			$quantity          		= $request->quantity;
			$emptyJarQty       		= $request->empty_jar_qty;
			$perUnitEmptyJarPrice 	= $request->per_unit_empty_jar_price;
			$perUnit 		   		= $request->product_per_unit_price;
			$productId 		   		= $request->product_id;
			$orderNo 				= 'B#'.substr(time(), 6, 4).rand(1000,9999).\carbon\Carbon::now()->day;
			$isAdvancePayment		= $request->is_advance_payment;
			$productType			= $request->product_type;
			// calculate next date of delivery.
			$next_delivery_date = Order::CalculateNextDeliveryDate($plan_id);
			$subscription 	= UserSubscriptionPlan::store($customer_id, $plan_id, $quantity, $emptyJarQty, $perUnitEmptyJarPrice, $productId, $next_delivery_date , $isAdvancePayment , $productType);
			$order = Order::store($no_of_items, $totalOrderAmount, $discountAmount , $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer_id , $orderTime , 0 , $subscription->id);
			OrderDetail::store($order->id, $productId, $quantity, $perUnit);
			\DB::commit();
			$result = [
			'order' => $order,
		];
		$response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
        }
        catch (\Exception $e)
        {
  			\DB::rollBack();
            return makeResponse(ResponseCode::ERROR,$e->getMessage(),false, $e->getMessage());
        }
        return $response;
    }

 public function search(Request $request)
    {
        $user =  \JWTAuth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'SEARCH_CUSTOMER');
        if ($validator['status'])
            return makeResponse(ResponseCode::FAIL,$validator['errors']->first(),false,$validator['errors']);
        \DB::beginTransaction();
        try
        {
            $searchValue   = $request->search;
            $group    = $request->group;
            $currentUserId = isset($user) ? $user->id : 0 ;
            $result = Customer::Search($searchValue,$currentUserId);
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL, $e->getMessage(), false);
        }
        return $response;
    }
 public function login(Request $request)
    {
		$request->phone_number = substr($request->phone_number , -10);
		if(!isset($request['phone_number'],$request['password']))
            return makeResponse(ResponseCode::FAIL,'Invalid '.isset($credentials) ? "Contact # " : "email" .' or Password',false);
	    $credentials = [
					'phone' => $request->phone_number,
					'password' => $request->password,
					];
		return attemptJWTLogin($credentials, 0, $request->FCMToken, $request->macAddress);
    }
  public function logout(Request $request)
    {
        $user =  \JWTAuth::user();
        $response = defaultErrorResponse();
        isset($request->FCMToken)? $FCMToken = $request->FCMToken : $FCMToken = null;
        \DB::beginTransaction();
        try
		{
          	auth()->logout();
        	$response = makeResponse(ResponseCode::SUCCESS,"Logout Successfully.",true);
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }
        return $response;
    }

	public function plansList(Request $request)
	 {
        try 
        {
        	$plans = UserSubscriptionPlan::where('subscription_end_date','<',\Carbon\Carbon::now()->format('Y-m-d H:i:s'))->where('user_id',$request->user_id)->get();
            
            
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$plans); 
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }

        return $response;    
    }
}
