<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Resources\Order\PaginatedOrderResource;
use App\Order;
use Illuminate\Http\Request;

use App\Constants\Message;
use App\Constants\ResponseCode;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function list(Request $request)
    {
        $user =  \JWTAuth::user();
        $response = defaultErrorResponse();
        try
        {
				if($request->isVendor == 0)
				{	
					$order_details  = Order::where('customer_id',$user->id)
                                        ->where('status', '!=', 0)
										->WhereNull('cancel_reason',)
                                        ->orderBy('created_at','DESC')
                                        ->paginate(12);
				}
				else if ($request->isVendor == 1)
				{

				$customer_list = Customer::where('vendor_id',$user->id)->pluck('id');
				$order_details  = Order::whereIn('customer_id',$customer_list)
                                        ->where('status', '!=', 0)
										->WhereNull('cancel_reason',)
                                        ->orderBy('created_at','DESC')
                                        ->paginate(12);
    			}
           		$result = new PaginatedOrderResource($order_details);
            return makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$result);
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::ERROR,$e->getMessage(),false, $e->getMessage());
        }
        return $response;
    }

}
