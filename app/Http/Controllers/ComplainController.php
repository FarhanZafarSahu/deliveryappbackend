<?php

namespace App\Http\Controllers;

use App\Complain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Constants\ResponseCode;
use App\Constants\Message;
use App\User;

class ComplainController extends Controller
{
    public function list()
    {
		$user = \Auth::user();
		$check = ($user->role_id == 3) ? 'vendor_id' : 'customer_id';
        $complains = Complain::where($check,$user->id)->get();
        return view('complains.list',compact('complains'));
    }

    public function add()
    {
        return view('complains.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {

            $complain =   Complain::create([
                                "vendor_id"      => Auth::id(),
                                "name"           => $request->name,
								"phone_number"   => $request->phone_number,
                                "message"        =>  $request->message
							]);

                            return redirect()->route('complain.list');

    }

    public function reply(Request $request, $id)
    {
        $complain = Complain::find($id);
        $replyto = User::where('id',$complain->vendor_id)->get();
        return view('complains.reply', compact('complain','replyto'));
    }

    public function response(Request $request, $id)
    {
        $complain = Complain::find($id);
        $complain->status = 1;
        $complain->complain_reply = $request->message;
        $complain->save();
        return redirect()->route('complain.list');
    }

    public function delete(Request $request , $id)
    {
    	 \DB::beginTransaction();
        try
        {
            $complain = Complain::where('id',$id)->first();
            if(empty($complain))
               return \Redirect::back()->with('error', 'Complain not found.');
            $complain->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'Complain deleted');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function isApprove(Request $request , $id)
    {
    	 \DB::beginTransaction();
        try
        {
            $complain = Complain::where('id',$id)->first();
            if(empty($complain))
               return \Redirect::back()->with('error', 'complain not found.');
            if($complain->status==1){
                $complain->status=0;
                $complain->save();
            }
            else{
                $complain->status=1;
                $complain->save();
            }

            \DB::commit();
            return \Redirect::back()->with('success', 'Complain status updated');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
}
