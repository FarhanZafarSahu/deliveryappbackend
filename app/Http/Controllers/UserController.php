<?php

namespace App\Http\Controllers;

use App\City;
use App\User;
use Illuminate\Http\Request;

use App\Constants\ResponseCode;
use App\Constants\Message;
use App\Group;

class UserController extends Controller
{
    public function listCity(Request $request)
    {
		$response = defaultErrorResponse();
        try 
        {
          
                $cities =  Group::all();
            $response = makeResponse(ResponseCode::SUCCESS,Message::REQUEST_SUCCESSFUL,true,null,$cities); 
        }
        catch (\Exception $e)
        {
            $response = makeResponse(ResponseCode::FAIL,$e->getMessage(),false);
        }

        return $response;    
    }
}
