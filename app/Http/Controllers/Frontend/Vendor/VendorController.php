<?php

namespace App\Http\Controllers\Frontend\Vendor;

use App\Constants\Message;
use App\Customer;
use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Resources\SubscriptionHistory\UserSubscriptionHistoryCollection;
use App\Order;
use App\OrderDetail;
use App\Plan;
use App\Product;
use App\SubscriptionPaymentHistory;
use App\User;
use App\UserSubscriptionPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Claims\Custom;
use App\Traits\UploadImageTrait;
use Illuminate\Support\Facades\Validator;

class VendorController extends Controller
{
	use UploadImageTrait;
	public function show_login()
	{
		if (\Auth::guard('web')->check()) {
			return redirect()->route('vendor-homefeed');
		}
		return view('frontend.vendor.login');
	}
	public function vendorlogin(Request $request)
	{
		$credentials = [
			'phone_number'       => $request->phone_number,
			'password'  => $request->password,
		];
		if (Auth::guard('web')->attempt($credentials)) {
			$vendor = Auth::guard('web')->user();
			if ($vendor['business_name'])
				return redirect()->route('vendor-homefeed');
			else
				return redirect()->route('completeProfile');
		} else {
			return back()->with('error', 'Invalid Credentials.');
		}
	}
	public function register(Request $request)
	{
		$valid = Validator::make($request->all(), [
			'name' => 'required',
			'phone_number' => 'required|unique:users',
		]);
		if ($valid->fails()) {
			return back()->with('error', 'Phone number is required, and must be unique.');
		}
		\Session::put('vendor_registration', $request->all());
		$otp = rand(1000, 9999);
		$sendOtp = sendSMS($otp, $request->phone);
		if (property_exists($sendOtp, 'return')) {
			if ($sendOtp->return) {
				session()->put('otp', $otp);
				return redirect()->route('vendor.verification')->with('success', 'OTP Send Successfully');
			} else {
				return back()->with('error', $sendOtp->message);
			}
		} else {
			return back()->with('error', 'Otp can not be sent, Please try Again');
		}
		return redirect()->route('vendor.verification');
	}
	public function verification_page()
	{
		return view('frontend.vendor.verifyOtp');
	}
	public function verifyOtp(Request $request)
	{
		$otp = $request->otp;
		$existOtp = session()->get('otp');
		if ($otp == $existOtp) {
			$data = \Session::get('vendor_registration');
			// dd($data);
			$vendor = new User();
			$vendor->phone_number = $data['phone_number'];
			$vendor->email = $data['email'];
			$vendor->name = $data['name'];

			$vendor->password = Hash::make($data['password']);
			$vendor->email_verified_at = now();
			$vendor->save();
			$credentials = [
				'phone_number' => $data['phone_number'],
				'password'     => $data['password'],
			];
			Auth::guard('web')->attempt($credentials);
			\Session::forget('vendor_registration');
			return redirect()->route('vendor-homefeed');
		} else {
			return back()->with('error', 'Invalid OTP');
		}
	}
	public function show_register()
	{
		if (Auth::check()) {
			return redirect()->intended('vendor-homefeed');
		}
		return view('frontend.vendor.register');
	}
	public function home_feed(Request $request)
	{
		if (Auth::guard('web')->user()->business_name && Auth::guard('web')->user()->is_approved == 1)
			return view('frontend.vendor.homefeed');
		elseif (!(Auth::guard('web')->user()->business_name))
			return redirect()->route('completeProfile');
		else
			return view('frontend.vendor.verificationRequestSent');
	}
	public function todayDelivery(Request $request, $groupId)
	{
		$userIds = Customer::where('group_id', $groupId)->pluck('id');
		$customers = UserSubscriptionPlan::whereIn('user_id', $userIds)->where('next_delivery_date', \Carbon\Carbon::now()->format('y-m-d'))->with('customer')->get();
		return view('frontend.vendor.customer.todayDelivery')->with(['customers' => $customers]);
	}
	public function complete_profile(Request $request)
	{
		$vendor = Auth::user();
		return view('frontend.vendor.completeProfile', compact('vendor'));
	}
	public function complete_profile_post(Request $request)
	{
		$vendor = User::where('id', $request->id)->first();
		if ($vendor) {
			$vendor->name = $request->name;
			$vendor->email = $request->email;
			$vendor->phone_number = $request->phone_number;
			$vendor->brand_name = $request->brand_name;
			$vendor->business_name = $request->business_name;
			$vendor->business_address = $request->business_address;
			$vendor->alternative_phone_number = $request->alternative_phone_number;
			$vendor->role_id = 3;
			$vendor->save();
			return redirect()->route('vendor-homefeed');
		}
		return \Redirect::back()->with('error', Message::INVALID_INPUT_VALUES);
	}
	public function logout(Request $request)
	{
		Auth::guard('web')->logout();
		return redirect()->route('vendorlogin');
	}
	public function allCustomer(Request $request)
	{
		$vendorId = Auth::guard('web')->user()->id;
		$tnhCustomer = Customer::where('vendor_id', $vendorId)->whereNotNull('assigned_by')->get();
		$personalCustomer = Customer::where('vendor_id', $vendorId)->whereNull('assigned_by')->get();
		$result = [
			'tnhCustomer' => $tnhCustomer,
			'personalCustomer' => $personalCustomer,
		];
		return view('frontend.vendor.allCustomer')->with(['result' => $result]);
	}
	public function allDeliveryBoy(Request $request)
	{
		$deliveryBoys = User::where('role_id', 5)->get();
		$result = [
			'deliveryBoys' => $deliveryBoys,
		];
		return view('frontend.vendor.deliveryboy.index')->with(['result' => $result]);
	}
	public function customerView1(Request $request, $id)
	{
		$customer = Customer::where('id', $id)->first();
		$plan  = UserSubscriptionPlan::where('subscription_end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d H:i:s'))->where('user_id', $id)->with('product')->first();
		$order_ids = Order::where('is_accept', 1)->where('customer_id', $id)->where('subscription_id', $plan->id ?? 0)->pluck('id');
		$out_qty = OrderDetail::whereIn('order_id', $order_ids)->sum('quantity');
		$total_balance = $plan->total_jar_qty ?? 0 - $out_qty;
		$result = [
			'customer' => $customer,
			'plan' => $plan,
			'total_balance' => $total_balance,
			'out_qty' => $out_qty,
		];
		return view('frontend.vendor.customer.customerview')->with(['result' => $result]);
	}
	public function customerView(Request $request, $id)
	{
		$customer = Customer::where('id', $id)->first();
		$subscription = UserSubscriptionPlan::with('plan')->where('user_id', $customer->id)->get();
		$history = UserSubscriptionHistoryCollection::collection($subscription);
		$subscription_history = collect($history)->toArray();

		$result = [
			'subscription_history' => $subscription_history,
			'active_subscription'	=> UserSubscriptionPlan::where('subscription_end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d H:i:s'))->where('user_id', $id)->first(),
			'customer'	=> $customer,
		];
		return view('frontend.vendor.customer.customerview')->with(['result' => $result]);
	}
	public function addCustomer(Request $request)
	{
		$products  = Product::all();
		$plans 	   = Plan::all();
		$result = [
			'products' => $products,
			'plans'	=> $plans,
		];
		return view('frontend.vendor.customer.addCustomer')->with(['result' => $result]);
	}
	public function addGroup(Request $request)
	{

		return view('frontend.vendor.groups.add');
	}
	public function addDeliverBoy(Request $request)
	{

		return view('frontend.vendor.deliveryboy.add');
	}
	public function storeCustomer(Request $request)
	{
		$user =  \Auth::user();
		\DB::beginTransaction();
		try {
			$qr         			= str_pad(rand(1, 999999999999), 7, '0', STR_PAD_LEFT);
			$plan_id    			= $request->plan_id;
			$quantity 			    = $request->quantity;
			$emptyJarQty		    = 0;
			$perUnitEmptyJarPrice	= 0;
			$productId 				= $request->product_id;
			$product 				= Product::where('id', $productId)->first();
			$no_of_items 			= 1;
			$deliveryCharges 		= ($quantity * config('constant.DELIVERY_CHARGES'));
			$totalOrderAmount 		= $product->price * $quantity + $deliveryCharges;
			$discountAmount 		= $product->price * $quantity + $deliveryCharges;
			$orderTime 				= \carbon\Carbon::now();
			$buyerContactNo 		= $request->phone;
			$deliveryName 			= $request->name;
			$deliveryAddress 		= $request->address;
			$isAdvancePayment		= 0;
			$productType			= $product->product_type;

			$customer   = Customer::create([
				"name"           => $request->name,
				"email"          => $request->email,
				"address"        => $request->address,
				"phone"          => $request->phone,
				"qr"             => $qr,
				"vendor_id"    	 => $user->id,
			]);
			$orderNo    		= 'B#' . substr(time(), 6, 4) . rand(1000, 9999) . \carbon\Carbon::now()->day;
			// calculate next date of delivery.
			$next_delivery_date = Order::CalculateNextDeliveryDate($plan_id);
			$subscription 		= UserSubscriptionPlan::store($customer->id, $plan_id, $quantity, $emptyJarQty, $perUnitEmptyJarPrice, $productId, $next_delivery_date, $isAdvancePayment, $productType);
			$order 				= Order::store($no_of_items, $totalOrderAmount, $discountAmount, $orderTime, $orderNo, $buyerContactNo, $deliveryName, $deliveryAddress, $customer->id,  $orderTime, $quantity, $subscription->id);

			OrderDetail::store($order->id, $productId, $quantity, $product->price);

			\DB::commit();
			return \Redirect::back()->with('success', 'Customer Added');
		} catch (\Exception $e) {
			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
		}
	}
	public function storeDeliveryBoy(Request $request)
	{
		$user =  \Auth::user();
		$response = defaultErrorResponse();
		$validator = validateData($request, 'ADD_DELIVERY_BOY_APP');
		if ($validator['status'])
			return \Redirect::back()->with('error', $validator['errors']->first());
		\DB::beginTransaction();
		try {
			$thumbnailUrl	= $this->uploadImageTrait($request, 'image', 'public/deliveryBoy');
			if ($thumbnailUrl == null)
				return \Redirect::back()->with('error', "File Required");
			$customer =   User::create([
				"name"            => $request->name,
				"phone_number"   => $request->phone,
				"image"		=>	$thumbnailUrl,
				"role_id"	=> 5
			]);
			\DB::commit();
			return \Redirect()->route('vendor.allDeliveryBoy')->with('success', 'Delivery Boy  Added');
		} catch (\Exception $e) {
			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
		}
		return $response;
	}
	public function allGroups(Request $request)
	{
		$vendorId = Auth::guard('web')->user()->id;
		$allGroups = Group::where('user_id', $vendorId)->get();
		$result = [
			'allGroups' => $allGroups,
		];
		return view('frontend.vendor.groups.dashboard')->with(['result' => $result]);
	}
	public function todayallGroups(Request $request)
	{
		$vendorId = Auth::guard('web')->user()->id;
		$allGroups = Group::where('user_id', $vendorId)->get();
		$result = [
			'allGroups' => $allGroups,
		];
		return view('frontend.vendor.groups.alltodaygroups')->with(['result' => $result]);
	}
	public function singleGroupDetail(Request $request, $id)
	{
		$group = Group::where('id', $id)->withcount('customer')->with('customer')->first();
		$customers = Customer::whereIn('vendor_id', User::where('group_id', $id)->pluck('id'))->get();
		$result = [
			'group' => $group,
			'customer' => $customers,
		];
		return view('frontend.vendor.groups.detail')->with(['result' => $result]);
	}
	public function assignGroup($id)
	{
		$user =  \Auth::user();
		try {
			$deliveryBoy = User::where('id', $id)->first();
			$groups = Group::all();
			if (empty($deliveryBoy))
				return \Redirect::back()->with('error', 'deliveryBoy not found.');
			return view('frontend.vendor.deliveryboy.assignGroup')->with(['deliveryBoy' => $deliveryBoy, 'groups' => $groups]);
		} catch (\Exception $e) {
			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
		}
	}
	public function postAssignGroup(Request $request)
	{
		$user = \Auth::user();
		$response = defaultErrorResponse();
		$validator = validateData($request, 'ASSIGN_GROUP_DELIVERY_BOY');
		if ($validator['status'])
			return \Redirect::back()->with('error', $validator['errors']);
		\DB::beginTransaction();
		try {
			$id    = $request->user_id;
			$groupId = $request->group_id;
			$deliveryBoy   	  = User::where('id', $id)->first();
			if (empty($deliveryBoy))
				return $response = makeResponse(ResponseCode::FAIL, "deliveryBoy not found", false, null);
			User::where('id', $id)
				->update([
					"group_id"            => $groupId,
				]);

			\DB::commit();
			return \Redirect()->route('vendor.allDeliveryBoy')->with('success', "Group Assign to deliveryBoy");
		} catch (\Exception $e) {
			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
		}
		return $response;
	}
	public function postAddGroup(Request $request)
	{
		$user =  \Auth::user();
		$response = defaultErrorResponse();
		$validator = validateData($request, 'ADD_GROUP');
		if ($validator['status'])
			return \Redirect::back()->with('error', $validator['errors']->first());
		\DB::beginTransaction();
		try {
			$customer =   Group::create([
				"name"            => $request->name,
				"user_id"        => $user->id,
			]);
			\DB::commit();
			return \Redirect()->route('vendor.allGroups')->with('success', 'Group Added');
		} catch (\Exception $e) {
			\DB::rollBack();
			return \Redirect::back()->with('error', $e->getMessage());
		}
		return $response;
	}
	public function subscriptionPayment(Request $request)
	{
		$subcription = UserSubscriptionPlan::where('id', $request->subscription_id)->first();
		if ($subcription == null) {
			return \Redirect::back()->with('error', Message::INVALID_INPUT_VALUES);
		}
		$paymentHistory = new SubscriptionPaymentHistory();
		$paymentHistory->qty = $subcription->qty;
		$paymentHistory->subscription_id = $request->subscription_id;
		$paymentHistory->per_unit_price = $subcription->product->price;
		if ($request->payed_amount == $subcription->qty * $subcription->product->price) {
			$paymentHistory->total_amount = $subcription->qty * $subcription->product->price;
			$paymentHistory->payed_amount = $request->payed_amount;
			$paymentHistory->balance_amount = 0;
		} else if ($request->payed_amount < $subcription->qty * $subcription->product->price) {
			$difference = $subcription->qty * $subcription->product->price - $request->payed_amount;
			Customer::where('id', $subcription->user_id)->decrement("balance_amount", $difference);
			$paymentHistory->total_amount = $subcription->qty * $subcription->product->price;
			$paymentHistory->payed_amount = $request->payed_amount;
			$paymentHistory->balance_amount = $difference;
		} else {
			$difference =  $request->payed_amount  - $subcription->qty * $subcription->product->price;
			Customer::where('id', $subcription->user_id)->increment("balance_amount", $difference);
			$paymentHistory->total_amount = $subcription->qty * $subcription->product->price;
			$paymentHistory->payed_amount = $request->payed_amount;
			$paymentHistory->balance_amount = $difference;
		}
		$paymentHistory->save();
		return \Redirect::back()->with('success', Message::REQUEST_SUCCESSFUL);
	}
}
