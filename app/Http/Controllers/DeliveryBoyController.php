<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;

class DeliveryBoyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
         $deliveryBoys = User::where('role_id', 5)->get();
		$result = [
			'deliveryBoys' => $deliveryBoys,
			];

		return view('deliveryBoys.index')->with(['result'=> $result]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('deliveryBoys.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user =  \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request, 'ADD_DELIVERY_BOY');
        if ($validator['status'])
               return \Redirect::back()->with('error', $validator['errors']->first());
        \DB::beginTransaction();
        try
        {
            $customer =   User::create([
                                "name"            => $request->name,
                                "email"      => $request->email,
								"business_address"   => $request->address,
								"land_mark"   => $request->land_mark,
								"phone_number"   => $request->phone,
								"password"		=> bcrypt($request->password),
								"role_id"	=> 5
							]);
            \DB::commit();
               return \Redirect()->route('deliveryboy.list')->with('success', 'Delivery Boy  Added');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
               return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =  \Auth::user();
        try
        {
            $deliveryBoy = User::where('id',$id)->first();
            if(empty($deliveryBoy))
               return \Redirect::back()->with('error', 'deliveryBoy not found.');
            return view('deliveryBoys.edit')->with(['deliveryBoy'=> $deliveryBoy]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
 	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assignGroup($id)
    {
        $user =  \Auth::user();
        try
        {
            $deliveryBoy = User::where('id',$id)->first();
			$groups = Group::where('user_id',$user->id)->get();
            if(empty($deliveryBoy))
               return \Redirect::back()->with('error', 'deliveryBoy not found.');
            return view('deliveryBoys.assignGroup')->with(['deliveryBoy'=> $deliveryBoy,'groups' => $groups]);
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'UPDATE_DElIVERY_BOY');
        if ($validator['status'])
            return \Redirect::back()->with('error', $validator['errors']);
       \DB::beginTransaction();
        try
        {
            $id    = $request->id;
			$name = $request->name;
			$email   = $request->email;
			$address   = $request->address;
			$phone   = $request->phone;
			$land_mark   = $request->land_mark;
			
            $deliveryBoy   	  = User::where('id',$id)->first();
            if(empty($deliveryBoy))
                return $response = makeResponse(ResponseCode::FAIL, "deliveryBoy not found", false, null);
            User::where('id',$id)
                    ->update([
                      		 	 "name"            => $name,
                                "email"      => $email,
								"business_address"   => $address,
								"land_mark"   => $land_mark,
								"phone_number"   => $phone,
					]);

            \DB::commit();
            return \Redirect()->route('deliveryboy.list')->with('success', "deliveryBoy Updated");
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }
 /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postassignGroup(Request $request)
    {
        $user = \Auth::user();
        $response = defaultErrorResponse();
        $validator = validateData($request,'ASSIGN_GROUP_DELIVERY_BOY');
        if ($validator['status'])
            return \Redirect::back()->with('error', $validator['errors']);
       \DB::beginTransaction();
        try
        {
            $id    = $request->user_id;
			$groupId = $request->group_id;
            $deliveryBoy   	  = User::where('id',$id)->first();
            if(empty($deliveryBoy))
                return $response = makeResponse(ResponseCode::FAIL, "deliveryBoy not found", false, null);
            User::where('id',$id)
                    ->update([
                      		 	 "group_id"            => $groupId,
					]);

            \DB::commit();
            return \Redirect()->route('deliveryboy.list')->with('success', "Group Assign to deliveryBoy");
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
        return $response;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
            $deliveryBoy = User::where('id',$id)->first();
            if(empty($deliveryBoy))
               return \Redirect::back()->with('error', 'deliveryBoy not found.');
            $deliveryBoy->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'deliveryBoy deleted');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
}
