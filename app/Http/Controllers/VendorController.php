<?php

namespace App\Http\Controllers;

use App\User;
use App\VendorBankDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class VendorController extends Controller
{
    public function list()
    {
        $vendors = User::where('role_id',3)->get();
        return view('vendors.list',compact('vendors'));
    }
    public function add()
    {
        return view('vendors.add');
    }
    public function edit($id=null){
        $vendor = User::with('bankDetails')->findOrFail($id);
        // $bank = VendorBankDetail::where('user_id',$vendor->id)->first();
        return view('vendors.edit',compact('vendor'));
    }

    public function save(Request $req, $id = null)
    {
        if ($req->hasFile('document'))
				{
					$propertyThumbnailDirectory = 'public/document';
					if (!\Storage::exists($propertyThumbnailDirectory))
					{
						\Storage::makeDirectory($propertyThumbnailDirectory);
					}
					$thumbnailUrl = \Storage::putFile($propertyThumbnailDirectory, $req->file('document'));
					$document = $thumbnailUrl;
                    $document = str_replace(array("public/"), "", $document);
				}


        if (is_null($id)) {
            $req->validate([
                'name' => 'required',
                'email' => 'required|email',
                // 'password' => 'required',
            ]);
        }

        if (is_null($id)) {
            $user = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'phone_number' => $req->phone_number,
                'brand_name' => $req->brand_name,
                'photo'    => 'default.png',
                'business_name' => $req->business_name,
                'business_address' => $req->business_address,
                'pin_code' => $req->pin_code,
                'alternative_phone_number' => $req->alternative_phone_number,
                'sale_price'  => $req->sale_price,
                'license_no'  => $req->license_no,
                'document'  => $document,
                'password' => Hash::make('12345678'),
                'role_id' => 3
            ]);
            $bank = VendorBankDetail::create([
                 'user_id' => $user->id,
                 'bank_name' => $req->bank_name,
                 'account_holder' => $req->account_holder,
                 'account_number' => $req->account_number,
                 'account_type'   => $req->account_type,
                 'IFSC_code'     => $req->IFSC_code,
                 'branch_name'  => $req->branch_name
            ]);
            $msg = "Record Added Successfully!";
        } else {
            $vendor = User::findOrFail($id);
                $vendor->name = $req->name;
                $vendor->email = $req->email;
                $vendor->phone_number = $req->phone_number;
                $vendor->brand_name = $req->brand_name;
                $vendor->photo    = 'default.png';
                $vendor->business_name = $req->business_name;
                $vendor->business_address = $req->business_address;
                $vendor->pin_code = $req->pin_code;
                $vendor->license_no = $req->license_no;
               $vendor->sale_price  = $req->sale_price;
            //    $vendor->document  = $req->document;
                $vendor->alternative_phone_number = $req->alternative_phone_number;
                $vendor->password = Hash::make('12345678');
            $vendor->save();

            $bank = VendorBankDetail::where('user_id',$id)->update([
                'user_id' => $id,
                'bank_name' => $req->bank_name,
                'account_holder' => $req->account_holder,
                'account_number' => $req->account_number,
                'account_type'   => $req->account_type,
                'IFSC_code'     => $req->IFSC_code,
                'branch_name'  => $req->branch_name
           ]);

            $msg = "Record Edited Successfully!";
        }

        return redirect()->back()->with('success', $msg);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request , $id)
    {
        $user =  \Auth::user();
    	 \DB::beginTransaction();
        try
        {
            $vendor = User::where('id',$id)->first();
            if(empty($vendor))
               return \Redirect::back()->with('error', 'vendor not found.');
            $vendor->delete();
            \DB::commit();
            return \Redirect::back()->with('success', 'vendor deleted');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function isApprove(Request $request , $id)
    {
    	 \DB::beginTransaction();
        try
        {
            $vendor = User::where('id',$id)->first();
            if(empty($vendor))
               return \Redirect::back()->with('error', 'vendor not found.');
            if($vendor->is_approved==1){
                $vendor->is_approved=0;
                $vendor->save();
            }
            else{
                $vendor->is_approved=1;
                $vendor->save();
            }

            \DB::commit();
            return \Redirect::back()->with('success', 'Vendor approval updated');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function isActive(Request $request , $id)
    {
    	 \DB::beginTransaction();
        try
        {
            $vendor = User::where('id',$id)->first();
            if(empty($vendor))
               return \Redirect::back()->with('error', 'Vendor not found.');
            if($vendor->is_active==1){
                $vendor->is_active=0;
                $vendor->save();
            }
            else{
                $vendor->is_active=1;
                $vendor->save();
            }

            \DB::commit();
            return \Redirect::back()->with('success', 'Vendor activation updated');
        }
        catch (\Exception $e)
        {
            \DB::rollBack();
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function documentView($id)
    {
        try
        {
            $user       = User::find($id);
            $userDoc = $user->document;
            if(!$userDoc)
               return back()->with('error','No Document found.');
            return view('vendors.document',compact('userDoc'));
         }
        catch (\Exception $e)
        {
            return \Redirect::back()->with('error', $e->getMessage());
        }
    }
    public function detail($id)
    {
        $vendor = User::with('bankDetails')->find($id);
        return view('vendors.detail',compact('vendor'));
    }
}
