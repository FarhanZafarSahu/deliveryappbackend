<?php

use App\Complain;
use App\Coupon;
use App\Customer;
use App\Feedback;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

function uploadFile($file, $path, $name)
{
    $name = $name . '.' . $file->getClientOriginalExtension();
    $file->move($path, $name);
    return $path . '/' . $name;
}

function totalVendor()
{
    return User::where('role_id', 3)->count();
}
function totalUser()
{
    return User::count();
}
function totalComplain()
{
    return Complain::count();
}
function totalFeedback()
{
    return Feedback::count();
}
function totalCustomer()
{
    return Customer::count();
}
function newcustomer()
{
    return Customer::where('vendor_id', null)->count();
}
function recentCoupon()
{
    return Coupon::latest()->count();
}


function sendSMS($otp, $number)
{

    //send guzzle request to https://www.fast2sms.com/dev/bulkV2
    $client = new Client([
        'verify' => false,     
    ]);

    $headers = [
        'authorization' => env('SMS_API_KEY'),
    ];
    $options = [
        'multipart' => [
            [
                'name' => 'variables_values',
                'contents' => $otp
            ],
            [
                'name' => 'route',
                'contents' => 'otp'
            ],
            [
                'name' => 'numbers',
                'contents' => $number
            ]
        ]
    ];

    $request = new Request('POST', 'https://www.fast2sms.com/dev/bulkV2', $headers);
    $res = $client->sendAsync($request, $options)->wait();
    $h = $res->getBody();
    $h= json_decode($h);
    return $h;
}
