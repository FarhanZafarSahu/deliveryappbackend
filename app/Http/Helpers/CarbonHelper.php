<?php

// Constants
use App\Constants\General;

// Carbon
use Carbon\Carbon;

/**
 * Concat two string.
 *
 * @param  string value
 * @param  string format (optional)
 * @return string|object
 */
function carbonDate($value,$format=null)
{
	$dateTime = Carbon::parse($value);

	if(!is_null($format))
		$dateTime = $dateTime->format($format);

	return $dateTime;
}

/**
 * Concat two string.
 *
 * @param  string format (optional)
 * @return string|object
 */
function currentCarbonDate($format=null)
{
	$dateTime = Carbon::now();

	if(!is_null($format))
		$dateTime = $dateTime->format($format);

	return $dateTime;
}

/**
 * Concat two string.
 *
 * @param  string date
 * @param  string filter
 * @return array
 */
function getDateRange($date,$filter,$date1=null)
{
	$SD = $ED = "";
	$format  = General::DATE_FORMAT_1;
	$date = carbonDate($date);

	if($filter==General::FILTER_WEEK)
	{
		$SD = carbonDate($date->startOfWeek(),$format);
		$ED = carbonDate($date->endOfWeek(),$format);
	}
	elseif($filter==General::FILTER_MONTH)
	{
		$SD = carbonDate($date->startOfMonth(),$format);
		$ED = carbonDate($date->endOfMonth(),$format);
	}
	elseif($filter==General::FILTER_YEAR)
	{
		$SD = carbonDate($date->startOfYear(),$format);
		$ED = carbonDate($date->endOfYear(),$format);
	}
	elseif($filter==General::FILTER_TODAY)
	{
		$SD = carbonDate($date->startOfDay(),$format);
		$ED = carbonDate($date->startOfDay(),$format);
	}
	else
	{
		$SD = carbonDate($date,$format);
		$ED = carbonDate($date1,$format);
	}


	return [$SD,$ED];
}