<?php

// Constants
use App\Constants\General;

// Models
use App\Constants\Message;
use App\Constants\ResponseCode;

// Constants
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Order;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

/**
 * Concat two string.
 *
 * @param  string $strA
 * @param  string $strB
 * @param  string $strBetween (optional)
 * @return string
 */
function concatTwoString($strA, $strB, $strBetween = '')
{
    return $strA . $strBetween . $strB;
}

/**
 * Generate SD,ED Range.
 *
 * @param  integer $filter
 * @param  string $SD
 * @param  string $ED
 * @param  string $schoolTimeZone
 * @return array
 */
function generateActivityDatesRange($filter, $customSD, $customED, $schoolTimeZone)
{
    $SD = $ED = "";
    $format = General::DATE_FORMAT_1;

    if ($filter != General::FILTER_CUSTOM) {
        list($SD, $ED) = getDateRange(date($format, strtotime($schoolTimeZone)), $filter);
    } else {
        $SD = carbonDate($customSD, $format);
        $ED = carbonDate($customED, $format);
    }
    return [$SD, $ED];
}

function calculatePercentage($valueA, $valueB, $decimalPlaces = 2)
{
    $result = ($valueA / $valueB) * 100;
    return number_format($result, $decimalPlaces, '.', '');
}

function attemptJWTLogin($credentials, $roleType = 0, $FCMToken = null, $macAddress = null)
{
    $token = null;
    try
    {
        if (!$token = JWTAuth::attempt($credentials)) {
            return getInvalidCredentialsResponse();
        }

    } catch (JWTAuthException $e) {
        return getTokenFailureResponse();
    }
    $user = JWTAuth::user();
    // if ($user->isUserDeleted()) {
    //     return makeResponse(ResponseCode::FAIL, Message::USER_BLOCK, false);
    // }

    if (isset($FCMToken)) {
        $user->updateFcmToken($FCMToken, $macAddress);
    }

    $result = [
        'token' => $token,
        'info' => $user->getArrayResponse(),
    ];
    return makeResponse(ResponseCode::SUCCESS, Message::REQUEST_SUCCESSFUL, true, null, $result);
}

function linkActivator($route)
{
    return (strpos(Route::getCurrentRoute()->uri(), $route));
}

function customIsset($value)
{
    if ($value != null && $value != "" && $value != "NULL" && $value != "null") {
        return true;
    }

    return false;
}

function paginate($items, $perPage = 5, $page = null)
{
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $total = count($items);
    $currentpage = $page;
    $offset = ($currentpage * $perPage) - $perPage ;
    $itemstoshow = array_slice($items , $offset , $perPage);
    return new LengthAwarePaginator($itemstoshow ,$total ,$perPage,   $page,
    [
        'path' => LengthAwarePaginator::resolveCurrentPath(),
    ]);
}
function csvToArray($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}
function deepLink($id)
{
    try
    {
        $fbResult = [];
        $val = "https://gahhakapp.page.link/?link=https://gahhak.pk/product-page?id=".$id;
        $data = [
            'longDynamicLink' => $val
        ];
        $dataString = json_encode($data);
        $headers = [
            'Content-Type: application/json',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyBQzMLj8hE3NYyn5ThXlhcB4l___b23as4');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $fbResults = curl_exec($ch);
        return $fbResults;
    }
    catch (\Exception $e)
    {
        $fbResults =
        [
         'shortLink' => NULL,
        ];

        return json_encode($fbResults);
    }
}

function permission($page, $roleId)
{

    $adminArray  = [
        'complain',
		'vendors',
        'invoice',
        'feedback',
		// 'customer',
		'/delivery/lock',
        // 'slider'
		'order',

    ];
    $superAdminArray = [
		'coupon',
		'vendors',
		'complain',
		'invoice',
		// 'customer',
        'feedback',
		'about-us',
		'contact-us',
		'term-condition',
		'category',
		'product',
        'newcustomer',
        'setting',
		'deliveries',
		'/delivery/lock',
        // 'slider'
		'order',
];
    $vendorArray = [
		'deliveries',
		'complain',
		'feedback',
    	'group',
		'deliveryboy',
		'route',
        'customer',
    ];
    $customerArray = [
    ];
    if (in_array($page, $adminArray) && $roleId == 2) {
        return true;
    }
    if (in_array($page, $superAdminArray) && $roleId == 1) {
        return true;
    }
    if (in_array($page, $vendorArray) && $roleId == 3) {
        return true;
    }
    if (in_array($page, $customerArray) && $roleId == 4) {
        return true;
    }
    return false;
}
function escape_like($string)
{
    $search     = array('%', '_','#','&');
    $replace    = array('\%', '\_', '\#','\&');

    return str_replace($search, $replace, $string);
}
function DeductPayment($amount,$user,$paymentType)
{
	if($paymentType == 1)  // payment through Gahhak wallet
	{
		if($amount > $user->eWallet)  // payment through Gahhak wallet
			return false;
		User::where('id',$user->id)->decrement('eWallet', $amount);
		return true ;
	}
	return false;
}
function totalTodayDelivery()
{
return  Order::whereDate('order_time', Carbon::today())->count();
}

function totalTodayDoneDelivery()
{
return  Order::whereDate('order_time', Carbon::today())->where('status', 4)->count();
}
