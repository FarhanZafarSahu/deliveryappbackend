<?php

namespace App\Classes;

class ResponseMessage
{
	const REQUEST_SCUESSFUL 	= 'Request Successful.';
	const INVALID_TOKEN 		= 'Invalid Token! User Not Found.';
	const SOMETHING_WENT_WRONG 	= 'Something went wrong. Please try again later!';
	const INVALID_CREDENTIAL 	= 'Incorrect Email or Password.';
	const TOKEN_FAILURE 		= 'Fail to create token.';
	const INVALID_USER 			= 'Not a valid user.';
	const USER_BLOCK			= 'Your Dukan has been Blocked By the Admin.';
	
}