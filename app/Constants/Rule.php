<?php

namespace App\Constants;

class Rule
{
    // Rules According to API's
private static $rules =
[
	'ASSIGN_DELIVERY_ROUTE' => [
		'assign_d_id' => 'required',
		'user_id'  	  => 'required',
		],
	'ASSIGN_GROUP_DELIVERY_BOY' => [
		'group_id'   => 'required',
		'user_id'	 => 'required',
		],
	'ADD_ROUTE' => [
		'route_name' => 'required',
		'user_id'	=> 'required',
		],
    'ADD_COUPON' => [
            // 'couponAmount' =>'required|numeric|min:1',
            'couponCode' =>'required',
			'is_percentage' => 'required',

        ],
	'UPDATE_COUPON' => [
            // 'couponAmount' =>'required|numeric|min:1',
            'couponCode' =>'required',
			'is_percentage' => 'required',
        ],

'ADD_DELIVERY_BOY' => [
	'name' => 'required',
	'email' => 'required',
	'phone' => 'required',
	'address' => 'required',
	'land_mark' => 'required',
	'password' => 'required',

	],
'ADD_DELIVERY_BOY_APP' => [
	'name' => 'required',
	'phone' => 'required',
	'image' => 'required',
	],
'ADD_CUSTOMER' => [
	'name' => 'required',
	'email' => 'required',
	'phone' => 'required',
	'address' => 'required',
	'land_mark' => 'required',

	],
'UPDATE_CUSTOMER' => [
'name' => 'required',
	'email' => 'required',
	'phone' => 'required',
	'address' => 'required',
	'land_mark' => 'required',

	],
'UPDATE_DElIVERY_BOY'=> [
	'name' => 'required',
	'email' => 'required',
	'phone' => 'required',
	'address' => 'required',
	'land_mark' => 'required',

	],
    'ADD_COMPLAIN' => [
        'name' =>'required',
        'phone_number' => 'required',
        'message' =>'required',
    ],
	'REGISTER' => [
		'name' =>'required',
        'phone_number' => 'required',
		'password'          =>  'required|confirmed|min:4',
	],
	'REGISTER_CUSTOMER' => [
  		'name' =>'required',
        'phone_number' => 'required',
		'password'          =>  'required|confirmed|min:4',

	],
'REGISTER_CUSTOMER_NEW' => [
  		'name' 				=>'required',
        'phone'				=> 'required|unique:customers,phone',
		'password'          =>  'required|min:4',

	],
	'VENDOR_CUSTOMER' => [
		  'name' =>'required',
        'phone' => 'required',
		'isJar'          =>  'required',
		'paymentType'          =>  'required',
	],
	'VENDOR_EDIT_PROFILE' => [
		'name' => 'required',
		'phoneNumber' => 'required',
		'brandName' => 'required',
		'businessName' => 'required',
		'businessAddress' => 'required',
		'pinCode' => 'required',
		'alternativePhoneNumber' => 'required',
		'email'	=> 'required',
	],
	'VENDOR_BANK_DETAILS' => [
		'bankName' => 'required',
		'accountHolder' => 'required',
		'accountNumber' => 'required',
		'IFSCCode' => 'required',
		'accountType' => 'required',
		'branchName'	=> 'required',
	],
	'Order' => [
			'cart_object' => 'required',
			'buyer_address' => 'required',
			'buyer_name' => 'required',
			'buyer_contact_no' => 'required',
			'sub_total' => 'required',
			'no_of_items' => 'required',
			'discount_amount' => 'required',
			'order_time' => 'required',
	],
	'SEARCH_CUSTOMER' => [
		'search' => 'required',
		],
	'SUBSCRIPTION' => [
			'buyer_address' 		   => 'required',
			'buyer_name' 	      	   => 'required',
			'buyer_contact_no' 		   => 'required',
			'sub_total' 			   => 'required',
			'delivery_charges' 		   => 'required',
			'discount_amount' 		   => 'required',
			'order_time' 			   => 'required',
			'product_id' 			   => 'required',
			'product_per_unit_price'   => 'required',
			'per_unit_empty_jar_price' => 'required',
			'empty_jar_qty' 		   => 'required',
			'quantity' 			  	   => 'required',
			'plan_id'				   =>  'required',
		],
	'SUBSCRIPTION_NEW' => [
			// 'buyer_address' 		   => 'required',
			// 'buyer_name' 	      	   => 'required',
			// 'buyer_contact_no' 		   => 'required',
			'productId' 			   => 'required',
			'empty_jar_qty' 		   => 'required',
			'qty' 			  	   	   => 'required',
			'plan_id'				   =>  'required',
		],
	'ADD_GROUP' => [
		'name' => 'required',
		],
	'ADD_CATEGORY' => [
		'name' => 'required',
		'image' => 'required',
		],
	'ADD_PRODUCT' => [
	'product_title' => 'required',
	'description'   => 'required',
	'unit'			=> 'required',
	'price'			=> 'required',
	'category_id'	=> 'required',
	'image'        =>  'required',
	],
	'ORDERPLACED' => [
			'quantity' => 'required',
			'buyer_address' => 'required',
			'buyer_name' => 'required',
			'buyer_contact_no' => 'required',
		],
	'REDEEM_COUPON' => [
			'couponNumber'	=> 'required',
		],
	'PLACED_ORDER_SCHEDULE' => [
			'id' => 'required',
			'buyer_address' => 'required',
			'buyer_name' => 'required',
			'buyer_contact_no' => 'required',
			'date'	=> 'required'
		
		] , 
	'SUBSCRIPTIONSDETAILS' => [
			'id' => 'required',
		]
];

	public static function get($api){
		return self::$rules[$api];
	}
}
