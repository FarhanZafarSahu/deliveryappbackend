<?php

namespace App\Constants;

class Message
{
	// General Constants
	const PERMISSION_DENIED = 'You don\'t have access to this operation.';
	const INVALID_TOKEN = 'Invalid Token! User Not Found.';
	const SOMETHING_WENT_WRONG = 'Something went wrong. Please try again later!';
	const INVALID_INPUT_VALUES = 'Invalid input values.';
	const TOKEN_EXPIRED = 'Token Expired.';
	const TOKEN_NOT_FOUND = 'Token Not Found.';
	const REQUEST_SUCCESSFUL = 'Request Successful.';
	const INVALID_ID = 'Invalid ID.';
	const RECORD_NOT_FOUND = "Record Not Found.";
	const ORDER_PLACE ="Order Placed successfully";
	const ALREADY_SUBSCRIPTION = "Already have an Subscription.";
};