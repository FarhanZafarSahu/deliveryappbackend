<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QrCode extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qr_codes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_number',
        'vendor_id',
    ];

    /**
     * Return a set of data we want.
     *
     */
    public function getArrayResponse() {
        
        return [
            'qr_number'             => $this->qr_number,
            'vendor_id'              => $this->vendor_id,
               ];
    }

}
