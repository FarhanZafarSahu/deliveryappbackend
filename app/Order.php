<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use League\Flysystem\Config;

class Order extends Model
{
	 const CREATED_AT = 'created_at';
     const UPDATED_AT = 'updated_at';
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s','updated_at' => 'datetime:Y-m-d H:i:s','deleteTime' => 'datetime:Y-m-d H:i:s','addTime' => 'datetime:Y-m-d H:i:s'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'no_of_items',
        'customer_id',
        'total_price',
        'discounted_price',
        'discount_amount',
		'delivery_charges',
		'order_time',
		'order_no',
		'delivery_name',
		'delivery_address',
		'delivery_mobile_number',
		'pickup_name',
		'pickup_address',
		'pickup_mobile_number',
		'status',
		'cancel_reason',
		'subscription_id',
		'vendor_id',
		'delivery_date',
		'is_accept',
		
    ];

    public static function store($no_of_items, $total_price, $discount_amount , $order_time, $order_no, $delivery_mobile_number, $delivery_name, $delivery_address,$customer_id , $delivery_date ,$quantity ,$subscription_id = NULL )
	{
		$customer = Customer::where('id',$customer_id)->with('vendor')->first();
		 return self::create([
								"no_of_items"            => $no_of_items,
								"total_price"            => $total_price,
								"discounted_price"       => $total_price - $discount_amount ,
								"discount_amount"        => $discount_amount,
								"delivery_charges"       => $quantity * config('constant.DELIVERY_CHARGES'),
								"order_time"  		     => $order_time,
								"order_no"       		 => $order_no,
								"delivery_name"          => $delivery_name,
								"delivery_address"       => $delivery_address,
								"delivery_mobile_number" => $delivery_mobile_number,
								"pickup_name"            => config('constant.PICK_UP_NAME'),
								"pickup_address"         => config('constant.PICK_UP_ADDRESS'),
								"pickup_mobile_number"   => config('constant.PICK_UP_MOBILE_NUMBER'),
								"status"           		 => 1,
								"customer_id"			 => $customer_id,
								"subscription_id"        => $subscription_id,
								"vendor_id"				 => $customer->vendor->id ?? 0,
								"delivery_date"			 => $delivery_date ?? \Carbon\Carbon::now()->today()
							]);
	}
 public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
public function orderqty()
    {
        return $this->orderDetails()->sum('quantity');
    }
 public function vendor()
    {
        return $this->hasOne(User::class,'id','vendor_id');
    }
 public function customer()
    {
        return $this->hasOne(Customer::class,'id','customer_id');
    }
public function subscription()
    {
        return $this->hasOne(UserSubscriptionPlan::class,'id','subscription_id');
    }
public static function CalculateNextDeliveryDate($plan_id)
{
			$next_date_of_delivery = NULL;
			if($plan_id == 1)
			{
				$next_date_of_delivery =  \carbon\Carbon::now()->today()->format('Y-m-d');
			}
			else if($plan_id == 2)
			{
			$next_date_of_delivery =  \carbon\Carbon::now()->addDay(2)->format('Y-m-d');
			}
			else if($plan_id == 3)
			{
			$next_date_of_delivery =  \carbon\Carbon::now()->addDay(3)->format('Y-m-d');
			}
			return $next_date_of_delivery ;
}
}
