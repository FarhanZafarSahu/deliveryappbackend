<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorBankDetail extends Model
{
    protected $guarded=[];
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'bank_name', 'account_holder','account_number','IFSC_code','account_type','branch_name'];
		public function getArrayResponse() {
        return [
            'id'                => $this->id,
            'user_id'           => $this->user_id,
			'bank_name'      	=> $this->bank_name,
			'account_holder'    => $this->account_holder,
			'account_number'	=> $this->account_number,
			'IFSC_code' 	    => $this->IFSC_code,
			'account_type'      => $this->account_type,
			'branch_name' 		=> $this->branch_name,
        ];
    }


}
